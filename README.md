# PIA Software

The PIA (PINE INP Analysis) software calculates INP concentrations, temperature spectra and overview plots. All data is quality controlled using SaQC (https://doi.org/10.5281/zenodo.5888547).
For a detailed information about the software refer to the [documentation](https://pia.readthedocs.io/en/stable/).

## Setup

### Prerequisites

[Download](https://git-scm.com/downloads) and [install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) git, which allows you to get the latest software version.
You can clone an existing remote repository to a local directory of your choice using git.
The software is compatible with Python `3.11`, `3.12`, and `3.13`.

### Clone the repository

Open a terminal (e.g. "git bash" on Windows) and navigate to a directory where you want the software to be located and clone it using git:

```sh
cd /path/where/module/should/be/located
git clone https://codebase.helmholtz.cloud/pine/pia_software.git
```

### Update your local repository

To update your local version to the newest version from the repository run

```sh
cd /path/where/module/should/be/located
git pull
```

### Create a virtual python environment

As a python software PIA depends on other python modules, which have to be installed on your computer. You can use the tool `venv` to manage your python environment.
Both tools represent a valid solution and can be used to create a virtual python environment, which prevents clashes with other python software and their dependencies.

### Python environment setup using venv

Open the terminal (e.g. "git bash" on Windows) and type

```sh
python -m venv PIA
```

which will create a new folder with your environment, containing the python executable to run your code. Activate the new environment by typing (UNIX):

```sh
source PIA/bin/activate
```

or if you are using Windows you might need to type the following instead:

```cmd
./PIA/Scripts/activate.bat
```

This activates your venv and you can install the python dependencies with

```sh
(PIA) pip install saqc==2.6.0 openpyxl
```

## How to

1. Copy the settings_pine-id_campaign.toml file from the files folder into the main folder and rename it according to your PINE and campaign name. Inside the file set the variable `DATA_PATH` for your campaign folder and the variable `CALIB_PATH` to the fidas calibration files and add metadata of the campaign. If you would like to save the analysed data at a path other than the camapign path, also specify `ANALYSIS_PATH`.
2. In `main.py` add `pine_id`, `campaign_id` and the number of operations you would like to analyze.
3. Run the `main.py` file.

### Read-in manual ice ice_threshold

1. Create a file according to the example file in the files folder. The conversion bin to diameter can be found in the bins_ice_threshold.txt file.
2. Save the file in your analysis path in `L1_Data/exportdata/ice_threshold`.
3. Set `manual_ice_threshold` in `main.py`to `True`.

If you want a graphical python interface suited for scientific programming you can also install and run `spyder` with

```sh
(PIA) pip install spyder
(PIA) spyder
```

## Output

### L0_Data -> exportdata:

* exportdata_opc_spd: mapped opc_data

### L1_Data -> exportdata:

* exportdata_cn: 3sec binned instrument data and concentrations for each run
* exportdata_ice: concentrations for each expansion
* ice_threshold: minimal ice diameter for each run

### L2_Data:

* Temp_Spec: 0.5°C binned concentrations for each run and mean over whole operation

### Plots:

* timeseries of whole operation for instrument data, size ditribution and concentrations
* diagnostics plot for theoretical temperature course
* histograms for single runs

### Quality_Control:

* log file for each operation with warnings and errors reported for each run
* quality_flags: all flags for each data point

## Contact

actris-ccice@imk-aaf.kit.edu