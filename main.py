# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""
# insert path to software folder
# from pathlib import Path
# import sys
# sys.path.append(str(Path("")))

from datetime import datetime

from pia.flagging import Flagging
from pia.level0_data import Lev0Data
from pia.level_data import Lev1Data, Lev2Data
from pia.plotting import PlotsPINE

if __name__ == "__main__":
    pine_id = "AIDAm"
    campaign = "TROPIC05"

    operation_id = 1

    manual_ice_threshold = False
    cirrus_mode = False

    t_0 = datetime.now()
    print(t_0)
    print("read raw data, create level 0 data")
    level0_data = Lev0Data(campaign, pine_id, operation_id)
    reference_time = level0_data.ref_time
    print("create level1 data")
    level1_data = Lev1Data(level0_data, reference_time, manual_ice_threshold, cirrus_mode)
    if (manual_ice_threshold is False) & (cirrus_mode is False):
        level1_data.export_ice_threshold()
    level1_data.export_concentrations(cirrus_mode)
    level1_data.export_ice()
    print("create level2 data")
    level2_data = Lev2Data(level0_data, level1_data)
    level2_data.export_data()
    level2_data.export_data_mean()
    print("flag data")
    flagged_data = Flagging(level0_data, level1_data, cirrus_mode)
    flagged_data.export_data()
    print("create plots")
    plots = PlotsPINE(campaign, pine_id, operation_id, manual_ice_threshold)
    plots.plot_timeseries(cirrus_mode)
    plots.plot_diagnostic()
    print(datetime.now() - t_0)
