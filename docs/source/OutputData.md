# Output Data

| {ref}`Level 0 <Level0Data>` | {ref}`Level 1 <Level1Data>` | {ref}`Level 2 <Level2Data>` |
| --------------------------- | --------------------------- | --------------------------- |
| {ref}`exportdata_opc_spd` | {ref}`exportdata_cn`<br>{ref}`exportdata_ice`<br>{ref}`ice_threshold <data_threshold>` | {ref}`Temp_Spec` |