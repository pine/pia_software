
# Getting Started

## Setup

### Prerequisites

[Download](https://git-scm.com/downloads) and [install](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) git, which allows you to get the latest software version.
You can clone an existing remote repository to a local directory of your choice using git.

### Clone the repository

Open a terminal (e.g. "git bash" on Windows) and navigate to a directory where you want the software to be located and clone it using git:

```sh
cd /path/where/module/should/be/located
git clone https://codebase.helmholtz.cloud/pine/pia_software.git
```

### Update your local repository

To update your local version to the newest version from the repository run

```sh
cd /path/where/module/should/be/located
git pull
```

### Create a virtual python environment

As a python software PIA depends on other python modules, which have to be installed on your computer. It is therefore recommended to run the software from within a virtual environment.
You can use the tool `venv` to manage your python environment. The PIA software supports python versions 3.11, 3.12, and 3.13.
Both tools represent a valid solution and can be used to create a virtual python environment, which prevents clashes with other python software and their dependencies.

### Python environment setup using venv

Open the terminal (e.g. "git bash" on Windows) and type

```sh
python -m venv PIA
```

which will create a new folder with your environment, containing the python executable to run your code. Activate the new environment by typing (UNIX):

```sh
source PIA/bin/activate
```

or if you are using Windows you need to type the following instead:

```cmd
.\PIA\Scripts\activate.bat
```

This activates your venv and you can install the python dependencies with

```sh
(PIA) pip install -r requirements.txt
```

## How To

This gives you an overview on how you can run the software and shows you some optional settings you can choose from.
 

### Running the software for standard PINE versions

1. Make sure you created and filled out the {ref}`Logbook <logbook>`.
2. Copy the {ref}`settings` file into the main directory and rename it according to your PINE version and campaign name.
Inside the file set the variables `DATA_PATH`, `CALIB_PATH`, and possibly `ANALYSIS_PATH` and add metadata of the campaign.
3. In `main.py` add `pine_id`, `campaign_id` and the number of the operation you would like to analyse.
4. Run the `main.py` file.

### Read-in manual ice threshold

It is possible to use manual set ice threshold diameters instead of the ones created by the automatic ice threshold finder of the PIA software. This is not recommended, though.
To do this follow these steps:

1. Create a file according to the {ref}`example file <manual-ice-threshold>`. The conversion bin to diameter can be found in the {ref}`bins-ice-threshold` file.
2. Save the file in your analysis path in `L1_Data/exportdata/ice_threshold`.
3. Set the parameter `manual_ice_threshold` in `main.py`to `True`.

### Running the software for PINEair

Running the PIA software for PINEair requires a slightly different set up, as the individual analysis steps need to be repeated for each chamber in use.
Therefore follow these steps:

1. Make sure you created and filled out the {ref}`Logbook <logbook>`.
2. Copy the {ref}`settings` file into the main directory and rename it according to your PINE version and campaign name.
Inside the file set the variables `DATA_PATH`, `CALIB_PATH`, and possibly `ANALYSIS_PATH` and add metadata of the campaign.
3. In `main_pineair.py` add `pine_id`, `campaign_id` and the number of the operation you would like to analyse.
If not all 3 chambers of PINEair are in use, the script needs to be adapted:
	- For level 0 data, the number of the chamber is part of the arguments. Remove all instances that belong to the unused chamber.
	- Remove the unsused chamber from the arguments of the export functions and the plotting functions.
4. Run the `main_pineair.py` file.

### Running the software for cirrus conditions

If you are running the PINE under cirrus conditions no ice threshold should be calculated. If you set the parameter `cirrus_mode` in the main file to True,
no ice threshold diameters will be calculated and the total particle concentration will be used as INP concentration instead.