(Level0Data)=
# Level 0 Data

(exportdata_opc_spd)=
## exportdata_opc_spd

The _*opc_spd.txt_ files contain the raw OPC bin data mapped to the respective size diameters of the particles.
One file contains the data of one run.
