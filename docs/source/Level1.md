(Level1Data)=
# Level 1 Data

(exportdata_cn)=
## exportdata_cn

The _*cn.txt_ files contain 3-second-binned data:
- time relative to start of run
- particle concentration
- particle count
- INP concentration
- INP count
- run mode: flush (1), expansion (2), refill (3)
- mean flow rate
- mean chamber pressure
- mean Ti1-Ti5
- particle counts per size bin
One file contains the data of one run.

(exportdata_ice)=
## exportdata_ice

The _*ice.txt_ files contain the run-wise data:
- run ID
- time at the middle of the flush
- minimal temperature (Ti5) reached during expansion
- pressure at the end of expansion
- mean flow rate during expansion
- INP concentration during expansion
- INP concentration during flush
- particle concentration during expansion
- particle concentration during flush
One file contains the data of one operation.

(data_threshold)=
## ice_threshold

The _*ice_threshold.txt_ files contain the bin number and the ice threshold for each run. The bin number does not correspond to the OPC bins, but follows a logarithmic equally sized distribution between 0.1 and
1000 $\mu$m. One file contains the data of one operation.

