# Supplementary Files

The directory `files` contains files necessary or helpful for the analysis.

(logbook)=
## Logbook_pine_id_campaign.xlsx

This is a template for the campaign logbook. It needs to be renamed according to the PINE version and the campaign name and copied into the campaign folder where the raw data is stored.
The software will not run, if the logbook doesn't exist.

(settings)=
## settings_pine-id_campaign.toml

This file is needed to set the data path and to include the metadata of the camapign in the analysed data.

It needs to be renamed according to the PINE version and the campaign name and copied into the main directory of the software. Inside the file set the `DATA_PATH`
to the directory of the raw data. The `CALIB_PATH` needs to lead to the directory containing the bin-to-size mapping of the OPC data.
If you want to store the analysed data at another directory as the raw data you can change the `ANALYSIS_PATH`, if you want to store the analysed data in the same
directory as the raw data, keep `ANALYSIS_PATH` as 0.
Now, add the coordinates and the station name to the metadata, as well as the name of the PI and the institute leading the campaign.

(manual-ice-threshold)=
## pine-id_campaign_op_id_operation-id_ice_threshold_manually.txt

If you would like to set the ice threshold manually, you can create .txt-files that look like this one. The name of the file is the same as how it would 
be created by the software, but with a `_manually` suffix. The file contains the columns `run id`, `bin`, and `d_min`.
Whereas `bin` corresponds to an internal diameter to bin conversion and `d_min` is the respective ice threshold.
This conversion is documented in the `bins_ice_threshold.txt` file.
To use the manual ice threshold a bin number and ice threshold diameter must be set for every run in the operation.
If you want to use a modified file that has been created by the software, make sure to remove the header including the version number.

(bins-ice-threshold)=
## bins_ice_threshold.txt

see {ref}`manual-ice-threshold`.