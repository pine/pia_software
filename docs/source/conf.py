# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath("../../"))

print("PATH", sys.path)

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "PIA"
copyright = "2024, KIT/IMKAAF"
author = "Nicole Büttner"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",  # create html from python files, https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html
    "sphinx.ext.autosummary",  # see https://www.sphinx-doc.org/en/master/usage/extensions/autosummary.html
    "sphinx.ext.napoleon",  # support for numpy docstring style
    "sphinx.ext.mathjax",  # support for mathematical expressions
    "myst_parser",  # markdown support with extended syntax, see https://www.sphinx-doc.org/en/master/usage/markdown.html
]

myst_enable_extensions = [
    "dollarmath",
    "amsmath",
]

templates_path = ["_templates"]
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "pydata_sphinx_theme"
html_static_path = ["_static"]

html_logo = "_static/pia_logo.png"
html_favicon = "_static/pia_logo.png"
