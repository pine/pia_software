.. pia documentation master file, created by
   sphinx-quickstart on Thu Nov 14 16:32:45 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
PIA - PINE INP Analysis Software
================================

The PIA Software is an analysis software to derive time series data (Level 1) and temperature spectra (Level 2) of INP concentrations from measurements with the PINE (Portable Ice Nucleation Experiment) instrument.
It support Python versions 3.11, 3.12, and 3.13.


.. toctree::
   :maxdepth: 1
   :hidden:

   Repository <https://codebase.helmholtz.cloud/pine/pia_software>
   
.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Contents:

   Getting started <GettingStarted.md>
   Output <Output.rst>
   Supplementary Files <files.md>
   Quality Control <qc.md>
   Module <module>
   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
