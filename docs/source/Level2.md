(Level2Data)=
# Level 2 Data

(Temp_Spec)=
## Temp_Spec

The directory Temp_Spec contains two types of files. The _*temp.txt_ and the _*temp_mean.txt_.
The _*temp.txt_ files contain the temperature binned data for one run:
- start temperature of bin
- end temperature of bin
- run ID
- time at the middle of the flush
- mean time relative to start of expansion
- mean INP concentration
- lowest chamber pressure

The _*temp_mean.txt_ files contain the averaged INP concentrations per temperature bin over all runs in one operation:
- start temperature of bin
- end temperature of bin
- mean INP concentration in temperature bin over all runs
- standard deviation of INP concentration