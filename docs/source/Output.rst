===========================================
Output
===========================================

The PIA Software creates 3 types of output:


.. toctree::
   :glob:
   :maxdepth: 1
   :caption: Contents:

   Data <OutputData.md>
   Flags <OutputFlags.md>
   Plots <OutputPlots.rst>