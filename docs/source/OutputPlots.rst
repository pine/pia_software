=====
Plots
=====

It is possible to create three different types of plots with the PIA software. The plotting functions are independent of the remaining software and only rely on the output data.
Only the function for the time series plot is included in the main.py script, the others have to be added individually.

--------------------
Time series overview
--------------------

The overview plot shows the gas and wall temperatures, the pressure, the dew point temperature, the size distribution of the binned particles, and the INP concentration.
Flagged data will be indicated with yellow, organge, or red coloured boxes.
This plot is created for one operation. For operations taking longer than a few hours, details won't be visible in the plot.
It can be called via:

.. autofunction:: pia.plotting.PlotsPINE
.. autofunction:: pia.plotting.PlotsPINE.plot_timeseries

.. image:: _static/timeseries.png
  :width: 800

----------------
Diagnostics Plot
----------------

The diagnostics plot creates a comparison of the theoretical dry adiabatic temperatures and saturation ratio with the measured values for one operation.
It can be called via:

.. autofunction:: pia.plotting.PlotsPINE
.. autofunction:: pia.plotting.PlotsPINE.plot_diagnostic

.. image:: _static/diagnostic.png
  :width: 800
  
-----------------------
Histogram of single run
-----------------------

For specified runs histograms of the size distribution including the ice threshold is plotted.
It can be called via:

.. autofunction:: pia.plotting.PlotsPINE
.. autofunction:: pia.plotting.PlotsPINE.plot_hist

.. image:: _static/hist.png
  :width: 600