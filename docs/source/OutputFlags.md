# Output Flags

Flags created by the automated quality control are transferred into warning messages saved in the Quality_Control directory into the _*flags.log_ files.
The messages are categorised by their severity into INFO, WARNING and ERROR. The flagged data points are not being removed from any of the data sets. 
The PINE users must decide themselves how to handle the flagged data.