#### 3.0.3 (2025-02-19)

##### Chores

*  change python version to >=3.11 in pyproject.toml ([f5ecca34](https://codebase.helmholtz.cloud/pine/pia_software/commit/f5ecca340d43fdfb1b2bc4767e4641d2b56b46b7))
*  update pyproject.toml ([8e916d1d](https://codebase.helmholtz.cloud/pine/pia_software/commit/8e916d1ddee88666297c08d1125a89475225170e))
*  edit ci pipeline ([72aa0cef](https://codebase.helmholtz.cloud/pine/pia_software/commit/72aa0cef85b314fd4ba1b3d3119a588b19b00c50))

##### New Features

*  adapt code for PINE-3 ([a8dd4677](https://codebase.helmholtz.cloud/pine/pia_software/commit/a8dd4677dd7b4ab9a938777ec73628b47bb33cf5))

##### Code Style Changes

*  remove white space ([a7416e7c](https://codebase.helmholtz.cloud/pine/pia_software/commit/a7416e7c0888fa971dda303058deb4166c16a8da))

#### 3.0.2 (2025-01-24)

##### Chores

*  update changelog [skip ci] ([62b7a75d](https://codebase.helmholtz.cloud/pine/pia_software/commit/62b7a75dbe3e45aa692ede8a9790579a949266e4))
*  update changelog [skip ci] ([a593b3cf](https://codebase.helmholtz.cloud/pine/pia_software/commit/a593b3cf67d7b98e8e7ffa0ec38ae82a534d1233))
*  update gitignore [skip ci] ([e472ed2d](https://codebase.helmholtz.cloud/pine/pia_software/commit/e472ed2dc3015af326cba4dcb6e8ec27bec93847))

##### Documentation Changes

*  update logo [skip ci] ([10b05543](https://codebase.helmholtz.cloud/pine/pia_software/commit/10b055436175bb84af05be2c25e727a3ca66c4f6))
*  remove conda from Setup [skip ci] ([57f33361](https://codebase.helmholtz.cloud/pine/pia_software/commit/57f3336166e5b697852e8671ec0757bf91a300a7))
*  remove conda from readme [skip ci] ([c134b906](https://codebase.helmholtz.cloud/pine/pia_software/commit/c134b906ab3fca2d1f9bd70dfdad166f16d31f20))
*  fix typos and minor changes [skip ci] ([5cf3179f](https://codebase.helmholtz.cloud/pine/pia_software/commit/5cf3179f3861306bcf87b92553a6fc8555414d6e))
*  edit expressions ([469d9b7a](https://codebase.helmholtz.cloud/pine/pia_software/commit/469d9b7af38933d74c27de31bb08b1ecab3eafa7))

##### New Features

*  update for LabView Software Version 3.13 ([4874c2ae](https://codebase.helmholtz.cloud/pine/pia_software/commit/4874c2aee6c495a54ed6db2ca48a4c429d96eb2a))

#### 3.0.1 (2024-12-18)

##### Chores

*  update gitignore  ([e472ed2d](https://codebase.helmholtz.cloud/pine/pia_software/commit/e472ed2dc3015af326cba4dcb6e8ec27bec93847))

##### Documentation Changes

*  create online documentation

##### Bug Fixes

* fix reporting of flags in log-file, solves issue #24

#### 3.0.0 (2024-10-16)

##### Documentation Changes

*  update docstrings ([15013de8](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/15013de87888d282a1293fd1f310e49770b36d62))
*  update docstrings ([dec5e806](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/dec5e8066aef44c3b9cdb5f72f5834efd6279bdd))

##### New Features

*  include chamber number = 1 in header for normal PINEs ([b7c220e7](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/b7c220e7a5fa439a6eeccf103900942c0fb873f1))
*  Time of expansion added to ice.txt-file ([3d10792d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3d10792d379f2a30f5369b4d9c8961528ef97fed))
*  config data and pfr file for pineair ([d379b941](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/d379b941901e7defcdbfe848fa7aadff9464560e))
*  create main script for pineair ([c2c59872](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/c2c598724b966f6755b1876dc2f951b948bb0a27))
*  instrument_data adapted to PINEair ([343db8c3](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/343db8c3fd30d8609cd5258950bc45155344ca74))

##### Bug Fixes

*  fix variable name for ice threshold to d ([3f8cd287](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3f8cd287cda2e4473cbb32e36734d2a4bbea1ca3))
*  fix minor bugs ([02e02d22](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/02e02d22af6969d4a3152d12ba7088af144a80c4))
*  change OPC raw data path for new software version ([6120f050](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/6120f050dbc5ee6db703ed36c4a36fa52834f436))
*  include column "ice_threshold_2_sigma" ([2a830be7](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/2a830be71427ec7870cf198cd02ee917e53d121b))
*  fix problem in corrected run mode for gaps in opc data ([644c5d6f](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/644c5d6fe8c23a35c888ad4748ca0aa9337516d8))
*  exclude runs without data ([c897e0e2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/c897e0e2e09742915e5c0cd2636423930652e588))
*  remove double line ([e917eb7c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e917eb7c928acaa66264ac95aa633db1346efe4a))
*  create folder for plots ([6d501e11](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/6d501e111c49fde014ebddc861eb8cec1d469807))
*  adapt code for aidam ([0b07462c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/0b07462c5979f517da5f70c80c8b02d9781e108c))
*  remove ice threshold range test. The algorithm is written in a way such that this test can not fail. ([5f59c6e5](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/5f59c6e53e15861e6cf9996105c28ad35d535ced))
*  opc_spd files don't have to be calibrated again for PINEair. More files because of time before first run. ([d329d66c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/d329d66c3e4a4c58a2ba33b1aa38898a2539fb80))
*  Remove Flow range flags if they are at the end or beginning of a run mode ([9f4ec0e2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/9f4ec0e2f55f546e210de3e530af71108d225b16))
*  INP during flush test adapted, so it doesn't trigger for 0 values ([c1fdbefb](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/c1fdbefbaa61ed98fd2b9e1d0a7bab9cd8b78ef2))
*  chained assignment error ([5e688598](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/5e688598c4d8bd063911f35d6d55ed04b129bf12))
*  ice threshold is set above biggest aerosol not below ([c6d105ec](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/c6d105ec77c402cf5a6116c3289dab59d800738c))

##### Code Style Changes

*  fix too long lines ([da047cf2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/da047cf2c78016fd71c7e86b9599574480cad612))
*  white space between operators, separators ([fab4f60e](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/fab4f60ead41513d6f518c390b4eddd375e238a8))
*  use "is" for None instead of "==" ([559dbd94](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/559dbd940187dc6661b6eaea2d78ed9941c0fe97))
*  clean up comments, unused lines ([0da4b885](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/0da4b885a91dd197ac7608e0be1701050c565295))
*  fix too long lines ([06e29729](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/06e29729fea3b84d65963d8bb9eddaf20b072d09))
*  remove white space ([0f30859c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/0f30859cea5a02d368a3b50a8ed765983198eaaa))
*  name all runs run_id ([cdd3b296](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/cdd3b2966295de8a4cd47794995e457062a28638))
*  sort imports ([1a5aa9d6](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/1a5aa9d644b646ad4ead583b64e30c134edddf02))

#### 2.2.0 (2024-08-08)

##### Documentation Changes

*  update readme and requirements ([94a3ead2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/94a3ead25c9a14bb3a7dc9e6c3b14425960ca3c6))
*  include requirements.txt file ([04acd7c9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/04acd7c9e1c2a2da8475d2a01210d6b7c4a95089))

##### New Features

*  include test for vertical stripes in the opc data ([ef038464](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ef0384643b45c552b1957eaea8fff6d3e47bd4df))
*  include new labview version ([f82760b8](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f82760b838f6d47e7d06fee2628bed004e0873cc))
*  includes the threshold for the INPs during flush test in the settings-file. Closes issue [#15](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/pull/15) ([dd1b96b5](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/dd1b96b55dc849c6e8fb16b061179d2e4dfc3b1b))
*  include test that checks for vertical opc stripes during expansion. Solution for issue [#9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/pull/9) ([d99ae94c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/d99ae94c0be2161f3630a7071c862c7b253a3313))
*  flag as missing opc data if opc file is empty. Solves issue [#6](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/pull/6) ([881fcf84](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/881fcf848105d63052a2f65a1c1f96b952decaaf))
*  exit program if housekeeping data is empty ([46a4f157](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/46a4f15732db9d1e7dd45e694a16ad7480fcc0a8))
*  include path for analyzed data ([3f235a97](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3f235a97e36339148800729d6eef84cdf8af538e))

##### Bug Fixes

*  remove quotes from flag ([02ad4bc5](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/02ad4bc5f968ac5238f4ca851fbe391cbffd2d1b))
*  update tests to saqc 2.6.0 ([6093b45d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/6093b45d9b7e3c554deb60a11d01731c846f83bf))
*  include date_format in read_opc_files ([3890e85a](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3890e85ab5e225b4df70a42410f9b63bdddd34bd))
*  fixed ChainedAssignmentError, closes Issue [#10](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/pull/10) ([61b93f75](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/61b93f75cc6801ba8d5e77b325bd1e22f4a06f8a))
*  changed TimedeltaIndex to pd.to_timedelta ([e2a4f19d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e2a4f19dfab00dfbd954a11be46c69a02769c966))
*  adapted date for data structure for PINE-04-02 ([065772ba](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/065772baa1b699766add77763d2b41f263e07ac5))
*  rename ice threshold 2 sigma flag ([38560d04](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/38560d0495fdb18f5171a75077afe9886a8405fe))
*  include flag 549 in settings ([f5e616e8](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f5e616e85eb0e4e8290bad9ef45f87541cabee77))
*  use correct PINE name in settings path ([77f152ab](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/77f152ab024cc184de4b4efd0bea979f9ac3b49d))
*  empty bins at the end of an expansion where the following bin has less than 5 particle counts will be excluded from the test. Solves issue [#16](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/pull/16) ([68d7a6c1](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/68d7a6c12f084306e8613d42046e795a16299ed9))

##### Code Style Changes

*  clean up imports ([ed3bb0a9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ed3bb0a94ea33ed3161bf397470bae0a20c142a3))

#### 2.0.2 (2023-09-22)

##### Documentation Changes

*  updated documentation ([3194d820](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3194d8209199efd2279af3f9742f847093a36ad0))
*  update docstrings ([4fcffdf2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/4fcffdf2f012f9223b6588b57b0246684a3df8cd))

##### New Features

*  update environment and requirements ([400f268e](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/400f268eba840d46de860f417c09d5680e2ebb14))
*  updated name ([2136170d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/2136170d0ebbb7500fe7d8f7add9f32dd16c0fa8))
*  ice threshold finder doesn't use 2-bin rule and correction via std anymore ([8253426a](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/8253426a8cbf6c06cfa1e908f41efd3a989842fb))
*  include software type ([d8ee513b](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/d8ee513baf5c658a3edb9bf222b8db8e2a64f07a))
*  rename quick_look to main ([e9a796d8](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e9a796d8486a7f53de1adc7338b0df8a7b16f02a))
*  only include test for icing for old pine software ([724ee27b](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/724ee27b99eb7518afa983cbda4d774be7268461))
*  test for supersaturation ([fe881189](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/fe8811892368da172073b3df2880a5afbd258e0c))
*  new reference time for one run is mid time of flush ([057d1db9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/057d1db96bd6acb1542bcacd3ec80439246a5a02))
*  implementation of PINE-06 labview software ([87aae874](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/87aae8742f2b863ac0eb8b0ac82b70db191c8545))
*  include time of expansion in lev2 data ([69f5fb31](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/69f5fb31b3f00dd03deb757c5cfa2fa95c11848b))
*  integrate settings in class ExtractData ([ee03ba25](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ee03ba25e5866587e7a82b4d66599efde094b1c7))
*  adapted to saqc 2.4.1 ([6d63e47f](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/6d63e47fa4e6bd6b79f99346cc4b1fdd7938761f))

##### Bug Fixes

*  set observed for groupby ([f9ea53a7](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f9ea53a7e8c7274b9814de3bc6ca05dbe88c855a))
*  highlight supersaturation as warning ([e1ed47c6](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e1ed47c68de1371062912bcb3b44d7911570d6c0))
*  remove bins from opc_gap test until no empty bins are at the beginning and end of expansion ([2a2ab3aa](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/2a2ab3aaef9dd38513b9223a31b1e66efdeb98fa))
*  include aerosol only as str ([cec30dac](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/cec30dac5610530a72e5f747a3d0afb929545ec2))
*  rename run_mode to run_modus ([b65ae981](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/b65ae9811cf9f09b83fbc9824fc4484c6940070b))
*  check for total run time and time until refill to be bigger than 0sec ([e26580d3](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e26580d3d1c7b17c5406979be7207cde9f549fbc))
*  create opc_spd folder before checking for files ([73711244](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/73711244f2fb191a036eee3650461b054c7a352c))
*  flagged with warning if particles are detected during refill ([3570b0eb](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3570b0eb2ecd5f3aeb9e20c17a7ab6e8fad81b5a))
*  updated for SaQC 2.4.1 ([84230e3c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/84230e3cda148edf59382291e9934285235c110e))
*  remove cloud activation from output files ([b02d4621](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/b02d4621bdcf2d914ecbd98c155f3db70f6636c1))
*  store cn data in operation folders ([9602f164](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/9602f1644b76af2f40a04f5f622954049fb146fa))
*  make andGroup from SaQC work ([ebc5a363](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ebc5a3636ab3bbae12735a07ce8c7fa476e799d3))
*  include version 2.0.2 in header rows ([f1d59ab9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f1d59ab9cd5ee2df2be673b657a7722f12c0791b))
*  flag T_diff for Ti4-Ti5 with error, else with warning ([94c218fd](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/94c218fdada0ebc309e2da89ec49f4672d5b5100))
*  remove lev2 from flagging ([79e8fffe](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/79e8fffe19eb9fc2e748510a8c163b19398694f4))
*  remove cloud activation from output files ([3793b104](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3793b1043e9c2be378de6e42633f7fbd47feba49))
*  typo Run -> RUN ([67deee44](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/67deee44d18c9a2c6b4d40aa2215076cc61de484))
*  fill nan's in temperature flatline flags with 000 ([1c056385](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/1c056385713f1000d925db2c235dd43210eeaf69))
*  add encoding in savetxt ([3843d2ac](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3843d2acfee326048483de590163d5b4630b662c))
*  get aerosol info from logbook ([132a492a](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/132a492a33d5607b0bfcbf887b1ca970e8daa1d7))
*  apply temperature flatline test only during expansion ([ce14d63d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ce14d63d25464d400977772b5da8b20f13301fb1))
*  include path to calibration files in settings ([aa5b652c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/aa5b652c1510ab6c1239fc5ef7c149214f2df8dd))
*  exit script if logbook can not be found ([eb9a8cd9](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/eb9a8cd93a78f0f998d515c2357aa3c12cb39e4a))
*  checks for all nans in Ti2 not only first entry ([acde79bb](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/acde79bba6a78c330b73f0b30178d97dc286b338))
*  remove comment from plots ([f246aa5a](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f246aa5af7129eeec5728bdca79745b55d7fd858))

#### 2.0.1 (2023-05-31)

##### New Features

*  save opc-spd data in operation folders ([4bf829cd](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/4bf829cd47d8b67d1b78cd59bccc813b8e6a55d5))
*  support for Labview software version 7.4 ([beebdbf6](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/beebdbf6b27ca36c8f1c67827495ec5c53d49945))
*  new operation types added ([786353f2](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/786353f24d5ba057b823dd20f63978072d861d75))

##### Bug Fixes

*  read in labview software version as float ([ea96a7a1](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ea96a7a140118f91661c57d4a4d44e1fa6a588e9))
*  Warning if more than 5 data points in refill contain particles ([bb65633b](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/bb65633bc04390d2ac2b36a90cd6fd95ef472056))
*  don't export ice threshold for manual files ([b1aed3dd](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/b1aed3ddc2ab17a83474391616e7ab97851ba689))

#### 2.0.0 (2023-05-10)

##### Documentation Changes

*  include example file for manual ice threshold file ([733bffb0](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/733bffb08a9e3f854a7c00b24f2ba08cdeceafff))

##### New Features

*  replace autom8qc with SaQC for quality control ([855d7e41](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/855d7e414c6e995930e62d8f35e098bd458efa5e))
*  opc_spd data is now saved in L0_Data not L1_Data ([7136f369](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/7136f369518de658cfb6f22fecf5643227472ffe))
*  Time of expansion added to ice.txt-file ([a2f03b63](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/a2f03b63bea7cd068d335505918e659f5fd57101))
*  new operation types added ([73234080](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/732340804e68f2e781e2721dab9e1346150feeab))
*  create metadata in settings ([ea00c0a3](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ea00c0a320c84dd186b5d1b174e080f5b9ff605c))
*  include metadata in output files ([f937cf46](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f937cf46a2007b352cea7afc501e324cea1ba8d3))
*  include flush concentrations in ice file, remove from cn file ([5a5cdeae](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/5a5cdeaeb36c581221eafcaec58df3451bef1c4a))
*  include time and operation type in plot title, remove flow and particle concentration from plot ([e63c15ba](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e63c15ba490375d70f0aa5d782045cbf80059614))

##### Bug Fixes

*  adapted filenames for PINE-1A ([c6094e32](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/c6094e326b4533cf9488f78456c92f89449d24a0))
*  datetime added in columns of ice file ([959729b0](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/959729b01b6d52c54e1c90dabec019f87acea5b1))
*  removed on_bad_lines from config_data ([2d55a5f3](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/2d55a5f34612f0f81856ddac5f8a7c7f95fffda8))
*  include PINE-3 for ice threshold finder ([1afeb3c4](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/1afeb3c448f72baec57d05f7e66422be28e53f36))
*  read in config parameters all at once ([112e831c](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/112e831c9ee1191f1d6b7cad09b6efd908ce23fb))
*  server address included in path ([3f1d8096](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/3f1d8096a9d6f0cb0d52762299a5c8c0fa30fe7a))
*  remove logger handler after flagging ([25b54077](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/25b540777be6a02df376c3c240f14c58b58a85f6))
*  replaced value None for version with "0" ([f4933bb1](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f4933bb141332da2a4063d494fd33fd333c448f3))


#### 1.0.1 (2023-02-10)

##### Documentation Changes

*  template for logbook added ([45d6f107](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/45d6f1070d7fe4b2ae7dafcfc158cc17722f627a))
*  changelog created ([eb5ae7a7](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/eb5ae7a75be537b785bb2b832f78f025fe8459c3))
*  file added for bin-diameter conversion of ice threshold ([60d392ce](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/60d392ce47cb383c7696092dd63aef9d6942d353))
*  prefix deleted from environment.yml ([f60659b6](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/f60659b6645c85a4e024bed805b34017212c1630))
*  deleted old licensing file ([725db4bd](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/725db4bdd300c641cf403510cc27f9fed84604ed))

##### New Features

*  system exits if logbook has no (valid) entry ([da46ecaf](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/da46ecaf06d6ae927ff03c693fd2ab7b31c0d7ee))
*  system exits if no valid dates are in pfr_file ([838c55d7](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/838c55d7fb70a0b2cd11d37c96e5640029734651))

##### Bug Fixes

*  supports now PINE-05-01 ([6498eeae](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/6498eeaebfe07a026b9a5d55afa5f05f1379e412))
*  .loc used to replace values in df ([20815c80](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/20815c80d3fa4f0f3df43f7bd6c4e29bf74bfa25))
*  aligned x-axis in time series plot ([ea5a5190](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/ea5a5190e7d18f0e5698bc18e940d3b7b03e5693))
*  changed lower limit of pressure to 495 hPa ([1cd7ab74](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/1cd7ab74b3465841b838e03add611b93481eb69e))
*  always get upper limit for plot ([70a9bb7d](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/70a9bb7d26e176b395d27085ca91c2d37c6af1bc))
*  ignore runs with too short expansion ([d8834d77](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/d8834d7786279880504480b13b6e5335b6208e72))
*  volume of pine-01-a changed to 6.21 l ([0164f474](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/0164f47447f113da2741a8d8b2f4fc2929c90453))
*  temperature qc changed to valid sensors for prototype pines ([73971acf](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/73971acfaf44bc1584fd271d30c0c455c6a4eba9))
*  typo data -> date ([e7688f88](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/e7688f884351bde7cfa864d20138ad8f92fb9af7))
*  in create_smaller_bins.py skip more rows due to version in header ([5a9c6ac3](git@git.scc.kit.edu:gi2423/pine_analysis_software.git/commit/5a9c6ac354e65f298fdf2647d8167c2a9c6c4e69))
