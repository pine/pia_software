# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""
import os

import matplotlib as mpl
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

import pia.functions as func
from pia.extract_data import ExtractData

pd.set_option("future.no_silent_downcasting", True)


class Plots:
    """
    Contains functions for plotting routines in PlotsPINE and PlotsPINEair.

    Parameters
    -----------
    campaign : str
        name of campaign
    pine_id : str
    operation_id : int
    manual_ice_threshold : bool
        whether the ice threshold is manually set or not
    *arg : tuple
        numbers of chambers that are used
    """

    def __init__(self, campaign, pine_id, operation_id, manual_ice_threshold, *arg):
        mpl.rcParams.update(mpl.rcParamsDefault)
        self.campaign = campaign
        self.pine_id = pine_id
        self.operation_id = operation_id
        self.manual_ice_threshold = manual_ice_threshold

    def make_dir_plots(self):
        """
        Creates directory for plots.
        """
        os.makedirs(self.path_plots, exist_ok=True)

    def get_ticks(self, len_fig, ref_time):
        """
        Creates ticks and labels for relativ time

        Parameters
        ----------
        len_fig : float
            width of the figure
        ref_time : datetime
            Start time of operation

        Returns
        -------
        ticks : np.array
        labels : np.array
        """
        self.pfr_file["t_rel_stop"] = (self.pfr_file["time end"] - ref_time).dt.total_seconds()
        duration_operation = max(self.pfr_file.t_rel_stop) / 60
        if (duration_operation < 20) | (int(duration_operation) % (int(len_fig / 4.2) / 2) == 0):
            ticks = np.linspace(0, round(duration_operation, 2), int(len_fig / 4.2) + 1)
        else:
            ticks = np.linspace(
                0,
                int(duration_operation) - (int(duration_operation) % (int(len_fig / 4.2) / 2)),
                int(len_fig / 4.2) + 1,
            )
        labels = np.around(ticks, decimals=2)
        return ticks, labels

    def create_error_colors(
        self,
        colors,
        name,
        flags_info=None,
        flags_warning=None,
        flags_error=None,
        flags_missing=None,
    ):
        """
        Creates a column containing colors used for flagging in the plot.

        Parameters
        ----------
        colors : DataFrame
        name : str
            column name
        flags_info : Series, optional
            Contains the info-flagged values. The default is None.
        flags_warning : Series, optional
            Contains the warning-flagged values. The default is None.
        flags_error : Series, optional
            Contains the error-flagged values. The default is None.
        flags_missing : Series, optional
            Contains missing values. The default is None.
        """
        colors[name] = None
        if flags_info is not None:
            flags_info = flags_info.reset_index().rename(columns={0: "info"})
            colors.loc[flags_info["info"], name] = "y"
        if flags_warning is not None:
            flags_warning = flags_warning.reset_index().rename(columns={0: "warning"})
            colors.loc[flags_warning["warning"], name] = "tab:orange"
        if flags_error is not None:
            flags_error = flags_error.reset_index().rename(columns={0: "error"})
            colors.loc[flags_error["error"], name] = "r"
        if flags_missing is not None:
            flags_missing = flags_missing.reset_index().rename(columns={0: "missing"})
            colors.loc[flags_missing["missing"], name] = "r"


class PlotsPINE(Plots):
    """
    Creates time series for PINE

    parameters
    ----------
    campaign : str
        name of campaign
    pine_id : str
    operation_id : int
    manual_ice_threshold : bool
        whether the ice threshold is manually set or not
    """

    def __init__(self, campaign, pine_id, operation_id, manual_ice_threshold):
        super().__init__(campaign, pine_id, operation_id, manual_ice_threshold)
        self.raw_data = ExtractData(campaign, pine_id, operation_id)
        self.metadata = self.raw_data.metadata
        self.path_campaign = self.raw_data.campaign
        self.path_analysis = self.raw_data.path_analysis
        self.path_plots = self.path_analysis / "Plots"
        self.instrument_data = self.raw_data.read_instrument_data()
        structure = self.raw_data.data_structure(self.instrument_data)
        self.pfr_file = self.raw_data.read_pfr_file(structure)
        self.ref_time = self.raw_data.ref_time
        if pine_id == "AIDAm":
            self.pine_name = pine_id
        else:
            self.pine_name = "PINE-" + pine_id
        self.dt = self.raw_data.dt
        os.makedirs(self.path_plots, exist_ok=True)

    def plot_timeseries(self, cirrus_mode):
        """
        Creates time series.

        Parameters
        ----------
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not
        """
        mpl.rcParams.update(mpl.rcParamsDefault)
        if self.pfr_file.loc[len(self.pfr_file), "time refill"] < pd.to_datetime(
            "1905-01-01", format="%Y-%m-%d"
        ):
            self.pfr_file.drop(len(self.pfr_file), axis=0, inplace=True)
        self.pfr_file["t_rel_start"] = (
            self.pfr_file["time start"] - self.ref_time
        ).dt.total_seconds()
        self.pfr_file["t_rel_stop"] = (self.pfr_file["time end"] - self.ref_time).dt.total_seconds()
        self.pfr_file["t_rel_refill"] = (
            self.pfr_file["time refill"] - self.ref_time
        ).dt.total_seconds()
        self.pfr_file["t_rel_expansion"] = (
            self.pfr_file["time expansion"] - self.ref_time
        ).dt.total_seconds()

        ticks, labels = super().get_ticks(50, self.ref_time)
        if self.instrument_data.DP.notna().any():
            fig, (axT, axP, axDP, axD, axC) = plt.subplots(5, 1, figsize=(50, 30))
            axDP.plot(self.instrument_data.t_rel_min, self.instrument_data.DP)
            axDP.set_ylabel("dew point [K]", fontsize=28.7)
            axDP.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
            axDP.tick_params(
                axis="y", which="major", labelsize=28.7, length=16.1, direction="inout"
            )
            plt.setp(
                (axT, axP, axDP, axD, axC),
                xlim=(0, max(self.pfr_file.t_rel_stop) / 60),
                xticks=ticks,
            )
        else:
            fig, (axT, axP, axD, axC) = plt.subplots(5, 1, figsize=(50, 25))
            plt.setp(
                (axT, axP, axD, axC), xlim=(0, max(self.pfr_file.t_rel_stop) / 60), xticks=ticks
            )
        axT.plot(self.instrument_data.t_rel_min, self.instrument_data.Ti1, color="red", label="Ti")
        axT.plot(self.instrument_data.t_rel_min, self.instrument_data.Ti2, color="green")
        axT.plot(self.instrument_data.t_rel_min, self.instrument_data.Ti3, color="blue")
        axT.plot(
            self.instrument_data.t_rel_min,
            self.instrument_data.Tw1,
            color="purple",
            linestyle="--",
            label="Tw",
        )
        axT.plot(
            self.instrument_data.t_rel_min,
            self.instrument_data.Tw2,
            color="black",
            linestyle="--",
        )
        axT.plot(
            self.instrument_data.t_rel_min,
            self.instrument_data.Tw3,
            color="darkcyan",
            linestyle="--",
        )
        axT.plot(self.instrument_data.t_rel_min, self.instrument_data.Ti5, color="orange")
        axT.plot(self.instrument_data.t_rel_min, self.instrument_data.Ti4, color="grey")
        axT.set_ylabel("temp [K]", fontsize=28.7)
        axT.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
        axT.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
        axT.legend(fontsize=28.7, loc=4)

        axP.plot(self.instrument_data.t_rel_min, self.instrument_data.Pch)
        axP.set_ylabel("pres [mbar]", fontsize=28.7)
        axP.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
        axP.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")

        axD.set_ylabel("particle \n diameter [µm]", fontsize=28.7)
        axD.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
        axD.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
        axD.set_xticks(ticks * 60)

        size_distribution_df = pd.concat(
            self.raw_data.read_concentrations(self.pfr_file, self.ref_time)
        )
        size_distribution_df.reset_index(inplace=True, drop=True)
        particles = size_distribution_df.drop(
            columns=[
                "opc_n",
                "run_modus",
                "run_id",
                "t_start",
                "date",
                "date_end",
                "n_ice",
                "flow",
                "Pch",
                "Ti1",
                "Ti2",
                "Ti3",
                "Ti4",
                "Ti5",
                "Tw1",
                "Tw2",
                "Tw3",
                "Fe",
                "Fm",
                "DP",
                "opc_cn_bin",
                "cn_ice",
                "t_rel",
            ],
            errors="ignore",
        ).T
        y_bins = list(func.make_df_grid_log(100, 0.1, 1000)[0]["d"]) + [2000]
        x_bins = list(size_distribution_df["t_start"])
        x_bins.append(x_bins[-1] + 3)
        h_d = axD.pcolormesh(
            x_bins, y_bins, particles, norm=mpl.colors.LogNorm(vmin=1, vmax=4000), cmap="gnuplot2"
        )
        axD.set_yscale("log")
        axD.set_xlim(xmax=max(self.pfr_file.t_rel_stop))
        if cirrus_mode is False:
            ice_threshold = self.raw_data.read_ice_threshold_file(self.manual_ice_threshold)
            axD.hlines(
                ice_threshold.d,
                xmin=self.pfr_file.t_rel_start,
                xmax=self.pfr_file.t_rel_stop,
                color="red",
                linewidths=2,
            )
        if (any(self.instrument_data.DP)) & (not any(self.instrument_data.DP.isnull())):
            cbaxes = fig.add_axes([0.90495, 0.269, 0.004, 0.1322])
        else:
            cbaxes = fig.add_axes([0.90495, 0.322, 0.004, 0.1395])
        cb = plt.colorbar(h_d, cax=cbaxes)
        cb.ax.tick_params(labelsize=20.8)

        ice_df, operation_type = self.raw_data.read_ice_data()
        lim_1 = 0.4 * min(ice_df.INP_cn_0)
        lim_2 = 2 * max(ice_df.INP_cn_0)
        if np.isnan(max(ice_df.INP_cn_0)):
            lim_1 = 0.1
            lim_2 = 100

        ice_df["t_rel"] = (
            pd.to_datetime(ice_df["datetime"], format="mixed") - self.ref_time
        ).dt.total_seconds() / 60
        axC.scatter(ice_df.t_rel, ice_df.INP_cn_0)
        axC.set_ylabel("INP concentration \n [$stdL^{-1}$]", fontsize=28.7)
        axC.set_xlabel("time since start of the operation [min]", fontsize=28.7)
        axC.tick_params(axis="both", which="major", labelsize=28.7, length=16.1, direction="inout")
        axC.tick_params(axis="y", which="minor", length=10, direction="inout")
        axC.set_yscale("log")
        axC.set_xticklabels(labels)
        plt.setp(axC, ylim=(lim_1, lim_2))

        # create flagged data
        flags = self.raw_data.read_qc_data()
        flags_metadata_nan = flags[1].replace(to_replace="000", value=np.nan)
        flags_instrument_nan = flags[0].replace(to_replace="000", value=np.nan)
        run_flags = flags_metadata_nan[["increasing_time"]].copy()
        run_flags[["dur_expansion"]] = flags_metadata_nan[["dur_expansion"]]
        time_error = run_flags.notna().any(axis=1)
        colors = pd.DataFrame(index=flags_instrument_nan.index)
        colors = colors.droplevel(level=["run_id", "run_modus"])
        colors.reset_index(drop=False, inplace=True)
        colors["t_rel"] = (colors.date - self.ref_time).dt.total_seconds()

        dp_flags_info = flags_instrument_nan[["DP_range"]].notna().any(axis=1)
        dp_flags_missing = flags_instrument_nan[["DP_range"]].isin(["999"]).any(axis=1)
        dp_flags_warning = flags_instrument_nan[["DP_flatline"]].notna().any(axis=1)
        self.create_error_colors(
            colors,
            "dp_flags_colors",
            flags_info=dp_flags_info,
            flags_warning=dp_flags_warning,
            flags_missing=dp_flags_missing,
        )
        if "Ti2_flatline" in flags_instrument_nan:
            T_flags_warning = (
                flags_instrument_nan[
                    [
                        "Ti1_range",
                        "Ti2_range",
                        "Ti3_range",
                        "Tw1_range",
                        "Tw2_range",
                        "Tw3_range",
                        "Ti1_flatline",
                        "Ti2_flatline",
                        "Ti3_flatline",
                        "Ti4_flatline",
                    ]
                ]
                .notna()
                .any(axis=1)
            )
            T_flags_error = (
                flags_instrument_nan[["Ti4_range", "Ti5_range", "Ti5_flatline"]].notna().any(axis=1)
            )
            T_flags_missing = (
                flags_instrument_nan[
                    [
                        "Ti1_range",
                        "Ti2_range",
                        "Ti3_range",
                        "Ti4_range",
                        "Ti5_range",
                        "Tw1_range",
                        "Tw2_range",
                        "Tw3_range",
                    ]
                ]
                .isin(["999"])
                .any(axis=1)
            )
            flags_t_end_warning = (
                flags_metadata_nan[[f"Ti{i}/Ti{i+1}_diff" for i in range(1, 4)]].notna().any(axis=1)
            )
            flags_t_end_error = flags_metadata_nan[["Ti4/Ti5_diff"]].notna().any(axis=1)
        else:
            T_flags_warning = (
                flags_instrument_nan[
                    [
                        "Ti1_range",
                        "Ti3_range",
                        "Ti5_range",
                        "Tw1_range",
                        "Tw2_range",
                        "Tw3_range",
                        "Ti1_flatline",
                        "Ti3_flatline",
                    ]
                ]
                .notna()
                .any(axis=1)
            )
            T_flags_error = flags_instrument_nan[["Ti5_range", "Ti5_flatline"]].notna().any(axis=1)
            T_flags_missing = (
                flags_instrument_nan[
                    ["Ti1_range", "Ti3_range", "Ti5_range", "Tw1_range", "Tw2_range", "Tw3_range"]
                ]
                .isin(["999"])
                .any(axis=1)
            )
            flags_t_end_warning = None
            flags_t_end_error = (
                flags_metadata_nan[[f"Ti{i}/Ti{i+2}_diff" for i in [1, 3]]].notna().any(axis=1)
            )

        flags_p_end = flags_metadata_nan[["pch_end"]].notna().any(axis=1)
        flags_p_error = flags_instrument_nan[["Pch_range"]].notna().any(axis=1)
        flags_p_warning = flags_instrument_nan[["Pch_flatline"]].notna().any(axis=1)
        flags_p_missing = flags_instrument_nan[["Pch_range"]].isin(["999"]).any(axis=1)
        if cirrus_mode is False:
            ice_threshold_2_sigma = (
                flags_metadata_nan[["ice_threshold_2_sigma"]].notna().any(axis=1)
            )
        stripes_flags = flags_metadata_nan[["ice_concentration"]].notna().any(axis=1)
        refill_flags = flags_instrument_nan[["opc_refill"]].notna().any(axis=1)
        opc_flags = flags_metadata_nan[["opc_data"]].notna().any(axis=1)
        opc_flags_missing = (
            flags_instrument_nan[["concentration_missing"]].isin(["997"]).any(axis=1)
        )
        supersaturation_flags = flags_metadata_nan[["supersaturation"]].notna().any(axis=1)

        self.create_error_colors(
            colors,
            "T_flags_colors",
            flags_warning=T_flags_warning,
            flags_error=T_flags_error,
            flags_missing=T_flags_missing,
        )
        self.create_error_colors(
            colors,
            "p_flags_colors",
            flags_warning=flags_p_warning,
            flags_error=flags_p_error,
            flags_missing=flags_p_missing,
        )
        self.create_error_colors(colors, "refill_flags_colors", flags_warning=refill_flags)
        self.create_error_colors(colors, "opc_missing_colors", flags_error=opc_flags_missing)

        if time_error.any():
            for run_id in time_error.index:
                if time_error[run_id]:
                    axT.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_start"] / 60,
                        self.pfr_file.loc[run_id, "t_rel_stop"] / 60,
                        facecolor="r",
                        alpha=0.5,
                    )
                    axP.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_start"] / 60,
                        self.pfr_file.loc[run_id, "t_rel_stop"] / 60,
                        facecolor="r",
                        alpha=0.5,
                    )
                    axD.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_start"],
                        self.pfr_file.loc[run_id, "t_rel_stop"],
                        facecolor="r",
                        alpha=0.5,
                    )
                    axC.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_start"] / 60,
                        self.pfr_file.loc[run_id, "t_rel_stop"] / 60,
                        facecolor="r",
                        alpha=0.5,
                    )
                    if self.instrument_data.DP.notna().any():
                        axDP.axvspan(
                            self.pfr_file.loc[run_id, "t_rel_start"] / 60,
                            self.pfr_file.loc[run_id, "t_rel_stop"] / 60,
                            facecolor="r",
                            alpha=0.5,
                        )
                    colors = colors[
                        (colors.t_rel < self.pfr_file.loc[run_id, "t_rel_start"])
                        | (colors.t_rel > self.pfr_file.loc[run_id, "t_rel_stop"])
                    ]
        for x in colors.index:
            if colors.loc[x, "T_flags_colors"] is not None:
                axT.axvspan(
                    (colors.loc[x, "t_rel"] - self.dt / 2) / 60,
                    (colors.loc[x, "t_rel"] + self.dt / 2) / 60,
                    facecolor=colors.loc[x, "T_flags_colors"],
                    alpha=0.5,
                )
            if "DP_range" in flags_instrument_nan:
                if colors.loc[x, "dp_flags_colors"] is not None:
                    axDP.axvspan(
                        (colors.loc[x, "t_rel"] - self.dt / 2) / 60,
                        (colors.loc[x, "t_rel"] + self.dt / 2) / 60,
                        facecolor=colors.loc[x, "dp_flags_colors"],
                        alpha=0.5,
                    )
            if colors.loc[x, "p_flags_colors"] is not None:
                axP.axvspan(
                    (colors.loc[x, "t_rel"] - self.dt / 2) / 60,
                    (colors.loc[x, "t_rel"] + self.dt / 2) / 60,
                    facecolor=colors.loc[x, "p_flags_colors"],
                    alpha=0.5,
                )
            if colors.loc[x, "refill_flags_colors"]:
                axD.axvspan(
                    (colors.loc[x, "t_rel"]),
                    (colors.loc[x, "t_rel"] + self.dt),
                    facecolor=colors.loc[x, "refill_flags_colors"],
                    alpha=0.5,
                )
        for run_id in flags_metadata_nan.index:
            if flags_p_end[run_id]:
                axP.axvspan(
                    self.pfr_file.loc[run_id, "t_rel_start"] / 60,
                    self.pfr_file.loc[run_id, "t_rel_stop"] / 60,
                    facecolor="tab:orange",
                    alpha=0.5,
                )
            if cirrus_mode is False:
                if ice_threshold_2_sigma[run_id]:
                    axD.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_start"],
                        self.pfr_file.loc[run_id, "t_rel_stop"],
                        facecolor="tab:orange",
                        alpha=0.5,
                    )
            if stripes_flags[run_id]:
                axD.axvspan(
                    self.pfr_file.loc[run_id, "t_rel_expansion"],
                    self.pfr_file.loc[run_id, "t_rel_stop"],
                    facecolor="red",
                    alpha=0.5,
                )
            if opc_flags[run_id]:
                axD.axvspan(
                    self.pfr_file.loc[run_id, "t_rel_start"],
                    self.pfr_file.loc[run_id, "t_rel_stop"],
                    facecolor="red",
                    alpha=0.5,
                )
            if flags_t_end_error[run_id]:
                axT.axvspan(
                    self.pfr_file.loc[run_id, "t_rel_expansion"] / 60,
                    self.pfr_file.loc[run_id, "t_rel_refill"] / 60,
                    facecolor="red",
                    alpha=0.5,
                )
            if flags_t_end_warning is not None:
                if flags_t_end_warning[run_id]:
                    axT.axvspan(
                        self.pfr_file.loc[run_id, "t_rel_expansion"] / 60,
                        self.pfr_file.loc[run_id, "t_rel_refill"] / 60,
                        facecolor="tab:orange",
                        alpha=0.5,
                    )
            if supersaturation_flags[run_id]:
                axDP.axvspan(
                    self.pfr_file.loc[run_id, "t_rel_expansion"] / 60,
                    self.pfr_file.loc[run_id, "t_rel_refill"] / 60,
                    facecolor="tab:orange",
                    alpha=0.5,
                )
        axT.set_title(
            f"{self.pine_name} {self.campaign} operation {self.operation_id},"
            + f" {operation_type}\n\n"
            + f"{self.pfr_file['time start'][1]} - "
            + f"{self.pfr_file['time end'][len(self.pfr_file)]}",
            fontsize=35.9,
        )
        plt.savefig(
            self.path_plots
            / f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_timeseries.png"
        )
        self.pfr_file.drop(labels=["t_rel_start", "t_rel_refill"], axis=1, inplace=True)

    def plot_hist(self, runs):
        """
        Creates histograms for single runs.

        Parameters
        ----------
        runs : list
            list of runs, that should be plotted

        """
        if len(runs) >= 1:
            if self.pine_id.startswith("A"):
                name = "AIDAm"
            else:
                name = f"PINE-{self.pine_id}"
            size_distribution = self.raw_data.read_concentrations(self.pfr_file, self.ref_time)
            ice_threshold = self.raw_data.read_ice_threshold_file(self.manual_ice_threshold)
            for run_id in runs:
                if run_id > len(self.pfr_file):
                    print(">>>This operation has no run_id " + str(run_id))
                    continue
                t_start = self.pfr_file.loc[run_id, "time start"]
                t_expansion = self.pfr_file.loc[run_id, "time expansion"]
                t_refill = self.pfr_file.loc[run_id, "time refill"]

                if type(t_start) is np.datetime64:
                    t_start = pd.Timestamp(t_start)
                    t_expansion = pd.Timestamp(t_expansion)
                    t_refill = pd.Timestamp(t_refill)

                df_run = size_distribution[run_id - 1]
                particles = df_run.drop(
                    columns=[
                        "opc_n",
                        "run_modus",
                        "run_id",
                        "date",
                        "date_end",
                        "t_start",
                        "Fm",
                        "DP",
                        "opc_cn_bin",
                        "cn_ice",
                        "t_rel",
                        "Ti5",
                        "Tw1",
                        "Tw2",
                        "Tw3",
                        "Fe",
                        "Pch",
                        "n_ice",
                        "Ti1",
                        "Ti2",
                        "Ti3",
                        "Ti4",
                        "flow",
                    ],
                    errors="ignore",
                )
                binned_data = particles.sum(axis=0)
                width = np.diff(list(map(float, binned_data.index)) + [2000])
                y_bins = list(func.make_df_grid_log(100, 0.1, 1000)[0]["d"]) + [2000]
                x_bins = list(df_run["t_start"])
                x_bins.append(x_bins[-1] + 3)

                fig = plt.figure(figsize=(50, 30))
                gs = fig.add_gridspec(1, 2)
                ax1 = fig.add_subplot(gs[0, 0])
                ax1.bar(
                    list(func.make_df_grid_log(100, 0.1, 1000)[0]["d"]),
                    binned_data,
                    align="edge",
                    width=width,
                )
                ax1.set_yscale("log")
                ax1.set_xscale("log")
                ax1.set_title("Aerosol size distribution during run", fontsize=30)
                ax1.tick_params(axis="both", which="major", labelsize=25, length=15)
                ax1.tick_params(axis="both", which="minor", length=10)
                ax1.set_xlabel("particle diameter [µm]", fontsize=25)
                ax1.set_ylabel("counts", fontsize=25)
                y_max = 4 * max(binned_data)
                ax1.text(
                    100,
                    3 * y_max,
                    f"{self.pine_name} {self.campaign} operation: {self.operation_id} "
                    + f"run: {run_id}",
                    fontsize=35,
                )
                ax1.set_ylim(0.9, y_max)
                ax1.set_xlim(0.1, 1050)
                ax1.vlines(
                    ice_threshold.d[run_id], ymin=1, ymax=y_max / 2, color="red", linewidths=2
                )

                ax2 = fig.add_subplot(gs[0, 1])
                h = ax2.pcolormesh(
                    x_bins,
                    y_bins,
                    particles.T,
                    norm=mpl.colors.LogNorm(vmin=1, vmax=4 * 10**3),
                    cmap="gnuplot2",
                )
                ax2.set_xlabel("relative time [sec]", fontsize=25)
                ax2.set_ylabel("particle diameter [µm]", fontsize=25)
                ax2.tick_params(axis="both", which="major", labelsize=25, length=15)
                ax2.tick_params(axis="both", which="minor", length=10)
                ax2.set_yscale("log")
                ax2.set_title("Size distribution during run", fontsize=30)
                ax2.hlines(
                    ice_threshold.d[run_id],
                    xmin=x_bins[0],
                    xmax=x_bins[-1],
                    color="red",
                    linewidths=2,
                )
                cbar = fig.colorbar(h, ax=ax2)
                cbar.ax.tick_params(labelsize=25)

                plt.savefig(
                    self.path_plots
                    / f"{name}_{self.campaign}_op_id_{self.operation_id}_run_id_{run_id}_hist.png"
                )

    def plot_diagnostic(self):
        """
        Creates a timeseries plot for the adiabatic temperature and
        the saturation ratio

        """
        self.pfr_file["time expansion"] = self.pfr_file["time expansion"].dt.floor("s")
        times_expansion = self.pfr_file["time expansion"]
        start_values = (
            self.instrument_data[self.instrument_data["date"].isin(times_expansion)]
        ).reset_index(drop=True)
        start_values["run_id"] = np.nan
        for i in start_values.index:
            start_values.loc[i, "run_id"] = self.pfr_file.index[
                self.pfr_file["time expansion"] == start_values.loc[i, "date"]
            ]
        start_values = start_values.set_index("run_id", drop=True)
        if len(start_values) < len(times_expansion):
            start_values = start_values.reindex(self.pfr_file.index)
        start_temperatures = start_values["Ti5"]
        start_pressures = start_values["Pch"]
        instrument_data_short = func.choose_time_interval(
            self.instrument_data, self.pfr_file.loc[1, "time start"], None
        )
        Sw_all = []
        adiabatic_temperature_all = []
        for run_id in range(1, len(self.pfr_file) + 1):
            if run_id < len(self.pfr_file):
                instrument_run = func.choose_time_interval(
                    self.instrument_data,
                    self.pfr_file.loc[run_id, "time start"],
                    self.pfr_file.loc[run_id + 1, "time start"],
                )
            else:
                instrument_run = func.choose_time_interval(
                    self.instrument_data, self.pfr_file.loc[run_id, "time start"], None
                )
            instrument_run.reset_index(inplace=True, drop=True)
            adiabatic_temperature_run = [
                func.T_adiabatic(
                    start_temperatures[run_id],
                    start_pressures[run_id],
                    instrument_run.loc[j, "Pch"],
                )
                for j in range(len(instrument_run))
            ]
            adiabatic_temperature_all += adiabatic_temperature_run
            Sw = [
                func.Saturation(
                    start_temperatures[run_id],
                    instrument_run.loc[j, "Ti5"],
                    start_pressures[run_id],
                    instrument_run.loc[j, "Pch"],
                )
                for j in range(len(instrument_run))
            ]
            Sw_all += Sw

        instrument_data_short["T_adiabatic"] = adiabatic_temperature_all
        instrument_data_short["Sw"] = Sw_all

        ticks, labels = super().get_ticks(50, self.ref_time)
        fig, (axT, axS) = plt.subplots(2, 1, figsize=(50, 25))
        axT.set_title(
            f"{self.pine_name} {self.campaign} operation {self.operation_id}", fontsize=35.9
        )
        axT.plot(instrument_data_short.t_rel_min, instrument_data_short.Ti5, label="measured")
        axT.plot(
            instrument_data_short.t_rel_min, instrument_data_short.T_adiabatic, label="adiabatic"
        )
        axT.set_ylabel("Temperature [K]", fontsize=28.7)
        axT.tick_params(axis="both", which="major", labelsize=28.7, length=16.1, direction="inout")
        axT.set_xticks(ticks)
        axT.legend(fontsize=28.7, loc=4)

        axS.plot(instrument_data_short.t_rel_min, instrument_data_short.Sw)
        axS.set_ylabel("Sw", fontsize=28.7)
        axS.tick_params(axis="both", which="major", labelsize=28.7, length=16.1, direction="inout")
        axS.set_xticks(ticks)
        axS.set_xlabel("time since start of the operation [min]", fontsize=28.7)
        plt.savefig(
            self.path_plots
            / f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_diagnostic.png"
        )
        plt.close("all")


class PlotsPINEair(Plots):
    """
    Creates time series for PINEair.

    Parameters
    -----------
    campaign : str
        name of campaign
    pine_id : str
    operation_id : int
    manual_ice_threshold : bool
        whether the ice threshold is manually set or not
    *arg : tuple
        numbers of chambers that are used
    """

    def __init__(self, campaign, pine_id, operation_id, manual_ice_threshold, *arg):
        super().__init__(campaign, pine_id, operation_id, manual_ice_threshold, *arg)
        self.chambers = arg
        self.raw_data = [
            ExtractData(campaign, pine_id, operation_id, chamber=chamber)
            for chamber in self.chambers
        ]
        self.metadata = self.raw_data[0].metadata
        self.path_campaign = self.raw_data[0].campaign
        self.path_analysis = self.raw_data[0].path_analysis
        self.path_plots = self.path_analysis / "Plots"
        self.instrument_data = [
            self.raw_data[x].read_instrument_data() for x in range(len(self.chambers))
        ]
        structure = self.raw_data[0].data_structure(self.instrument_data[0])
        self.pfr_file = self.raw_data[0].read_pfr_file(structure)
        self.ref_time = self.raw_data[0].ref_time
        self.pine_name = "PINE" + pine_id
        os.makedirs(self.path_plots, exist_ok=True)

    def plot_timeseries(self, cirrus_mode):
        """
        Creates time series.

        Parameters
        ----------
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not
        """
        self.pfr_file["t_rel_start"] = (
            self.pfr_file["time start"] - self.ref_time
        ).dt.total_seconds()

        ticks, labels = super().get_ticks(50, self.ref_time)
        if len(self.chambers) == 3:
            fig, (axT, axP, axDP, axD1, axD2, axD3, axC) = plt.subplots(7, 1, figsize=(50, 30))
            plt.setp(
                (axT, axP, axDP, axD1, axD2, axD3, axC),
                xlim=(0, max(self.pfr_file.t_rel_stop) / 60),
                xticks=ticks,
            )
            axD_dict = {0: axD1, 1: axD2, 2: axD3}
        elif len(self.chambers) == 2:
            fig, (axT, axP, axDP, axD1, axD2, axC) = plt.subplots(6, 1, figsize=(50, 30))
            plt.setp(
                (axT, axP, axDP, axD1, axD2, axC),
                xlim=(0, max(self.pfr_file.t_rel_stop) / 60),
                xticks=ticks,
            )
            axD_dict = {0: axD1, 1: axD2}
        elif len(self.chambers) == 1:
            fig, (axT, axP, axDP, axD1, axC) = plt.subplots(5, 1, figsize=(50, 30))
            plt.setp(
                (axT, axP, axDP, axD1, axC),
                xlim=(0, max(self.pfr_file.t_rel_stop) / 60),
                xticks=ticks,
            )
            axD_dict = {0: axD1}
        for chamber, instrument_data in enumerate(self.instrument_data):
            axT.plot(
                instrument_data.t_rel_min,
                instrument_data.Ti5,
                label=f"Chamber {self.raw_data[chamber].chamber}",
            )
            axP.plot(
                instrument_data.t_rel_min,
                instrument_data.Pch,
                label=f"Chamber {self.raw_data[chamber].chamber}",
            )
        axT.set_ylabel("temp [K]", fontsize=28.7)
        axT.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
        axT.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
        axT.legend(fontsize=28.7, loc=4)

        axP.set_ylabel("pres [mbar]", fontsize=28.7)
        axP.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
        axP.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")

        axDP.plot(self.instrument_data[0].t_rel_min, self.instrument_data[0].DP)
        axDP.set_ylabel("dew point [K]", fontsize=28.7)
        axDP.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
        axDP.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")

        y_bins = list(func.make_df_grid_log(100, 0.1, 1000)[0]["d"]) + [2000]
        for i, raw_data in enumerate(self.raw_data):
            ax = axD_dict[i]
            ax.set_ylabel(f"particle \n diameter [µm]\nchamber {raw_data.chamber}", fontsize=28.7)
            ax.tick_params(axis="y", which="major", labelsize=28.7, length=16.1, direction="inout")
            ax.tick_params(axis="x", which="major", labelsize=0, length=16.1, direction="inout")
            ax.set_xticks(ticks * 60)

            size_distribution = pd.concat(
                raw_data.read_concentrations(self.pfr_file, self.ref_time)
            )
            size_distribution.reset_index(inplace=True, drop=True)
            particles = size_distribution.drop(
                columns=[
                    "opc_n",
                    "run_modus",
                    "run_id",
                    "t_start",
                    "date",
                    "date_end",
                    "n_ice",
                    "flow",
                    "Pch",
                    "Ti1",
                    "Ti2",
                    "Ti3",
                    "Ti4",
                    "Ti5",
                    "Tw1",
                    "Tw2",
                    "Tw3",
                    "Fe",
                    "Fm",
                    "DP",
                    "opc_cn_bin",
                    "cn_ice",
                    "t_rel",
                ],
                errors="ignore",
            ).T

            x_bins = list(size_distribution["t_start"])
            x_bins.append(x_bins[-1] + 3)
            h = ax.pcolormesh(
                x_bins,
                y_bins,
                particles,
                norm=mpl.colors.LogNorm(vmin=1, vmax=4000),
                cmap="gnuplot2",
            )
            ax.set_yscale("log")
            ax.set_xlim(xmax=max(self.pfr_file.t_rel_stop))
            if cirrus_mode is False:
                ice_threshold = raw_data.read_ice_threshold_file(
                    self.manual_ice_threshold, raw_data.chamber
                )
                ax.hlines(
                    ice_threshold.d,
                    xmin=self.pfr_file.t_rel_start,
                    xmax=self.pfr_file.t_rel_stop,
                    color="red",
                    linewidths=2,
                )
        if len(self.chambers) == 3:
            cbaxes = fig.add_axes([0.90495, 0.222, 0.005, 0.3195])
        elif len(self.chambers) == 2:
            cbaxes = fig.add_axes([0.90495, 0.242, 0.005, 0.242])
        elif len(self.chambers) == 1:
            cbaxes = fig.add_axes([0.90495, 0.242, 0.005, 0.27])
        cb = plt.colorbar(h, cax=cbaxes)
        cb.ax.tick_params(labelsize=22.8)

        ice_df, operation_type = self.raw_data[0].read_ice_data()
        lim_1 = 0.1 * min(ice_df.INP_cn_0)
        lim_2 = 0.7 * max(ice_df.INP_cn_0)
        if np.isnan(lim_1):
            lim_1 = 0.1
            lim_2 = 100

        for chamber in self.chambers:
            chamber_df = ice_df.loc[ice_df.chamber == chamber, :]
            chamber_df["t_rel"] = (
                pd.to_datetime(chamber_df["datetime"], format="mixed") - self.ref_time
            ).dt.total_seconds() / 60
            axC.scatter(chamber_df.t_rel, chamber_df.INP_cn_0, label=f"Chamber {chamber}")
        axC.set_ylabel("INP \n concentration \n [$stdL^{-1}$]", fontsize=28.7)
        axC.set_xlabel("time since start of the operation [min]", fontsize=28.7)
        axC.tick_params(axis="both", which="major", labelsize=28.7, length=16.1, direction="inout")
        axC.tick_params(axis="y", which="minor", length=10, direction="inout")
        axC.set_yscale("log")
        axC.set_xticklabels(labels)
        plt.setp(axC, ylim=(lim_1, lim_2))
        axT.set_title(
            f"PINE{self.pine_id} {self.campaign} operation "
            + f"{self.operation_id}, {operation_type}\n\n"
            + self.pfr_file["time start"][1].strftime("%Y-%m-%d %H:%M:%S")
            + " - "
            + self.pfr_file["time end"][len(self.pfr_file)].strftime("%Y-%m-%d %H:%M:%S"),
            fontsize=35.9,
        )

        plt.savefig(
            self.path_plots
            / f"PINE{self.pine_id}_{self.campaign}_op_id_{self.operation_id}_timeseries.png"
        )
