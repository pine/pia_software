# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2022 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

import sys
import tomllib
from pathlib import Path

import numpy as np
import pandas as pd

import pia.functions as func


class SmallerBins:
    def __init__(
        self,
        calib_file_template,
        pine_id,
        campaign,
        operation_id,
        path_analysis,
        pfr_file,
        ref_time,
    ):
        self.pfr_file = pfr_file
        self.ref_time = ref_time
        if pine_id.startswith("air"):
            self.pine_name = "PINE" + pine_id
        elif pine_id == "AIDAm":
            self.pine_name = pine_id
        else:
            self.pine_name = "PINE-" + pine_id
        if "unittest" in campaign:
            self.path_calib = Path(__file__).parents[1].joinpath("tests/test_data")
        else:
            with open(
                Path(__file__).parents[1].joinpath(f"settings_{self.pine_name}_{campaign}.toml"),
                "rb",
            ) as f:
                metadata = tomllib.load(f)
            self.path_calib = Path(metadata["paths"]["CALIB_PATH"])
        self.dt = 1
        grid, self.distribution_bins = func.make_df_grid_log(100, 0.1, 1000)
        self.size_distribution = self.get_opc_data(
            calib_file_template, grid, pine_id, campaign, operation_id, path_analysis
        )

    def get_opc_data(
        self, calib_file_template, grid, pine_id, campaign, operation_id, path_analysis
    ):
        """
        Reads opc files for given operation.

        Parameters
        ----------
        calib_file_template: str
            name of the calibration file
        grid: Dataframe
            logarithmic grid for size binning
        pine_id : str
        campaign : str
        operation_id : int
        path_analysis : str

        Returns
        -------
        size_distribution : df
            df with binned opc data
        # calib_dict : dict
        #     dictionary to convert opc bin numbers to diameters
        """
        # filename template for OPC_spd files
        if pine_id == "AIDAm":
            opc_spd_file = f"AIDAm_{campaign}_op_id_{operation_id}_run_id_"
        else:
            opc_spd_file = f"PINE-{pine_id}_{campaign}_op_id_{operation_id}_run_id_"
        opc_spd_path = path_analysis / f"L0_Data/exportdata/exportdata_opc_spd/OP{operation_id}"
        opc_spd_name = opc_spd_file + "{}_opc_spd.txt"

        # list of filename templates for operations
        opc_spd_files = [opc_spd_path / opc_spd_name.format(i) for i in self.pfr_file.index]

        opc_list = []
        time_operation = (
            self.pfr_file["time end"][len(self.pfr_file)] - self.ref_time
        ).total_seconds()
        if time_operation <= 0:
            time_operation = (
                self.pfr_file["time end"][len(self.pfr_file) - 1] - self.ref_time
            ).total_seconds()
        time_grid = func.make_time_grid(time_operation, self.dt)
        date = self.ref_time + pd.to_timedelta(time_grid, unit="s")
        date_end = self.ref_time + pd.to_timedelta(time_grid + 1, unit="s")
        grid_df = pd.concat(
            [
                pd.DataFrame(time_grid, columns=["t_start"]),
                pd.DataFrame(date, columns=["date"]),
                pd.DataFrame(date_end, columns=["date_end"]),
            ],
            axis=1,
        )
        calibration_file = pd.read_csv(
            self.path_calib / calib_file_template, header=None, names=["channel", "d"], sep="\t"
        )
        del calibration_file
        run_id = 1
        with open(opc_spd_files[0], "r", encoding="latin1") as read_file:
            try:
                version = read_file.readlines()[0].split(":")[1].strip()
            except:
                version = None
            if version == "0":
                header_rows = 5
            elif int(version[0]) < 4:
                header_rows = 6
        opc_spd_dfs = (
            pd.read_csv(
                file,
                skiprows=header_rows,
                header=None,
                sep="\t",
                names=["date", "channel", "p_l", "d"],
                parse_dates=[0],
                encoding="latin1",
                usecols=[0, 2, 3],
            )
            for file in opc_spd_files
        )
        for file in opc_spd_dfs:
            file["t_rel_op"] = pd.to_datetime(file.date) - self.ref_time
            size_distribution_run = self.create_size_distribution(run_id, file, grid, grid_df)
            opc_list.append(size_distribution_run)
            run_id += 1
        return opc_list

    def create_size_distribution(self, run_id, file, grid, time_grid):
        """
        Bins opc data for time and size grid.

        Parameters
        ----------
        run_id : int
        file : DataFrame
            opc data
        grid : DataFrame
            logarithmic size grid
        time_grid : DataFrame
            time grid

        Returns
        -------
        size_distribution : DataFrame
            Binned opc data

        """

        t_start = self.pfr_file["time start"][run_id]
        t_expansion = self.pfr_file["time expansion"][run_id]
        t_refill = self.pfr_file["time refill"][run_id]
        t_end = self.pfr_file["time end"][run_id]
        size_distribution_run = time_grid[
            (time_grid.date_end >= t_start) & (time_grid.date <= t_end)
        ].copy()
        run_t = (t_end - t_start).total_seconds()
        if run_t <= 0:
            print(
                "Time for expansion end is set before time of run start"
                + f" in pfr-file for run {run_id}"
            )
            self.pfr_file.drop(labels=run_id, axis=0, inplace=True)
            if len(self.pfr_file) == 0:
                sys.exit("no run in operation")
            return pd.DataFrame()
        else:
            size_distribution_run.reset_index(drop=True, inplace=True)
            size_distribution_run["run_id"] = run_id
            size_distribution_run.loc[(size_distribution_run.date < t_expansion), "run_modus"] = 1
            size_distribution_run.loc[
                (size_distribution_run.date >= t_expansion)
                & (size_distribution_run.date < t_refill),
                "run_modus",
            ] = 2
            size_distribution_run.loc[(size_distribution_run.date >= t_refill), "run_modus"] = 3
            opc_run = file
            if len(opc_run) == 0:
                size_distribution_run["opc_n"] = 0
                size_distribution_run[self.distribution_bins] = 0
            else:
                opc_run.reset_index(drop=True, inplace=True)
                opc_run["t_rel"] = (opc_run.date - self.ref_time).dt.total_seconds()
                opc_run.drop(["date", "p_l"], axis=1, inplace=True)
                run_group = (
                    opc_run.groupby(
                        pd.cut(opc_run.t_rel, size_distribution_run.t_start, right=False),
                        observed=False,
                    )["d"]
                    .apply(lambda x: self.get_hist_data(x, grid))
                    .to_frame()
                )
                run_group = pd.DataFrame(
                    run_group["d"].to_list(), columns=self.distribution_bins, index=run_group.index
                )
                run_group.reset_index(drop=True, inplace=True)
                size_distribution_run["opc_n"] = run_group.sum(axis=1)
                size_distribution_run[self.distribution_bins] = run_group
            return size_distribution_run.copy()

    def get_hist_data(self, opc_run, grid):
        """
        Applies the histogram grid to data.

        Parameters
        ----------
        opc_run : series
            particle diameter
        grid : dataframe
            logarithmic grid for binning the data

        Returns
        -------
            histogram data
        """
        dp_sorted = np.log10(np.sort(opc_run.to_numpy()))
        hist, bins = np.histogram(dp_sorted.tolist(), bins=grid.dlog.tolist(), density=False)
        del dp_sorted, bins
        return np.append(hist, 0)


def main(campaign, pine_id, operation_id, calib_file_template, path_analysis, pfr_file, ref_time):
    small_bins = SmallerBins(
        calib_file_template, pine_id, campaign, operation_id, path_analysis, pfr_file, ref_time
    )
    return small_bins.size_distribution
