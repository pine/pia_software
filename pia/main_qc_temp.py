# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2023 IMK-AAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

from qc_temperature_sensors import QCTempSensors

campaign = "CORONA_new"
pine_id = "04-02"

if __name__ == "__main__":
    qc = QCTempSensors(campaign, pine_id)
