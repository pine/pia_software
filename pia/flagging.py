# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""


import logging
import os
import warnings
from collections import Counter

import numpy as np
import pandas as pd
from saqc import SaQC
from saqc.constants import FILTER_ALL

import pia.create_smaller_bins as create_smaller_bins
from pia.level_data import EvalData
from pia.saqc_actrisflags import ACTRISScheme

pd.set_option("future.no_silent_downcasting", True)
warnings.filterwarnings("ignore", message="Column", category=RuntimeWarning)


class Flagging(EvalData):
    """
    Performs QA/QC tests on the instrument and meta data and creates a
    log-file with warnings.

    Parameters
    -----------
    lev0_data : instance of class Lev0Data
    lev1_data : instance of class Lev1Data
    cirrus_mode : bool
        whether the measurements were made in cirrus regime or not
    """

    def __init__(self, lev0_data, lev1_data, cirrus_mode):
        super().__init__(lev0_data)
        self.calib_dict = lev0_data.calib_dict
        if cirrus_mode is False:
            self.ice_threshold = lev1_data.ice_threshold
        self.settings = lev0_data.settings
        self.structure = lev0_data.structure
        self.inp_cn_flush = lev1_data.df_flush["cn_ice"]
        self.inp_cn_exp = lev1_data.df_exp["cn_ice_0"]
        self.size_distribution = lev1_data.size_distribution
        self.expansion_data = lev1_data.df_exp
        self.calib_file_name = lev0_data.calib_file_name
        self.ref_time = lev0_data.ref_time
        self.assign_run_mode()
        self.distribution_bins = lev1_data.distribution_bins
        self.flags_metadata, self.flags_instrument = self.qc_tests(cirrus_mode)
        self.create_log_file()

    def assign_run_mode(self):
        """
        Assigns run id and run mode (1,2,3) to instrument data according to
        run times.

        """
        self.instrument_data["run_id"] = np.nan
        self.instrument_data["run_mode"] = np.nan
        if self.pine_id.startswith("air"):
            for run_id in range(1, self.num_runs + 1):
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file.loc[run_id, "time start"])
                    & (self.instrument_data.date < self.pfr_file.loc[run_id, "time end"]),
                    "run_id",
                ] = run_id
                self.instrument_data.loc[
                    (
                        (self.instrument_data.date >= self.pfr_file.loc[run_id, "time start"])
                        & (self.instrument_data.date < self.pfr_file.loc[run_id, "time idle"])
                    )
                    | (
                        (self.instrument_data.date >= self.pfr_file.loc[run_id, "time flush"])
                        & (self.instrument_data.date < self.pfr_file.loc[run_id, "time end"])
                    ),
                    "run_mode",
                ] = 1
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file.loc[run_id, "time expansion"])
                    & (self.instrument_data.date < self.pfr_file.loc[run_id, "time refill"]),
                    "run_mode",
                ] = 2
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file.loc[run_id, "time refill"])
                    & (self.instrument_data.date < self.pfr_file.loc[run_id, "time flush"]),
                    "run_mode",
                ] = 3
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file.loc[run_id, "time idle"])
                    & (self.instrument_data.date < self.pfr_file.loc[run_id, "time expansion"]),
                    "run_mode",
                ] = 4
        else:
            for run_id in range(1, self.num_runs + 1):
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file["time start"][run_id])
                    & (self.instrument_data.date < self.pfr_file["time end"][run_id]),
                    "run_id",
                ] = run_id
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file["time start"][run_id])
                    & (self.instrument_data.date < self.pfr_file["time expansion"][run_id]),
                    "run_mode",
                ] = 1
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file["time expansion"][run_id])
                    & (self.instrument_data.date < self.pfr_file["time refill"][run_id]),
                    "run_mode",
                ] = 2
                self.instrument_data.loc[
                    (self.instrument_data.date >= self.pfr_file["time refill"][run_id])
                    & (self.instrument_data.date < self.pfr_file["time end"][run_id]),
                    "run_mode",
                ] = 3

    def qc_tests(self, cirrus_mode):
        """
        QA/QC tests on instrument data.

        Parameters
        ----------
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not

        Returns
        -------
        flags_run : pd.DataFrame
            flags for each run
        flags_data : pd.DataFrame
            flags for each data point
        """
        # get data
        data = pd.concat(self.size_distribution).drop(
            columns=["t_start", "date_end", "t_rel", "n_ice", "flow", "cn_ice"]
            + self.distribution_bins,
            errors="ignore",
        )
        data = data.drop_duplicates(subset=["date"], keep="first")
        data.reset_index(inplace=True, drop=True)
        # convert flow during expansion to normalized flow
        data.loc[data.run_modus == 2, "Fe"] = data.Fe * 1013 / data.Pch * data.Ti5 / 273.15

        # select run modi for flow outlier test
        data_flow = data[["run_modus", "Fe", "Fm"]].copy()
        if self.pine_id == "AIDAm":
            data_flow.Fm = data_flow.Fe
            data_flow.loc[(data_flow.run_modus == 2) | (data_flow.run_modus == 3), "Fm"] = np.nan
        else:
            data_flow.loc[data_flow.run_modus == 3, "Fm"] = np.nan
        data_flow.loc[data_flow.run_modus != 2, "Fe"] = np.nan

        # select temperatures during expansion for flatline test
        data_temp = data.loc[data.run_modus == 2, ["Ti1", "Ti2", "Ti3", "Ti4", "Ti5"]]
        # convert flow during expansion to normalized flow
        data.loc[data.run_modus == 2, "Fe"] = data.Fe * 1013 / data.Pch * data.Ti5 / 273.15
        # manipulate run modus for gap test in opc data
        # bins are removed from expansion modus if they are empty and at
        # beginning of the expansion or the end
        data["run_modus_corr"] = data.run_modus
        for x in range(2):
            # empty bins during expansion
            no_opc = data.copy()[(data.run_modus == 2) & (data.opc_n == 0)]
            for i in no_opc.index:
                if (data.loc[i - 1, "run_modus_corr"] == 1) & (
                    data.loc[i - 2, "run_modus_corr"] == 1
                ):
                    data.loc[i, "run_modus_corr"] = 1
                elif (data.loc[i + 1, "run_modus_corr"] == 3) & (
                    data.loc[i + 2, "run_modus_corr"] == 3
                ):
                    data.loc[i, "run_modus_corr"] = 3
                elif data.iloc[i + 1 : i + 6]["opc_n"].mean() < 5:
                    data.loc[i, "run_modus_corr"] = 3
                else:
                    if i < 5:
                        if data.iloc[0:i]["opc_n"].mean() < 5:
                            data.loc[i, "run_modus_corr"] = 1
                    else:
                        if data.iloc[i - 5 : i]["opc_n"].mean() < 5:
                            data.loc[i, "run_modus_corr"] = 1

        data["opc_cn"] = np.nan
        for run_id in self.expansion_data.index:
            data.loc[data["run_id"] == run_id, "opc_cn"] = self.expansion_data.loc[
                run_id, "opc_cn_0"
            ]
        # Convert data to SaQC instance for QA/QC
        flow_qc = SaQC(data_flow, scheme=ACTRISScheme())
        del data_flow
        data_qc = SaQC(data, scheme=ACTRISScheme())
        temp_qc = SaQC(data_temp, scheme=ACTRISScheme())
        # Tests for each data point
        # Pressure
        data_qc = (
            data_qc.flagMissing("Pch", target="Pch_range", flag=999)
            .flagRange(
                "Pch",
                target="Pch_range",
                min=self.settings["flagging"]["PRES_MIN"],
                max=self.settings["flagging"]["PRES_MAX"],
                flag=684,
            )
            .flagConstants(
                "Pch",
                target="Pch_flatline",
                window=self.settings["flagging"]["POINTS"],
                thresh=0,
                flag=699,
            )
        )

        # Flow
        if self.pine_id == "AIDAm":
            f_min = self.settings["flagging"]["FLOW_MIN_AIDAM"]
            f_max = self.settings["flagging"]["FLOW_MAX_AIDAM"]
        else:
            f_min = self.settings["flagging"]["FLOW_MIN_PINE"]
            f_max = self.settings["flagging"]["FLOW_MAX_PINE"]
        if self.pine_id == "AIDAm":
            f_zero_min = -0.01
            f_zero_max = 0.01
        elif self.pine_id == "1B":
            f_zero_min = 0
            f_zero_max = 0.1
        else:
            f_zero_min = 0
            f_zero_max = 0.025
        # fmt: off
        if self.pine_id.startswith("air"):
            flow_qc = flow_qc.flagZScore("Fe", target="Fe_outlier", flag=459)
            data_qc = (
                data_qc.flagMissing("Fe", target="Fe_missing", flag=999)
                .flagRange("Fe", min=f_min, max=f_max, target="Fe_1"))
        else:
            flow_qc = (
                flow_qc.flagZScore("Fe", target="Fe_outlier", flag=459)
                .flagZScore("Fm", target="Fm_outlier", flag=460)
            )
            data_qc = (
                data_qc.flagMissing("Fe", target="Fe_missing", flag=999)
                .flagRange("Fe", min=f_min, max=f_max, target="Fe_1")
                .flagRange("Fe", min=f_zero_min, max=f_zero_max, target="Fe_zero")
            )
        # fmt: on
        if self.pine_id == "AIDAm":
            data_qc = data_qc.andGroup(
                ["Fe_1", "not_refill"],
                flag=686,
                target="Fe_range",
                group=[
                    data_qc.flagRange("Fe", min=f_min, max=f_max, target="Fe_1"),
                    data_qc.flagGeneric("run_modus", func=lambda x: x != 3, target="not_refill"),
                ],
                dfilter=FILTER_ALL,
            ).andGroup(
                ["Fe_zero", "refill"],
                flag=688,
                target="Fe_range_zero",
                group=[
                    data_qc.flagRange("Fe", min=f_zero_min, max=f_zero_max, target="Fe_zero"),
                    data_qc.flagGeneric("run_modus", func=lambda x: x == 3, target="refill"),
                ],
                dfilter=FILTER_ALL,
            )
        elif self.pine_id.startswith("air"):
            data_qc = (
                data_qc.flagMissing("Fe", target="Fe_range", flag=999)
                .andGroup(
                    ["Fe_1", "expansion"],
                    flag=686,
                    target="Fe_range",
                    group=[
                        data_qc.flagRange("Fe", min=f_min, max=f_max, target="Fe_1"),
                        data_qc.flagGeneric("run_modus", func=lambda x: x == 2, target="expansion"),
                    ],
                    dfilter=FILTER_ALL,
                )
                .andGroup(
                    ["Fe_zero", "not_expansion"],
                    target="Fe_range_zero",
                    flag=688,
                    group=[
                        data_qc.flagRange("Fe", min=f_zero_min, max=f_zero_max, target="Fe_zero"),
                        data_qc.flagGeneric(
                            "run_modus", func=lambda x: x != 2, target="not_expansion"
                        ),
                    ],
                    dfilter=FILTER_ALL,
                )
            )
        else:
            data_qc = (
                data_qc.flagMissing("Fm", target="Fm_missing", flag=999)
                .andGroup(
                    ["Fe_1", "expansion"],
                    flag=686,
                    target="Fe_range",
                    group=[
                        data_qc.flagRange("Fe", min=f_min, max=f_max, target="Fe_1"),
                        data_qc.flagGeneric("run_modus", func=lambda x: x == 2, target="expansion"),
                    ],
                    dfilter=FILTER_ALL,
                )
                .andGroup(
                    ["Fm", "not_refill"],
                    flag=687,
                    target="Fm_range",
                    group=[
                        data_qc.flagRange("Fm", min=f_min, max=f_max),
                        data_qc.flagGeneric(
                            "run_modus", func=lambda x: x != 3, target="not_refill"
                        ),
                    ],
                    dfilter=FILTER_ALL,
                )
                .andGroup(
                    ["Fe_zero", "not_expansion"],
                    flag=688,
                    target="Fe_range_zero",
                    group=[
                        data_qc.flagRange("Fe", min=f_zero_min, max=f_zero_max, target="Fe_zero"),
                        data_qc.flagGeneric(
                            "run_modus", func=lambda x: x != 2, target="not_expansion"
                        ),
                    ],
                    dfilter=FILTER_ALL,
                )
            )
        # Dew Point
        if self.pine_id == "01-B":
            data_qc = data_qc.flagRange(
                "DP",
                target="DP_range",
                min=self.settings["flagging"]["DP_MIN"],
                max=self.settings["flagging"]["DP_MAX"],
                flag=685,
            ).flagConstants(
                "DP",
                target="DP_flatline",
                flag=698,
                window=self.settings["flagging"]["POINTS"],
                thresh=0,
            )
        else:
            data_qc = (
                data_qc.flagMissing("DP", target="DP_range", flag=999)
                .flagRange(
                    "DP",
                    target="DP_range",
                    min=self.settings["flagging"]["DP_MIN"],
                    max=self.settings["flagging"]["DP_MAX"],
                    flag=685,
                )
                .flagConstants(
                    "DP",
                    target="DP_flatline",
                    flag=698,
                    window=self.settings["flagging"]["POINTS"],
                    thresh=0,
                )
            )
        # concentration
        # fmt: off
        data_qc = (
            data_qc
            .flagMissing("opc_n", target="concentration_missing", flag=997)
            .flagRange("opc_cn_bin", target="concentration_range", min=0, max=700000, flag=461)
        )
        # fmt: on
        # Temperature
        if self.pine_id == "AIDAm":
            temp_min = self.settings["flagging"]["TEMP_MIN_AIDAM"]
        else:
            temp_min = self.settings["flagging"]["TEMP_MIN"]
        temp_max = self.settings["flagging"]["TEMP_MAX"]
        for i in range(1, 6):
            if (i not in [2, 4]) | (not data.Ti2.isnull().all()):
                # fmt: off
                data_qc = (
                    data_qc
                    .flagMissing(f"Ti{i}", target=f"Ti{i}_range", flag=999)
                    .flagRange(
                        f"Ti{i}", target=f"Ti{i}_range", flag=688+i, min=temp_min, max=temp_max
                    )
                )
                # fmt: on
                temp_qc = temp_qc.flagConstants(
                    f"Ti{i}",
                    target=f"Ti{i}_flatline",
                    flag=699 + i,
                    thresh=0,
                    window=self.settings["flagging"]["POINTS_TEMP"],
                )
        # fmt: off
        for i in range(1, 4):
            data_qc = (
                data_qc
                .flagMissing(f"Tw{i}", target=f"Tw{i}_range", flag=999)
                .flagRange(f"Tw{i}", target=f"Tw{i}_range", flag=693+i, min=temp_min, max=temp_max)
            )
        # fmt: on
        # particles in refill
        data_qc = data_qc.andGroup(
            ["opc_n", "refill"],
            flag=710,
            target="opc_refill",
            group=[
                data_qc.flagGeneric("opc_n", func=lambda x: x > 0),
                data_qc.flagGeneric("run_modus", func=lambda x: x == 3, target="refill"),
            ],
            dfilter=FILTER_ALL,
        )
        # tests with 1 data point per run
        run_data = pd.DataFrame(index=self.pfr_file.index)

        # Target values
        p_set = self.pfr_file["end pressure"].replace(0, np.nan)
        fe_set = self.pfr_file["flow expansion"].replace(0, np.nan)
        if not self.pine_id.startswith("air"):
            fm_set = self.pfr_file["flow flush"].replace(0, np.nan)
            run_data["fm_range_min"] = fm_set * (
                1 - self.settings["flagging"]["MEAN_FLOW_DEV"] / 100
            )
            run_data["fm_range_max"] = fm_set * (
                1 + self.settings["flagging"]["MEAN_FLOW_DEV"] / 100
            )
        run_data["p_range_min"] = p_set * (1 - self.settings["flagging"]["DEV_END_PRES"] / 100)
        run_data["p_range_max"] = p_set * (1 + self.settings["flagging"]["DEV_END_PRES"] / 100)
        run_data["fe_range_min"] = fe_set * (1 - self.settings["flagging"]["MEAN_FLOW_DEV"] / 100)
        run_data["fe_range_max"] = fe_set * (1 + self.settings["flagging"]["MEAN_FLOW_DEV"] / 100)
        # duration of expansion
        atm_pressure = (
            self.instrument_data[["Pch", "run_id", "run_mode"]]
            .groupby(["run_id", "run_mode"])
            .tail(1)
        )
        atm_pressure = (
            atm_pressure[atm_pressure.run_mode == 1]
            .set_index("run_id", drop=True)
            .rolling(window=10, center=True, min_periods=1)
            .mean()
        )
        if self.pine_id == "01-A":
            dur_expansion_calc = ((atm_pressure.Pch / p_set) * 6.21 - 6.21) / fe_set * 60
        elif self.pine_id.startswith("air"):
            dur_expansion_calc = ((atm_pressure.Pch / p_set) * 3 - 3) / fe_set * 60
        else:
            dur_expansion_calc = ((atm_pressure.Pch / p_set) * 10 - 10) / fe_set * 60
        run_data["dur_expansion_icing"] = dur_expansion_calc.round(2)
        # find minimum temperature and set lower threshold for
        # duration of expansion
        min_temperatures = data[["run_id", "Ti5"]].groupby("run_id").min()
        run_data["dur_expansion_min"] = [
            self.settings["flagging"]["DUR_MIN_PINE_LT_35"]
            if temp < 239
            else self.settings["flagging"]["DUR_MIN_PINE_GT_35"]
            for temp in min_temperatures.Ti5
        ]
        # measured values
        # pressure, temperature and flow
        start_expansion = (
            self.instrument_data[["Pch", "Ti5", "DP", "run_id", "run_mode"]]
            .groupby(["run_id", "run_mode"])
            .tail(1)
        )
        start_expansion = start_expansion[start_expansion.run_mode == 1].set_index(
            "run_id", drop=True
        )
        run_data[["Ti5_start", "DP_start"]] = start_expansion[["Ti5", "DP"]]
        end_values = (
            data[["run_id", "run_modus", "Pch", "Ti1", "Ti2", "Ti3", "Ti4", "Ti5"]]
            .groupby(["run_id", "run_modus"])
            .tail(1)
        )
        end_values = end_values[end_values.run_modus == 2].set_index("run_id", drop=False)
        run_data["pch_end"] = end_values["Pch"]
        run_data[["Ti1_end", "Ti2_end", "Ti3_end", "Ti4_end", "Ti5_end"]] = end_values[
            ["Ti1", "Ti2", "Ti3", "Ti4", "Ti5"]
        ]
        group_fe = data[["run_id", "run_modus", "Fe"]].groupby(["run_id", "run_modus"]).median()
        fe_mean = group_fe.iloc[group_fe.index.get_level_values("run_modus") == 2].droplevel(
            "run_modus"
        )
        run_data["Fe"] = fe_mean["Fe"]
        if self.pine_id == "AIDAm":
            fm_mean = group_fe.iloc[group_fe.index.get_level_values("run_modus") == 1].droplevel(
                "run_modus"
            )
            run_data["Fm"] = fm_mean["Fe"]
        elif not self.pine_id.startswith("air"):
            data_wo_refill = data[data.run_modus != 3]
            fm_mean = data_wo_refill[["run_id", "Fm"]].groupby("run_id").median()
            run_data["Fm"] = fm_mean["Fm"]
        # duration of expansion calculated
        atm_pressure = start_expansion.rolling(window=10, center=True, min_periods=1).mean()
        if self.pine_id == "01-A":
            dur_expansion_calc = ((atm_pressure.Pch / p_set) * 6.21 - 6.21) / fe_set * 60
        else:
            dur_expansion_calc = ((atm_pressure.Pch / p_set) * 10 - 10) / fe_set * 60
        run_data["dur_expansion_icing"] = dur_expansion_calc.round()
        run_data["time expansion"] = pd.to_datetime(self.pfr_file["time expansion"])
        run_data["time refill"] = pd.to_datetime(self.pfr_file["time refill"])
        run_data["time start"] = self.pfr_file["time start"]
        run_data["time end"] = self.pfr_file["time end"]
        run_data["dur_expansion"] = (
            run_data["time refill"] - run_data["time expansion"]
        ).dt.total_seconds()
        # ice threshold
        if not cirrus_mode:
            run_data["ice_threshold"] = self.ice_threshold["d"]
            d_ice_mean = run_data.loc[:, "ice_threshold"].mean()
            d_ice_std = run_data.loc[:, "ice_threshold"].std()
            d_ice_range = [d_ice_mean - (2 * d_ice_std), d_ice_mean + (2 * d_ice_std)]
        # particles in flush
        run_data["inp_cn_flush"] = self.inp_cn_flush
        run_data["inp_cn_exp"] = self.inp_cn_exp
        # INP concentration
        run_data["ice_concentration"] = self.expansion_data["cn_ice_0"]
        # create saqc instance
        run_data_qc = SaQC(run_data, scheme=ACTRISScheme())
        # tests
        # run times
        run_data_qc = run_data_qc.flagGeneric(
            field=["time start", "time expansion", "time refill", "time end"],
            target="increasing_time",
            flag=670,
            func=lambda t1, t2, t3, t4: (t1 >= t2) | (t2 >= t3) | (t3 >= t4),
        )
        # ice threshold
        if not cirrus_mode:
            run_data_qc = run_data_qc.flagGeneric(
                "ice_threshold",
                target="ice_threshold_2_sigma",
                flag=463,
                func=lambda x: (x <= d_ice_range[0]) | (x >= d_ice_range[1]),
            )
        # ice particles in flush
        run_data_qc = run_data_qc.flagGeneric(
            ["inp_cn_flush", "inp_cn_exp"],
            target="inp_flush",
            flag=711,
            func=lambda cninp_flush, cninp_exp: (
                cninp_flush > (self.settings["flagging"]["THRESH_INP_FLUSH"] * cninp_exp)
            ),
        )
        # INP concentrations for stripes
        run_data_qc = run_data_qc.flagZScore(
            "ice_concentration", method="modified", center=False, thresh=2, flag=464
        )
        # pressure and flow
        run_data_qc = run_data_qc.flagGeneric(
            ["pch_end", "p_range_min", "p_range_max"],
            target="p_end",
            flag=709,
            func=lambda p, p_min, p_max: (p < p_min) | (p > p_max),
        ).flagGeneric(
            ["Fe", "fe_range_min", "fe_range_max"],
            target="fe_mean",
            flag=664,
            func=lambda f, f_min, f_max: (f < f_min) | (f > f_max),
        )
        if not self.pine_id.startswith("air"):
            run_data_qc = run_data_qc.flagGeneric(
                ["Fm", "fm_range_min", "fm_range_max"],
                target="fm_mean",
                flag=665,
                func=lambda f, f_min, f_max: (f < f_min) | (f > f_max),
            )
        # temperature
        if (self.pine_id in ["01-A", "01-B", "3", "AIDAm"]) | (self.pine_id.startswith("air")):
            for i in [1, 3]:
                run_data_qc = run_data_qc.flagGeneric(
                    [f"Ti{i}_end", f"Ti{i+2}_end"],
                    target=f"Ti{i}/Ti{i+2}_diff",
                    flag=634 + i,
                    func=lambda Ti1, Ti2: (Ti2 > Ti1),
                )
        else:
            for i in range(1, 5):
                run_data_qc = run_data_qc.flagGeneric(
                    [f"Ti{i}_end", f"Ti{i+1}_end"],
                    target=f"Ti{i}/Ti{i+1}_diff",
                    flag=634 + i,
                    func=lambda Ti1, Ti2: (Ti2 > Ti1),
                )
        # duration of expansion
        if not self.pine_id.startswith("A"):
            run_data_qc = run_data_qc.flagGeneric(
                ["dur_expansion", "dur_expansion_min"],
                target="dur_expansion",
                flag=708,
                func=lambda t, t_min: (t < t_min) | (t > self.settings["flagging"]["DUR_MAX"]),
            )
            if self.structure < 6:
                run_data_qc = run_data_qc.flagGeneric(
                    ["dur_expansion", "dur_expansion_icing"],
                    target="dur_expansion",
                    flag=677,
                    func=lambda t, t_min: (t < t_min),
                )
        else:
            run_data_qc = run_data_qc.flagRange(
                "dur_expansion",
                flag=708,
                min=self.settings["flagging"]["DUR_MIN_AIDAM"],
                max=self.settings["flagging"]["DUR_MAX"],
            )
        # supersaturation
        run_data_qc = run_data_qc.flagGeneric(
            ["Ti5_start", "DP_start"],
            target="supersaturation",
            flag=549,
            func=lambda t, dp: t > dp,
        )
        # create DFs for flags
        flags_for_all = [
            "Pch_range",
            "Pch_flatline",
            "Fe_range",
            "Fe_missing",
            "Fe_range_zero",
            "DP_range",
            "DP_flatline",
            "concentration_missing",
            "concentration_range",
            "opc_refill",
        ] + [f"Tw{i}_range" for i in range(1, 4)]
        if self.pine_id in ["01-A", "01-B", "3"]:
            columns_data = (
                flags_for_all
                + ["Fm_range", "Fm_missing"]
                + [f"Ti{i}_range" for i in range(1, 6, 2)]
            )
            columns_temp = [f"Ti{i}_flatline" for i in range(1, 6, 2)]
        elif self.pine_id.startswith("air") | (self.pine_name == "AIDAm"):
            columns_data = flags_for_all + [f"Ti{i}_range" for i in [1, 3, 5]]
            columns_temp = [f"Ti{i}_flatline" for i in range(1, 6, 2)]
        else:
            columns_data = (
                flags_for_all + ["Fm_range", "Fm_missing"] + [f"Ti{i}_range" for i in range(1, 6)]
            )
            columns_temp = [f"Ti{i}_flatline" for i in range(1, 6)]
        flags_data = data_qc.flags[columns_data].to_pandas()
        if self.pine_id.startswith("air"):
            columns_flow = ["Fe_outlier"]
        else:
            columns_flow = ["Fe_outlier", "Fm_outlier"]
        flags_flow = flow_qc.flags[columns_flow].to_pandas()
        flags_temp = temp_qc.flags[columns_temp].to_pandas()
        flags_data = pd.concat([flags_data, flags_flow, flags_temp], axis=1)
        flags_data[columns_temp] = flags_data.loc[:, columns_temp].fillna("000")
        if self.pine_id.startswith("air") | (self.pine_name == "AIDAm"):
            flags_w_exceptions = [
                "Fe_outlier",
                "opc_refill",
                "concentration_missing",
                "Fe_range",
                "Fe_range_zero",
            ]
        else:
            flags_w_exceptions = [
                "Fe_outlier",
                "Fm_outlier",
                "opc_refill",
                "concentration_missing",
                "Fe_range",
                "Fm_range",
                "Fe_range_zero",
            ]
        for flag in flags_w_exceptions:
            flagged_data = flags_data.loc[flags_data[flag] != "000", flag]
            if flag == "opc_refill":
                for i in flagged_data.index:
                    if (data.run_modus[i - 1] == 2) | (data.run_modus[i - 2] == 2):
                        flags_data.loc[i, flag] = "000"
            elif flag == "concentration_missing":
                for i in flagged_data.index:
                    if i == len(flags_data) - 1:
                        flags_data.loc[i, flag] = "000"
                    elif (data.run_modus[i] == 3) & (data.run_modus[i + 1] == 1):
                        flags_data.loc[i, flag] = "000"
            else:
                for i in flagged_data.index:
                    if i == 0:
                        flags_data.loc[i, flag] = "000"
                        x = i
                    elif (
                        (data.loc[i - 1, "run_modus"] != data.loc[i, "run_modus"])
                        | (data.loc[i + 1, "run_modus"] == data.loc[i, "run_modus"])
                        | ((i - 1) == x)
                    ):
                        flags_data.loc[i, flag] = "000"
                        x = i
        if self.pine_id in ["01-A", "01-B", "3"]:
            columns_run = [
                "increasing_time",
                "ice_concentration",
                "pch_end",
                "Fe",
                "Fm",
                "dur_expansion",
                "supersaturation",
                "inp_flush",
            ] + [f"Ti{i}/Ti{i+2}_diff" for i in [1, 3]]
        elif self.pine_name == "AIDAm":
            columns_run = [
                "increasing_time",
                "ice_concentration",
                "pch_end",
                "Fe",
                "dur_expansion",
                "supersaturation",
                "inp_flush",
            ] + [f"Ti{i}/Ti{i+2}_diff" for i in [1, 3]]
        elif self.pine_id.startswith("air"):
            columns_run = [
                "increasing_time",
                "inp_flush",
                "pch_end",
                "Fe",
                "dur_expansion",
                "supersaturation",
                "ice_concentration",
            ] + [f"Ti{i}/Ti{i+2}_diff" for i in [1, 3]]
        else:
            columns_run = [
                "increasing_time",
                "ice_concentration",
                "pch_end",
                "Fe",
                "Fm",
                "dur_expansion",
                "supersaturation",
                "inp_flush",
            ] + [f"Ti{i}/Ti{i+1}_diff" for i in range(1, 5)]
        if cirrus_mode is False:
            columns_run += ["ice_threshold_2_sigma"]
        flags_run = run_data_qc.flags[columns_run].to_pandas()

        # if inlet icing is detected for more than 2 runs in a row,
        # flag all runs from run-5
        flagged_runs = list(flags_run.loc[flags_run.dur_expansion == 677].index.values)
        if len(flags_run.loc[flags_run.dur_expansion == 677]) > 2:
            flag_icing = False
            for i, run_id in enumerate(flagged_runs[:-2], 0):
                if (flagged_runs[i + 1] == run_id + 1) & (flagged_runs[i + 2] == run_id + 2):
                    flag_icing = True
                    break
            if flag_icing:
                if run_id <= 10:
                    flags_run["dur_expansion"] = 677
                else:
                    flags_run.loc[run_id - 4 :, "dur_expansion"] = 677
            else:
                flags_run.loc[flags_run.dur_expansion == 677, "dur_expansion"] = "000"
        else:
            flags_run.loc[flags_run.dur_expansion == 677, "dur_expansion"] = "000"

        # check for stripes
        # for outliers in INP concentration: check if more than 50% of
        # measured INPs are occuring in one time bin
        outlier_runs = list(flags_run.loc[flags_run.ice_concentration == 464].index.values)
        for run_id in outlier_runs:
            total_inp = self.expansion_data.loc[run_id, "n_ice_0"]
            max_inp_bin = max(
                self.size_distribution[run_id - 1].loc[
                    self.size_distribution[run_id - 1]["run_modus"] == 2, "n_ice"
                ]
            )
            if (max_inp_bin <= (0.5 * total_inp)) | (max_inp_bin < 5):
                flags_run.loc[run_id, "ice_concentration"] = "000"
            elif (max_inp_bin < (0.6 * total_inp)) & (max_inp_bin < 8):
                flags_run.loc[run_id, "ice_concentration"] = "000"

        # gaps in opc data
        flags_run["opc_data"] = "000"
        # detect gaps in opc data, only for operations
        # with opc_cn > 180 stdL-1
        data_qc = data_qc.flagGeneric(
            ["opc_cn", "opc_cn_bin", "run_modus_corr"],
            flag=999,
            target="opc_data",
            func=lambda cn_all, cn_bin, i: ((i == 2) & (cn_bin == 0) & (cn_all > 180)),
        )
        # if gaps in 3sec binned data, test is repeated with 1sec binned
        # data
        if any(data_qc.flags["opc_data"] == 999):
            index = data_qc.flags["opc_data"][data_qc.flags["opc_data"] == 999].index[0]
            run_w_gap = data.loc[index, "run_id"]
            print(f"Warning: There is a gap in opc data in run {run_w_gap}. 1sec bins are created.")
            self.size_distribution_1s = create_smaller_bins.main(
                self.campaign,
                self.pine_id,
                self.operation_id,
                self.calib_file_name,
                self.path_analysis,
                self.pfr_file,
                self.ref_time,
            )
            data_1s = pd.concat(self.size_distribution_1s)
            data_1s = data_1s.drop_duplicates(subset=["date"], keep="first")
            data_1s.reset_index(inplace=True)
            data_1s["run_modus_corr"] = data_1s.run_modus
            # remove empty bins at the beginning or the end of the expansion
            num_zero_values = len(data_1s[(data_1s.run_modus_corr == 2) & (data_1s.opc_n == 0)])
            num_zero_values_new = num_zero_values - 1
            while num_zero_values_new < num_zero_values:
                no_opc = data_1s[(data_1s.run_modus_corr == 2) & (data_1s.opc_n == 0)]
                for i in no_opc.index:
                    if (i + 1) < len(data_1s):
                        if (data_1s.loc[i - 1, "run_modus_corr"] == 1) & (
                            data_1s.loc[i - 2, "run_modus_corr"] == 1
                        ):
                            data_1s.loc[i, "run_modus_corr"] = 1
                        elif (data_1s.loc[i + 1, "run_modus_corr"] == 3) & (
                            data_1s.loc[i + 2, "run_modus_corr"] == 3
                        ):
                            data_1s.loc[i, "run_modus_corr"] = 3
                        elif data_1s.loc[i - 1 : i - 6, "opc_n"].mean() < 5:
                            data_1s.loc[i, "run_modus_corr"] = 1
                        elif data_1s.loc[i + 1 : i + 6, "opc_n"].mean() < 5:
                            data_1s.loc[i, "run_modus_corr"] = 3
                    else:
                        data_1s.loc[i, "run_modus_corr"] = 3
                num_zero_values = len(no_opc)
                num_zero_values_new = len(
                    data_1s[(data_1s.run_modus_corr == 2) & (data_1s.opc_n == 0)]
                )
            data_1s["opc_cn"] = np.nan
            for run_id in run_data.index:
                data_1s.loc[data_1s["run_id"] == run_id, "opc_cn"] = self.expansion_data.loc[
                    run_id, "opc_cn_0"
                ]
            gap_data_qc = SaQC(
                data_1s[["run_modus_corr", "opc_n", "opc_cn"]], scheme=ACTRISScheme()
            )
            gap_data_qc = gap_data_qc.andGroup(
                ["opc_n", "opc_cn"],
                flag=999,
                target="opc_data",
                group=[
                    gap_data_qc.flagGeneric(
                        ["opc_n", "run_modus_corr"],
                        target="opc_n",
                        func=lambda cn, i: (i == 2) & (cn == 0),
                    ),
                    gap_data_qc.flagRange(["opc_cn"], max=180),
                ],
                dfilter=FILTER_ALL,
            )
            flags_gaps = gap_data_qc.flags.to_pandas()
            gaps_results = pd.concat(
                [data_1s.run_id, data_1s.run_modus, flags_gaps["opc_data"]], axis=1
            )
            for run_id in self.expansion_data.index:
                if gaps_results.loc[gaps_results["run_id"] == run_id, "opc_data"].isin([999]).any():
                    flags_run.loc[run_id, "opc_data"] = 998

        flags_data = pd.concat([data.date, data.run_id, data.run_modus, flags_data], axis=1)
        del data
        flags_data.set_index(["date", "run_id", "run_modus"], inplace=True)
        return flags_run, flags_data

    def create_log_file(self):
        """
        Creates and saves a log file with error messages from QA/QC tests

        """
        # create directory
        flags_instrument = self.flags_instrument.copy()
        flags_metadata = self.flags_metadata.copy()
        os.makedirs(self.path_analysis / "Quality_Control", exist_ok=True)
        # create log file
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

        if self.chamber in [2, 3]:
            logging.basicConfig(
                filename=self.path_analysis
                / f"Quality_Control/{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_flags.log",
                filemode="a",
                format="%(levelname)s:%(message)s",
                level=logging.INFO,
            )
        else:
            logging.basicConfig(
                filename=self.path_analysis
                / f"Quality_Control/{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_flags.log",
                filemode="w",
                format="%(levelname)s:%(message)s",
                level=logging.INFO,
            )
        # read error messages for flags
        flags_dict = self.settings["flag_errors"]
        # change type(key) of flags_dict to int
        flags_dict = dict(zip([int(key) for key in flags_dict.keys()], list(flags_dict.values())))
        mode_dict = {1: "flush", 2: "expansion", 3: "refill"}
        flags_instrument.replace(to_replace="000", value=np.nan, inplace=True)
        flags_instrument.dropna(how="all", inplace=True)
        runs_instrument = flags_instrument.groupby(["run_id", "run_modus"]).apply(
            lambda x: self.unique_values(x)
        )
        flags_metadata.replace(to_replace="000", value=np.nan, inplace=True)
        flags_metadata.dropna(how="all", inplace=True)
        flags_metadata["flags"] = [
            [x for x in list(set(flags_metadata.loc[run_id])) if pd.isnull(x) is False]
            for run_id in flags_metadata.index
        ]

        # write run- and mode-wise error messages
        if self.chamber is None:
            for run_id in range(1, self.num_runs + 1):
                try:
                    for flag in flags_metadata["flags"][run_id]:
                        level = flags_dict.get(flag)[1]
                        if level == "info":
                            logging.info(f"run {run_id}, run: {flags_dict.get(flag)[0]}")
                        elif level == "warning":
                            logging.warning(f"run {run_id}, run: {flags_dict.get(flag)[0]}")
                        elif level == "error":
                            logging.error(f"run {run_id}, run: {flags_dict.get(flag)[0]}")
                except KeyError:
                    pass
                for mode in range(1, 4):
                    try:
                        for flag in runs_instrument.loc[(run_id, mode)]:
                            level = flags_dict.get(flag)[1]
                            modus = mode_dict.get(mode)
                            if level == "info":
                                if (
                                    runs_instrument.loc[(run_id, mode)][flag]
                                    < flags_dict.get(flag)[2]
                                ):
                                    logging.info(
                                        f"run {run_id}, {modus}: {flags_dict.get(flag)[0]}"
                                    )
                                else:
                                    logging.warning(
                                        f"run {run_id}, {modus}: {flags_dict.get(flag)[0]}"
                                    )
                            elif level == "warning":
                                logging.warning(f"run {run_id}, {modus}: {flags_dict.get(flag)[0]}")
                            elif level == "error":
                                logging.error(f"run {run_id}, {modus}: {flags_dict.get(flag)[0]}")
                    except KeyError:
                        pass
        else:
            for run_id in range(1, self.num_runs + 1):
                try:
                    for flag in flags_metadata["flags"][run_id]:
                        level = flags_dict.get(flag)[1]
                        if level == "info":
                            logging.info(
                                f"chamber {self.chamber}, run {run_id}, run: {flags_dict.get(flag)[0]}"
                            )
                        elif level == "warning":
                            logging.warning(
                                f"chamber {self.chamber}, run {run_id}, run: {flags_dict.get(flag)[0]}"
                            )
                        elif level == "error":
                            logging.error(
                                f"chamber {self.chamber}, run {run_id}, run: {flags_dict.get(flag)[0]}"
                            )
                except KeyError:
                    pass
                for mode in range(1, 4):
                    try:
                        for flag in runs_instrument.loc[(run_id, mode)]:
                            level = flags_dict.get(flag)[1]
                            modus = mode_dict.get(mode)
                            if level == "info":
                                if (
                                    runs_instrument.loc[(run_id, mode)][flag]
                                    < flags_dict.get(flag)[2]
                                ):
                                    logging.info(
                                        f"chamber {self.chamber}, run {run_id}, {modus}: "
                                        + f"{flags_dict.get(flag)[0]}"
                                    )
                                else:
                                    logging.warning(
                                        f"chamber {self.chamber}, run {run_id}, {modus}: "
                                        + f"{flags_dict.get(flag)[0]}"
                                    )
                            elif level == "warning":
                                logging.warning(
                                    f"chamber {self.chamber}, run {run_id}, {modus}: "
                                    + f"{flags_dict.get(flag)[0]}"
                                )
                            elif level == "error":
                                logging.error(
                                    f"chamber {self.chamber}, run {run_id}, {modus}: "
                                    + f"{flags_dict.get(flag)[0]}"
                                )
                    except KeyError:
                        pass
        for handler in logging.root.handlers[:]:
            logging.root.removeHandler(handler)

    def export_data(self):
        """
        Exports data-wise and run-wise flags.

        """

        if self.pine_id.startswith("air"):
            path_qc = f"Quality_Control/quality_flags/OPC{self.chamber}"
        else:
            path_qc = "Quality_Control/quality_flags"
        super().create_dir(self.path_analysis / path_qc)
        instrument = self.flags_instrument.reset_index()
        names = [str(col) for col in instrument.columns]
        header = f"software version: {self.version}\n"
        super().export_data(
            path_qc,
            super().create_filename("flags_instrument", chamber=True),
            instrument,
            header + "\t".join(names),
        )
        metadata = self.flags_metadata.reset_index()
        metadata.rename(columns={"index": "Run"}, inplace=True)
        names = [str(col) for col in metadata.columns]
        super().export_data(
            path_qc,
            super().create_filename("flags_metadata", chamber=True),
            metadata,
            header + "\t".join(names),
        )

    def unique_values(self, data):
        """
        Creates as list of unique flags per data set and counts the appearance.

        Parameters
        ----------
        data : pd.DataFrame
            Grouped flags for each run mode

        Returns
        -------
        values_counter : Counter

        """
        values = []
        for index, row in data.iterrows():
            flag = row.unique()
            flag = [x for x in flag if pd.isnull(x) is False]
            values += flag
        values_counter = Counter(values)
        return values_counter
