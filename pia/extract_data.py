# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

import re
import sys
import tomllib
from datetime import datetime, timedelta
from pathlib import Path

import numpy as np
import pandas as pd
from saqc import SaQC

import pia.calibrate_t_sensors as tcalib
from pia.version import version


class ExtractData(object):
    """
    Extracts metadata and provides functions to read raw data and level data.

    Parameters
    ----------
    campaign : str
        campaign name as used for documentation
    pine_id : str
        PINE Id, eg: 01-A, 01-B, 04-01, AIDAm
    operation_id : int
        Operation Id for analysis
    chamber : int, optional
        Number of chamber if multiple chambers are used

    """

    def __init__(self, campaign, pine_id, operation_id, chamber=None):
        if not isinstance(campaign, str):
            raise TypeError("campaign must be of type string")
        if not isinstance(pine_id, str):
            raise TypeError("pine_id must be of type string")
        if not isinstance(operation_id, int):
            raise TypeError("operation_id must be integer")
        self.chamber = chamber
        self.version = version
        self.campaign = campaign
        self.pine_id = pine_id
        if pine_id.startswith("air"):
            self.pine_name = "PINE" + pine_id
        elif pine_id == "AIDAm":
            self.pine_name = pine_id
        else:
            self.pine_name = "PINE-" + pine_id
        self.operation_id = operation_id
        if self.pine_id.startswith("air"):
            settings_file = "settings_analysis_flagging_PINEair.toml"
        else:
            settings_file = "settings_analysis_flagging.toml"
        with open(Path(__file__).parent.joinpath(f"settings/{settings_file}"), "rb") as f:
            self.settings = tomllib.load(f)
        if "unittest" not in campaign:
            with open(
                Path(__file__).parents[1].joinpath(f"settings_{self.pine_name}_{campaign}.toml"),
                "rb",
            ) as f:
                self.metadata = tomllib.load(f)
            self.path_campaign = Path(self.metadata["paths"]["DATA_PATH"])
            path_analysis = self.metadata["paths"]["ANALYSIS_PATH"]
            if path_analysis == 0:
                self.path_analysis = self.path_campaign
            else:
                self.path_analysis = Path(path_analysis)
        else:
            with open(
                Path(__file__)
                .parents[1]
                .joinpath(
                    f"tests/test_data/{self.pine_name}/{campaign}/"
                    + f"settings_{self.pine_name}_{campaign}.toml"
                ),
                "rb",
            ) as f:
                self.metadata = tomllib.load(f)
            self.path_campaign = (
                Path(__file__).parents[1].joinpath(f"tests/test_data/{self.pine_name}/{campaign}")
            )
            self.path_analysis = self.path_campaign
        self.path_instrument = self.path_campaign / "L0_Data/housekeeping"
        self.path_instrument_new = self.path_campaign / "raw_Data/housekeeping"
        self.config_info = self.config_info()
        self.labview_version = self.config_info[0]
        self.software_type = self.config_info[3]
        self.ref_time = self.set_reference_time()
        self.dt = self.settings["analysis"]["DT"]

    def read_instrument_data(self):
        """
        Reads instrument data and reports time relative to start time
        of operation in minutes.
        Returns
        -------
        instrument_data : pd.DataFrame
        """
        if self.campaign == "PICNIC":
            filename_instrument_old = f"df_PINE-{self.pine_id[1] + self.pine_id[3]}_PDD18_opid-{self.operation_id}_instrument.txt"
        elif self.pine_id == "3":
            filename_instrument_new = (
                f"df_{self.pine_name}_{self.campaign}_opid-{self.operation_id}_instrument.txt"
            )
        else:
            filename_instrument_old = f"df_PINE-{self.pine_id[1]}{self.pine_id[3:]}_{self.campaign}_opid-{self.operation_id}_instrument.txt"
            filename_instrument_new = (
                f"df_{self.pine_name}_{self.campaign}_opid-{self.operation_id}_instrument.txt"
            )
        try:
            instrument_data = pd.read_csv(
                self.path_instrument / filename_instrument_new, sep="\t", header=0, parse_dates=[0]
            )
        except FileNotFoundError:
            try:
                instrument_data = pd.read_csv(
                    self.path_instrument_new / filename_instrument_new,
                    sep="\t",
                    header=0,
                    parse_dates=[0],
                )
            except FileNotFoundError:
                instrument_data = pd.read_csv(
                    self.path_instrument / filename_instrument_old,
                    sep="\t",
                    header=0,
                    parse_dates=[0],
                )
        instrument_data.rename(columns={"date and time": "date"}, inplace=True)
        if len(instrument_data) == 0:
            sys.exit("No data in housekeeping file.")
        instrument_data["t_rel_min"] = (
            instrument_data.date - instrument_data.date[0]
        ).dt.total_seconds() / 60
        instrument_data = instrument_data.loc[:, ~instrument_data.columns.str.contains("^Unnamed")]
        if self.pine_id.startswith("air"):
            tcalib.calibrate_instrument(self.pine_id, instrument_data, self.chamber)
        if self.chamber is None:
            if instrument_data.shape[1] == 11:
                instrument_data["DP"] = np.nan
            elif instrument_data.shape[1] == 12:
                if "Ti0" in instrument_data:
                    instrument_data.rename(
                        columns={"Ti0": "Ti1", "Ti1": "Ti3", "Ti2": "Ti5"}, inplace=True
                    )
                else:
                    instrument_data.rename(columns={"Ti2": "Ti3", "Ti3": "Ti5"}, inplace=True)
                instrument_data.insert(2, "Ti2", np.nan)
                instrument_data.insert(4, "Ti4", np.nan)
            elif self.pine_name == "AIDAm":
                instrument_data.rename(columns={"Ti2": "Ti3", "Ti3": "Ti5"}, inplace=True)
                instrument_data.insert(2, "Ti2", np.nan)
                instrument_data.insert(4, "Ti4", np.nan)
        else:
            columns = [
                "date",
                f"Ti{self.chamber}-1",
                f"Ti{self.chamber}-2",
                f"Ti{self.chamber}-3",
                f"Tw{self.chamber}-1",
                f"Tw{self.chamber}-2",
                f"Tw{self.chamber}-3",
                f"Pch-{self.chamber}",
                "dp",
                "Fe",
                "t_rel_min",
            ]
            instrument_data = instrument_data.loc[:, columns]
            instrument_data.rename(
                columns={
                    f"Ti{self.chamber}-1": "Ti1",
                    f"Ti{self.chamber}-2": "Ti3",
                    f"Ti{self.chamber}-3": "Ti5",
                    f"Tw{self.chamber}-1": "Tw1",
                    f"Tw{self.chamber}-2": "Tw2",
                    f"Tw{self.chamber}-3": "Tw3",
                    f"Pch-{self.chamber}": "Pch",
                    "dp": "DP",
                },
                inplace=True,
            )
            instrument_data.insert(2, "Ti2", np.nan)
            instrument_data.insert(4, "Ti4", np.nan)
        zero_values = self.check_zero_values(instrument_data)
        instrument_data[
            ["Ti1", "Ti2", "Ti3", "Ti4", "Ti5", "Tw1", "Tw2", "Tw3", "DP"]
        ] = instrument_data[["Ti1", "Ti2", "Ti3", "Ti4", "Ti5", "Tw1", "Tw2", "Tw3", "DP"]].apply(
            self.celsius_to_kelvin
        )
        instrument_data.loc[
            zero_values["value"] > 0,
            ["Ti1", "Ti2", "Ti3", "Ti4", "Ti5", "Tw1", "Tw2", "Tw3", "Pch", "Fe", "DP"],
        ] = np.nan
        if self.chamber is None:
            instrument_data.loc[zero_values["value"] > 0, "Fm"] = np.nan
        return instrument_data

    def check_zero_values(self, instrument_data):
        """
        Checks for zero values in all instrument data at the same
        time. Returns flagged data (valid: -inf).

        Parameters
        ----------
        instrument_data : pd.DataFrame

        Returns
        -------
        zero_test.flags : DictOfSeries
        """
        instrument_zero_test = instrument_data.drop(columns=["date", "t_rel_min"])
        zero_test = [
            SaQC(instrument_zero_test[[column]].rename(columns={column: "value"}))
            for column in instrument_zero_test.columns
        ]
        zero_test = zero_test[0].andGroup(
            "value",
            group=[
                zero_test[x].flagGeneric("value", func=lambda x: x == 0)
                for x in range(len(zero_test))
            ],
        )
        return zero_test.flags

    def celsius_to_kelvin(self, T):
        """
        Changes temperatures from celsius to kelvin.

        Parameters
        ----------
        T : float

        Returns
        -------
        T + 273.15 : float
        """
        return T + 273.15

    def data_structure(self, instrument_data):
        """
        Checks which data structure was used from the Labview program
        depending on the date.

        Parameters
        ----------
        instrument_data : pd.DataFrame

        Returns
        -------
        0: for measurements before 2020, date and time in separate columns
        1: for filenames *PINE-40x*/*PINE-1A*
        2: filenames changed to *PINE-04-0x*/*PINE-01-A*/*_opc*
        3: housekeeping data and opc data saved to raw_Data
        4: pfr files saved to raw_Data
        5: opc data stored in subfolders
        6: structure for new labview software
        """
        if self.software_type == "old":
            if self.labview_version in [7.3, 7.4, 7.5]:
                return 5
            else:
                start_date = instrument_data.date[0]
                if self.pine_id.startswith("A"):
                    return None
                elif self.pine_id.startswith("air"):
                    return 4
                elif (self.pine_id == "04-03") & (self.campaign == "Tropic05"):
                    return 1
                elif (self.pine_id == ("c")) | (self.pine_id == ("04-03")):
                    return 3
                elif (self.pine_id == "05-02") | (self.pine_id == "05-01"):
                    if self.campaign == "JFJ_INP_VAL01":
                        return 5
                    else:
                        return 4
                elif (self.pine_id == "04-01") & (
                    self.campaign in ["PaCE22", "PaCE22_2", "Pace23_1"]
                ):
                    return 4
                elif (self.pine_id == "01-A") & (self.campaign == "ExINP_GVB24"):
                    if (start_date > datetime.strptime("2024-04-06 17:49", "%Y-%m-%d %H:%M")) & (
                        start_date < datetime.strptime("2024-04-08 09:23", "%Y-%m-%d %H:%M")
                    ):
                        return 4
                    else:
                        return 5
                elif self.pine_id == "04-02":
                    if start_date < datetime.strptime("2020-11-15", "%Y-%m-%d"):
                        return 1
                    elif (start_date > datetime.strptime("2020-11-15", "%Y-%m-%d")) & (
                        start_date < datetime.strptime("2021-12-18", "%Y-%m-%d")
                    ):
                        return 3
                    elif start_date > datetime.strptime("2021-12-18", "%Y-%m-%d"):
                        return 4
                elif self.pine_id.startswith("04") | self.pine_id.startswith("05"):
                    if start_date < datetime.strptime("2020-11-15", "%Y-%m-%d"):
                        return 1
                    elif (start_date > datetime.strptime("2020-11-15", "%Y-%m-%d")) & (
                        start_date < datetime.strptime("2021-03-01", "%Y-%m-%d")
                    ):
                        return 2
                    elif start_date > datetime.strptime("2021-03-01", "%Y-%m-%d"):
                        return 3
                elif self.pine_id.startswith("01"):
                    if start_date < datetime.strptime("2020-01-01", "%Y-%m-%d"):
                        return 0
                    elif (start_date > datetime.strptime("2020-01-01", "%Y-%m-%d")) & (
                        start_date < datetime.strptime("2021-04-01", "%Y-%m-%d")
                    ):
                        return 1
                    elif start_date > datetime.strptime("2021-02-12", "%Y-%m-%d"):
                        return 3
                elif self.pine_id == "3":
                    return 1
        else:
            if self.labview_version in [3.7, 3.8, 3.12, 3.13]:
                return 6

    def set_reference_time(self):
        """
        Determines the reference time at the start of the operation.

        Returns
        -------
        ref_time : datetime

        """
        pfo_file = pd.read_csv(
            self.path_campaign / f"pfo_{self.pine_name}_{self.campaign}.txt",
            header=0,
            sep="\t",
            usecols=["op_id", "dto_start"],
            index_col="op_id",
            parse_dates=[1],
        )
        ref_time = pfo_file.loc[self.operation_id, "dto_start"]
        if self.pine_name == "AIDAm":
            ref_time = pd.to_datetime(ref_time, format="%Y-%m-%d %H-%M-%S")
        return ref_time

    def config_info(self):
        """
        Reads out the configuration parameters.

        Returns
        -------
        version : float
            labview software version
        opc_id : str
        calib_file_name : str
            name of file for mapping of size bins
        software_type : str
        """
        if "unittest" in self.campaign:
            path_config = self.path_campaign / "operation_setup"
        else:
            path_config = self.path_campaign / "operation setup"
        if self.campaign == "PICNIC":
            filename_config = (
                f"cf_PINE-{self.pine_id[1] + self.pine_id[3]}_PDD18_opid-{self.operation_id}.txt"
            )
        else:
            filename_config = f"cf_{self.pine_name}_{self.campaign}_opid-{self.operation_id}.txt"
        if self.pine_id.startswith("air"):
            return self.config_info_pineair(path_config, filename_config)
        else:
            return self.config_info_pine(path_config, filename_config)

    def config_info_pine(self, path_config, filename_config):
        """
        Configuration parameters for PINE.

        Parameters
        ----------
        path_config : pathlib.PATH
        filename_config : str

        Returns
        -------
        version : float
            labview software version
        opc_id : str
        calib_file_name : str
            name of file for mapping of size bins
        software_type : str

        """
        config_data = pd.read_csv(
            path_config / filename_config, sep="=", header=None, skiprows=3, encoding="latin1"
        )
        config_data = dict(zip(config_data.iloc[:, 0], config_data.iloc[:, 1]))
        try:
            version = float(config_data["sw-version"])
        except KeyError:
            version = None
        try:
            software_type = config_data["sw-type"]
        except KeyError:
            software_type = "old"
        opc_id = config_data["opc-id"]
        if software_type == "new":
            calib_file_name = config_data["size-file"]
        else:
            calib_file_name = config_data["calib-file"]
        return version, opc_id, calib_file_name, software_type

    def config_info_pineair(self, path_config, filename_config):
        """
        Configuration parameters for PINEair.

        Parameters
        ----------
        path_config : path.PATH
        filename_config : str

        Returns
        -------
        version : float
            labview software version
        opc_id : str
        calib_file_name : str
            name of file for mapping of size bins
        software_type : str

        """

        config_data = pd.read_csv(
            path_config / filename_config, sep="=", header=None, skiprows=3, encoding="latin1"
        )
        config_data["opc"] = np.nan
        for i in config_data.index:
            if i == 0:
                config_data.loc[i, "opc"] = 0
            else:
                if config_data.iloc[i, 0].startswith("["):
                    config_data.loc[i, "opc"] = int(config_data.iloc[i, 0].strip("[OPC ]"))
                else:
                    config_data.loc[i, "opc"] = config_data.loc[i - 1, "opc"]
        config_data_group = config_data.groupby("opc")
        for key, group in config_data_group:
            if key == 0:
                general = dict(zip(group.iloc[:, 0], group.iloc[:, 1]))
            elif key == self.chamber:
                opc_config = dict(zip(group.iloc[:, 0], group.iloc[:, 1]))
            else:
                continue
        version = float(general["sw-version"].strip("v "))
        opc_id = opc_config["opc-id"]
        calib_file_name = opc_config["calib-file"]
        try:
            software_type = config_data["sw-type"]
        except KeyError:
            software_type = "old"
        return version, opc_id, calib_file_name, software_type

    def read_pfr_file(self, structure):
        """
        Reads the pfr-file for run times and parameter settings.

        Parameters
        ----------
        structure : int
            structure of raw data

        Returns
        -------
        pfr_file : pd.DataFrame

        """
        if self.campaign == "PICNIC":
            filename_pfr = (
                f"pfr_PINE-{self.pine_id[1] + self.pine_id[3]}_PDD18_opid-{self.operation_id}.txt"
            )
        else:
            if structure is None:
                filename_pfr = f"pfr_AIDAm_{self.campaign}_opid-{self.operation_id}.txt"
            elif (structure > 1) | (self.pine_id == "3"):
                filename_pfr = f"pfr_{self.pine_name}_{self.campaign}_opid-{self.operation_id}.txt"
            elif structure < 2:
                filename_pfr = f"pfr_PINE-{self.pine_id[1]}{self.pine_id[3:]}_{self.campaign}_opid-{self.operation_id}.txt"

        if structure is not None:
            if structure == 0:
                pfr_file = pd.read_csv(
                    self.path_campaign / "L0_Data" / filename_pfr, header=0, sep="\t"
                )
                pfr_file["time start"] = pfr_file.date + " " + pfr_file["time start"]
                pfr_file["time expansion"] = [
                    pfr_file.date[i] + " " + pfr_file["time expansion"][i].split(".")[0]
                    for i in pfr_file.index
                ]
                pfr_file["time refill"] = pfr_file.date + " " + pfr_file["time refill"]
                pfr_file["time end"] = pfr_file.date + " " + pfr_file["time end"]
                pfr_file[["time start", "time expansion", "time refill", "time end"]] = pfr_file[
                    ["time start", "time expansion", "time refill", "time end"]
                ].apply(pd.to_datetime)
            elif structure < 4:
                pfr_file = pd.read_csv(
                    self.path_campaign / "L0_Data" / filename_pfr,
                    header=0,
                    sep="\t",
                    parse_dates=[1, 2, 3, 4],
                )
            elif self.pine_id.startswith("air"):
                pfr_file = pd.read_csv(
                    self.path_campaign / "raw_Data" / filename_pfr,
                    header=0,
                    sep="\t",
                    parse_dates=[2, 3, 4, 5, 6, 10, 11, 12, 13, 17, 18, 19, 20],
                )
            else:
                pfr_file = pd.read_csv(
                    self.path_campaign / "raw_Data" / filename_pfr,
                    header=0,
                    sep="\t",
                    parse_dates=[1, 2, 3, 4],
                )
        else:
            pfr_file = pd.read_csv(
                self.path_campaign / "L0_Data" / filename_pfr,
                header=0,
                parse_dates=[1, 2, 3, 4],
                sep="\t",
            )

        pfr_file = pfr_file.loc[:, ~pfr_file.columns.str.contains("^Unnamed")]
        pfr_file.drop(labels="filter", axis=1, inplace=True, errors="ignore")
        if self.pine_id == "AIDAm":
            filename_valve = f"df_AIDAm_{self.campaign}_opid-{self.operation_id}_valve.txt"
            valves = pd.read_csv(
                self.path_instrument / filename_valve, header=0, parse_dates=[0], sep="\t"
            )
            valve_refill = valves.loc[valves["V_rf"] == 1, :]
            valve_refill.reset_index(inplace=True, drop=True)
            diff_refill = pfr_file["time refill"] - valve_refill["date and time"]
            for i in pfr_file.index:
                if diff_refill[i] >= timedelta(seconds=2):
                    print(
                        "Difference between valve opening and refill time for run "
                        + f"{pfr_file.RUN[i]}, refill time set to valve time."
                    )
                    pfr_file.loc[i, "time refill"] = valve_refill.loc[i, "date and time"]
        elif self.pine_id.startswith("air"):
            if isinstance(pfr_file["end rf 3"][0], datetime):
                chamber_end_time = 3
            elif isinstance(pfr_file["end rf 2"][0], datetime):
                chamber_end_time = 2
            else:
                chamber_end_time = 1

            columns = pfr_file.columns.str.endswith(str(self.chamber))
            if self.chamber == chamber_end_time:
                pfr_file = pd.concat([pfr_file[["RUN", "start"]], pfr_file.loc[:, columns]], axis=1)
            else:
                end_time = f"end rf {chamber_end_time}"
                pfr_file = pd.concat(
                    [pfr_file[["RUN", "start", end_time]], pfr_file.loc[:, columns]], axis=1
                )
            pfr_file.columns = pfr_file.columns.str.strip(str(self.chamber)).str.strip()
            pfr_file.rename(
                columns={
                    "start": "time start",
                    "start exp": "time expansion",
                    "start rf": "time refill",
                    "start idle": "time idle",
                    "Fend": "flow expansion",
                    "Pend": "end pressure",
                    "Fexp": "flow expansion",
                },
                inplace=True,
            )
            if self.chamber == chamber_end_time:
                pfr_file.rename(columns={"end rf": "time end"}, inplace=True)
                pfr_file["time flush"] = pfr_file["time end"]
            else:
                pfr_file.rename(
                    columns={"end rf": "time flush", end_time: "time end"}, inplace=True
                )
        pfr_file.set_index("RUN", inplace=True)
        pfr_file.dropna(axis=0, how="all", inplace=True)
        return pfr_file

    def check_date(self):
        """
        Checks if date and time in pfr file are valid and corrects
        the date if the measurement was running over night.
        Only necesssary for dates before 2020.

        """
        for run_id in range(1, len(self.pfr_file) + 1):
            t_start = self.pfr_file.loc[run_id, "time start"]
            t_expansion = self.pfr_file.loc[run_id, "time expansion"]
            t_refill = self.pfr_file.loc[run_id, "time refill"]
            t_end = self.pfr_file.loc[run_id, "time end"]
            if type(t_start) is np.datetime64:
                t_start = pd.Timestamp(t_start)
                t_expansion = pd.Timestamp(t_expansion)
                t_refill = pd.Timestamp(t_refill)
                t_end = pd.Timestamp(t_end)
            run_t = (t_end - t_start).total_seconds()
            if run_t <= 0:
                if (
                    t_start
                    >= datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ) & (
                    t_expansion
                    < datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ):
                    self.pfr_file.loc[run_id, "time end"] = t_end + timedelta(days=1)
                    self.pfr_file.loc[run_id, "time refill"] = t_refill + timedelta(days=1)
                    self.pfr_file.loc[run_id, "time expansion"] = t_expansion + timedelta(days=1)
                elif (
                    t_start
                    >= datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ) & (
                    t_refill
                    < datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ):
                    self.pfr_file.loc[run_id, "time end"] = t_end + timedelta(days=1)
                    self.pfr_file.loc[run_id, "time refill"] = t_refill + timedelta(days=1)
                elif (
                    t_start
                    >= datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ) & (
                    t_end
                    < datetime.strptime(
                        f"{self.pfr_file.date[run_id]} 23:50:00", "%Y-%m-%d %H:%M:%S"
                    )
                ):
                    self.pfr_file.loc[run_id, "time end"] = t_end + timedelta(days=1)

    def read_concentrations(self, pfr_file, ref_time):
        """
        Function to read Level 1 concentration data.

        Parameters
        ----------
        pfr_file : pd.DataFrame
        ref_time : datetime

        Returns
        -------
        size_distribution : list

        """
        if self.pine_id.startswith("air"):
            path_concentrations = (
                f"L1_Data/exportdata/exportdata_cn/OPC{self.chamber}/OP{self.operation_id}/"
            )
        else:
            path_concentrations = f"L1_Data/exportdata/exportdata_cn/OP{self.operation_id}/"
        size_distribution = []
        for run_id in pfr_file.index:
            if pfr_file.loc[run_id, "time refill"] < pd.to_datetime(
                "1905-01-01", format="%Y-%m-%d"
            ):
                continue
            if self.dt == 3:
                file = (
                    self.path_analysis
                    / path_concentrations
                    / self.create_filename("cn", run_id, chamber=True)
                )
            else:
                file = (
                    self.path_analysis
                    / path_concentrations
                    / self.create_filename(f"cn_{self.dt}sec", run_id, chamber=True)
                )
            with open(file, "r", encoding="latin1") as read_file:
                metadata = read_file.readlines()
                try:
                    version = re.search(r"\d.\d.\d", metadata[0]).group()
                    major_version = int(version.split(".")[0])
                    minor_version = int(version.split(".")[1])
                    patch_version = int(version.split(".")[2])
                except AttributeError:
                    major_version = 0
                t_start = pd.to_datetime(
                    re.search(r"\d\d\d\d-\d\d-\d\d \d\d:\d\d:\d\d", " ".join(metadata[:20])).group()
                )
            if major_version == 0:
                header_rows = 18
            elif major_version == 1:
                if patch_version < 2:
                    header_rows = 19
                else:
                    header_rows = 28
            elif major_version == 2:
                if minor_version == 0:
                    if patch_version == 0:
                        header_rows = 28
                    else:
                        header_rows = 27
                else:
                    header_rows = 27
            else:
                header_rows = 27
            concentrations_run = pd.read_csv(
                file, skiprows=header_rows, header=0, sep="\t", encoding="latin1"
            )
            concentrations_run.rename(
                columns={"cn_bin": "opc_cn_bin", "n": "opc_n", "run_mode": "run_modus"},
                inplace=True,
            )
            concentrations_run[["opc_cn_bin", "cn_ice"]] = concentrations_run[
                ["opc_cn_bin", "cn_ice"]
            ].apply(pd.to_numeric, args=("coerce",))
            run_series = pd.Series(
                [run_id] * len(concentrations_run), index=concentrations_run.index
            ).rename("run_id")
            date_series = pd.Series(
                [
                    t_start + timedelta(seconds=concentrations_run.t_rel[i])
                    for i in range(len(concentrations_run))
                ],
                index=concentrations_run.index,
            ).rename("date")
            t_start_series = (date_series - ref_time).dt.total_seconds().rename("t_start")
            concentrations_run = pd.concat(
                [concentrations_run, run_series, date_series, t_start_series], axis=1
            )
            size_distribution.append(concentrations_run)
        return size_distribution

    def read_ice_data(self):
        """
        Function to read Level 1 ice data.

        Returns
        -------
        ice_data : pd.DataFrame
        operation_type : str

        """
        path_ice = "L1_Data/exportdata/exportdata_ice"
        file = self.path_analysis / path_ice / self.create_filename("ice")
        with open(file, "r", encoding="latin1") as read_file:
            operation_type = read_file.readlines()[8].split(":")[1].strip()
        header_rows = 20
        ice_data = pd.read_csv(
            file, skiprows=header_rows, header=0, sep="\t", encoding="latin1", parse_dates=[1]
        )
        return ice_data, operation_type

    def read_ice_threshold_file(self, manual_ice_threshold, chamber=None):
        """
        Function to read Level 1 ice threshold data.

        Parameters
        ----------
        manual_ice_threshold : bool
            Whether the ice threshold is set manually or not
        chamber : int, optional
            Number of chamber if multiple chambers are used

        Returns
        -------
        ice_threshold : pd.DataFrame
            DataFrame containing the ice threshold for each run

        """
        if self.pine_id.startswith("air"):
            path_ice_threshold = f"L1_Data/exportdata/ice_threshold/OPC{chamber}"
        else:
            path_ice_threshold = "L1_Data/exportdata/ice_threshold"
        if manual_ice_threshold is True:
            ice_threshold = pd.read_csv(
                self.path_analysis
                / path_ice_threshold
                / self.create_filename("ice_threshold_manually", chamber=True),
                header=0,
                sep="\t",
                index_col=[0],
            )
        else:
            ice_threshold = pd.read_csv(
                self.path_analysis
                / path_ice_threshold
                / self.create_filename("ice_threshold", chamber=True),
                header=0,
                sep="\t",
                index_col=[0],
                skiprows=1,
            )
        ice_threshold = ice_threshold.rename(columns={"d_min": "d"})
        return ice_threshold

    def read_qc_data(self):
        """
        Function to read flagged data.

        Returns
        -------
        flags_instrument : pd.DataFrame
        flags_run : pd.DataFrame

        """
        if self.pine_id.startswith("air"):
            path_qc = f"Quality_Control/quality_flags/OPC{self.chamber}"
        else:
            path_qc = "Quality_Control/quality_flags"
        flags_instrument = pd.read_csv(
            self.path_analysis / path_qc / self.create_filename("flags_instrument", chamber=True),
            header=0,
            sep="\t",
            skiprows=1,
            parse_dates=[0],
            index_col=["date", "run_id", "run_modus"],
            dtype="str",
        )
        flags_run = pd.read_csv(
            self.path_analysis / path_qc / self.create_filename("flags_metadata", chamber=True),
            header=0,
            sep="\t",
            skiprows=1,
            index_col="RUN",
            dtype="str",
        )
        return flags_instrument, flags_run

    def create_filename(self, ending, run_id=None, chamber=False):
        """
        Creates filenames for exported data.

        Parameters
        ----------
        ending : str
            individual naming for the files
        run_id : int, optional
            run id for run-wise files
        chamber : int, optional
            number of chamber if multiple chambers are used

        Returns
        -------
        filename : str

        """
        if self.pine_id.startswith("air") & (chamber is True):
            if run_id is None:
                filename = f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_OPC{self.chamber}_{ending}.txt"
            else:
                filename = f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_OPC{self.chamber}_run_id_{run_id}_{ending}.txt"
        else:
            if run_id is None:
                filename = (
                    f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_{ending}.txt"
                )
            else:
                filename = f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_run_id_{run_id}_{ending}.txt"
        return filename
