# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

import numpy as np
import pandas as pd


def make_df_grid_log(num_bins, min_d, max_d):
    """
    Creates a logarithmic grid

    Parameters
    ----------
    num_bins : int
        number of bins
    min_d : float
        smallest bin
    max_d : float
        biggest bin

    Returns
    -------
    grid : pd.DataFrame
        DataFrame with bin sizes
    distribution_bins : list
        list with bin names

    """
    grid_bins = np.arange(0, num_bins, dtype=np.int32)
    grid_dlog = np.linspace(np.log10(min_d), np.log10(max_d), num_bins)
    grid_d = np.power(10, grid_dlog)
    grid = pd.DataFrame.from_dict({"bin": grid_bins, "dlog": grid_dlog, "d": grid_d})
    distribution_bins = [str(round(i, 5)) for i in grid_d]
    del grid_bins, grid_dlog, grid_d
    return grid, distribution_bins


def make_time_grid(run_t, dt):
    """
    Creates a time grid for a time interval of length run_t with bin
    sizes dt

    Parameters
    ----------
    run_t : float
    dt : int

    Returns
    -------
    t_grid : np.array

    """
    if run_t % dt == 0:
        del_t = run_t
        length_t_grid = int(del_t / dt)
        t_grid = np.linspace(0, run_t, length_t_grid + 1)
    else:
        del_t = run_t + (dt - run_t % dt)
        length_t_grid = int(del_t / dt)
        t_grid = np.linspace(0, run_t + dt - (run_t % dt), length_t_grid + 1)
    return t_grid


def choose_time_interval(data_frame, start, stop):
    """
    slices a time interval of a dataframe

    Parameters
    ----------
    data_frame : pd.DataFrame
        needed column: "date"
    start : date
    stop : date

    Returns
    -------
    df : pd.DataFrame
        sliced dataframe

    """
    if start is None:
        df = data_frame[data_frame.date < stop]
    elif stop is None:
        df = data_frame[data_frame.date >= start]
    else:
        df = data_frame[(data_frame.date >= start) & (data_frame.date < stop)]
    return df


def calculate_concentration(flow, count, time_del):
    """
    Concentration in stdL-1

    Parameters
    ----------
    flow : float
    count : int
        particle count
    time_del : float
        time interval

    Returns
    -------
    count / (flow * time_del / 60) : float

    """
    return count / (flow * time_del / 60)


def saturation_pressure_ice(T):
    """
    Calculates the saturation vapour pressure over ice for temperature
    T in Kelvin.

    Parameters
    ----------
    T : float

    Returns
    -------
    p_ice : float

    """
    p_ice = np.exp(9.550426 - 5723.265 / T + 3.53068 * np.log(T) - 0.00728332 * T)
    return p_ice


def saturation_pressure_liquid(T):
    """
    Calculates the saturation vapour pressure over supercooled water
    for temperature in Kelvin.

    Parameters
    ----------
    T : float

    Returns
    -------
    p_liq : float
    """
    p_liq = np.exp(
        54.842763
        - 6763.22 / T
        - 4.21 * np.log(T)
        + 0.000367 * T
        + np.tanh(0.0415 * (T - 218.8))
        * (53.878 - 1331.22 / T - 9.44523 * np.log(T) + 0.014025 * T)
    )
    return p_liq


def Saturation(T_0, T, p_0, p):
    """
    Calculates the saturation ratio with respect to Liquid water,
    assuming ice saturated conditions at T_0, p_0.

    Parameters
    ----------
    T_0 : float
        Temperature at start point in Kelvin
    T : float
        Temperature for calculations in Kelvin
    p_0 : float
        Pressure at start point
    p : float
        Pressure for calculations

    Returns
    -------
    S : float
    """
    S = saturation_pressure_ice(T_0) * p / (p_0 * saturation_pressure_liquid(T))
    return S


def T_adiabatic(T_0, p_0, p):
    """
    Calculates the temperature for an adiabatic cooling.

    Parameters
    ----------
    T_0 : float
        start temperature in Kelvin
    p_0 : float
        start pressure
    p : float
        pressure during cooling

    Returns
    -------
    T_ad : float
    """
    gamma_air = 29.19 / 20.85
    T_ad = T_0 * (p / p_0) ** ((gamma_air - 1) / gamma_air)
    return T_ad
