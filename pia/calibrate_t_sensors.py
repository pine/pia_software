# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2023 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""


def calibrate_pineair(instrument_data, chamber):
    "Calibration functions for T-sensors of PINEair"
    if chamber == 1:
        instrument_data["Ti1-1"] = instrument_data["Ti1-1"] - (
            0.000221 * instrument_data["Ti1-1"] ** 2
            - 0.034785 * instrument_data["Ti1-1"]
            + 0.959544
        )
        instrument_data["Ti1-2"] = instrument_data["Ti1-2"] - (
            0.000215 * instrument_data["Ti1-2"] ** 2 - 0.034668 * instrument_data["Ti1-2"] + 0.9135
        )
        instrument_data["Ti1-3"] = instrument_data["Ti1-3"] - (
            0.000226 * instrument_data["Ti1-3"] ** 2
            - 0.034641 * instrument_data["Ti1-3"]
            + 1.073126
        )
        instrument_data["Tw1-1"] = instrument_data["Tw1-1"] - (
            -0.000037 * instrument_data["Tw1-1"] ** 2
            + 0.003437 * instrument_data["Tw1-1"]
            + 0.101619
        )
        instrument_data["Tw1-2"] = instrument_data["Tw1-2"] - (
            -0.000034 * instrument_data["Tw1-2"] ** 2
            + 0.00102 * instrument_data["Tw1-2"]
            - 0.047541
        )
        instrument_data["Tw1-3"] = instrument_data["Tw1-3"] - (
            -0.000037 * instrument_data["Tw1-3"] ** 2
            + 0.002287 * instrument_data["Tw1-3"]
            - 0.129273
        )
    elif chamber == 2:
        instrument_data["Ti2-1"] = instrument_data["Ti2-1"] - (
            0.000224 * instrument_data["Ti2-1"] ** 2
            - 0.035641 * instrument_data["Ti2-1"]
            + 1.080869
        )
        instrument_data["Ti2-2"] = instrument_data["Ti2-2"] - (
            0.000233 * instrument_data["Ti2-2"] ** 2
            - 0.034979 * instrument_data["Ti2-2"]
            + 1.005806
        )
        instrument_data["Ti2-3"] = instrument_data["Ti2-3"] - (
            0.000221 * instrument_data["Ti2-3"] ** 2
            - 0.036055 * instrument_data["Ti2-3"]
            + 1.048615
        )
        instrument_data["Tw2-1"] = instrument_data["Tw2-1"] - (
            -0.00004 * instrument_data["Tw2-1"] ** 2 + 0.00269 * instrument_data["Tw2-1"] - 0.138028
        )
        instrument_data["Tw2-2"] = instrument_data["Tw2-2"] - (
            -0.000041 * instrument_data["Tw2-2"] ** 2
            + 0.002806 * instrument_data["Tw2-2"]
            - 0.088455
        )
        instrument_data["Tw2-3"] = instrument_data["Tw2-3"] - (
            -0.000039 * instrument_data["Tw2-3"] ** 2 + 0.00198 * instrument_data["Tw2-3"] - 0.18492
        )
    elif chamber == 3:
        instrument_data["Ti3-1"] = instrument_data["Ti3-1"] - (
            0.000232 * instrument_data["Ti3-1"] ** 2
            - 0.035034 * instrument_data["Ti3-1"]
            + 0.963302
        )
        instrument_data["Ti3-2"] = instrument_data["Ti3-2"] - (
            0.000224 * instrument_data["Ti3-2"] ** 2 - 0.03589 * instrument_data["Ti3-2"] + 1.200684
        )
        instrument_data["Ti3-3"] = instrument_data["Ti3-3"] - (
            0.000234 * instrument_data["Ti3-3"] ** 2
            - 0.035895 * instrument_data["Ti3-3"]
            + 1.142779
        )
        instrument_data["Tw3-1"] = instrument_data["Tw3-1"] - (
            -0.000038 * instrument_data["Tw3-1"] ** 2 + 0.00171 * instrument_data["Tw3-1"] - 0.24698
        )
        instrument_data["Tw3-2"] = instrument_data["Tw3-2"] - (
            -0.000025 * instrument_data["Tw3-2"] ** 2
            + 0.005724 * instrument_data["Tw3-2"]
            - 0.030886
        )  # Achtung:Parametrisierung für Tw3-2 & Tw3-3 sind vertauscht!
        instrument_data["Tw3-3"] = instrument_data["Tw3-3"] - (
            -0.000037 * instrument_data["Tw3-3"] ** 2
            + 0.000188 * instrument_data["Tw3-3"]
            - 0.128905
        )


def calibrate_instrument(pine_id, instrument_data, chamber):
    "Calls respective calibration functions"
    if pine_id == "air":
        calibrate_pineair(instrument_data, chamber)
