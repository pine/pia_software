# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2023 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

import glob
import logging
from collections import Counter
from pathlib import Path

import numpy as np
import pandas as pd
import settings
from saqc import SaQC
from saqc_actrisflags import ACTRISScheme

pd.options.mode.chained_assignment = None


class QCTempSensors:
    """
    Checks a whole campaign for outliers in the temperature sensors.

    Parameters
    ----------
    campaign : str
        name of campaign
    pine_id : str
    """

    def __init__(self, campaign, pine_id):
        self.campaign = campaign
        self.pine_id = pine_id
        self.campaign_path = Path(settings.DATA_PATH)
        self.instrument_data = self.get_instrument_data(self.get_logbook())
        self.temp_diff, self.tw_diff, self.std = self.create_diff_t_sensors()
        flags_ti, flags_tw = self.quality_control()
        self.write_into_log_file(flags_ti, flags_tw)

    def get_logbook(self):
        """
        Read logbook for operation type.

        Returns
        -------
        logbook : pd.DataFrame

        """
        try:
            logbook = pd.read_excel(
                self.campaign_path / f"Logbook_{self.campaign}.xlsx",
                usecols=[2, 4],
                header=0,
                names=["operation", "operation type"],
                commentindex_col=0,
                skiprows=1,
            )
        except FileNotFoundError:
            try:
                logbook = pd.read_excel(
                    self.campaign_path / f"Logbook_PINE-{self.pine_id}_{self.campaign}.xlsx",
                    usecols=[2, 4],
                    header=0,
                    names=["operation", "operation type"],
                    index_col=0,
                    skiprows=1,
                )
            except FileNotFoundError:
                logbook = pd.read_excel(
                    self.campaign_path / f"Logbook_{self.campaign}_PINE-1A.xlsx",
                    usecols=[2, 4],
                    header=0,
                    names=["operation", "operation type"],
                    index_col=0,
                    skiprows=1,
                )
            else:
                print("Logbook can't be found")
        logbook = logbook[~logbook.index.duplicated(keep="first")]
        return logbook

    def get_pfr_file(self, instrument_file):
        """
        Read run times from pfr-file.

        Parameters
        ----------
        instrument_file : str
            path and name of instrument-file

        Returns
        -------
        run_times : pd.DataFrame

        """
        pfr_file_old = (
            instrument_file.replace("housekeeping\\df", "\\pfr")
            .replace("_instrument", "")
            .replace("raw_Data", "L0_Data")
        )
        pfr_file_new = instrument_file.replace("housekeeping\\df", "\\pfr").replace(
            "_instrument", ""
        )
        try:
            run_times = pd.read_csv(
                pfr_file_new,
                sep="\t",
                header=0,
                parse_dates=[1, 2, 3, 4],
                usecols=[0, 1, 2, 3, 4],
                index_col="RUN",
            )
        except FileNotFoundError:
            run_times = pd.read_csv(
                pfr_file_old,
                sep="\t",
                header=0,
                parse_dates=[1, 2, 3, 4],
                usecols=[0, 1, 2, 3, 4],
                index_col="RUN",
            )
        return run_times

    def get_instrument_data(self, logbook):
        """
        Get instrument files of whole campaign.

        Parameters
        ----------
        logbook : pd.DataFrame

        Returns
        -------
        all_data_campaign : pd.DataFrame
            instrument data during flush for whole campaign

        """
        instrument_files = glob.glob(
            str(self.campaign_path / "L0_Data/housekeeping/*instrument.txt")
        )
        if len(instrument_files) == 0:
            instrument_files = glob.glob(
                str(self.campaign_path / "raw_Data/housekeeping/*instrument.txt")
            )
        all_data_campaign = []
        for file in instrument_files:
            op_id = int(file.split("-")[-1].split("_")[0])
            try:
                op_type = logbook["operation type"][op_id]
            except KeyError:
                continue
            try:
                op_type = int(op_type[op_type.find("(") + 1])
            except AttributeError:
                continue
            if op_type == 1:
                continue
            instrument_file = pd.read_csv(
                file, header=0, parse_dates=[0], sep="\t", index_col="date and time"
            )
            run_times = self.get_pfr_file(file)
            instrument_file["run_modus"] = np.nan
            instrument_file["run_id"] = np.nan
            instrument_file["op_id"] = op_id
            instrument_file["op_type"] = op_type
            for run in run_times.index:
                time_start = run_times["time start"][run]
                time_end = run_times["time expansion"][run]
                instrument_file.loc[
                    (instrument_file.index >= time_start) & (instrument_file.index < time_end),
                    "run_modus",
                ] = 1
                instrument_file.loc[
                    (instrument_file.index >= time_start) & (instrument_file.index < time_end),
                    "run_id",
                ] = run
            all_data_campaign.append(instrument_file)
        all_data_campaign = pd.concat(all_data_campaign)
        all_data_campaign = all_data_campaign[~all_data_campaign.index.duplicated()]
        all_data_campaign = all_data_campaign[all_data_campaign.run_modus == 1]
        all_data_campaign.sort_index(inplace=True)
        return all_data_campaign

    def create_diff_t_sensors(self):
        """
        Calculate difference between temperature sensors next to each other.

        Returns
        -------
        diff_data : pd.DataFrame
            contains all operations
        tw_diff_data : pd.DataFrame
            contains only operations with constant temperature
        std : pd.DataFrame
            standard deviations for differences,
            if std is < 0.25 std is set to 0.25

        """
        diff_data = pd.DataFrame(index=self.instrument_data.index)
        diff_data["op_id"] = self.instrument_data.op_id
        diff_data["run_id"] = self.instrument_data.run_id
        diff_data["op_type"] = self.instrument_data.op_type
        diff_data["Ti1-Ti2"] = self.instrument_data.Ti1 - self.instrument_data.Ti2
        diff_data["Ti2-Ti3"] = self.instrument_data.Ti2 - self.instrument_data.Ti3
        diff_data["Ti3-Ti4"] = self.instrument_data.Ti3 - self.instrument_data.Ti4
        diff_data["Ti4-Ti5"] = self.instrument_data.Ti4 - self.instrument_data.Ti5
        diff_data["Tw1-Tw2"] = self.instrument_data.Tw1 - self.instrument_data.Tw2
        diff_data["Tw2-Tw3"] = self.instrument_data.Tw2 - self.instrument_data.Tw3
        std = dict()
        std["Ti1-Ti2"] = max((diff_data["Ti1-Ti2"].std(), 0.25))
        std["Ti2-Ti3"] = max((diff_data["Ti2-Ti3"].std(), 0.25))
        std["Ti3-Ti4"] = max((diff_data["Ti3-Ti4"].std(), 0.25))
        std["Ti4-Ti5"] = max((diff_data["Ti4-Ti5"].std(), 0.25))
        tw_diff_data = diff_data.loc[
            diff_data.op_type != 3, ["Tw1-Tw2", "Tw2-Tw3", "run_id", "op_id"]
        ]
        return diff_data, tw_diff_data, std

    def quality_control(self):
        """
        Checks for outliers in difference between temperature sensors.

        Returns
        -------
        flags_data_ti : pd.DataFrame
            flags for gas temperature
        flags_data_tw : pd.DataFrame
            flags for wall temperature

        """
        qc_ti = SaQC(
            self.temp_diff[["Ti1-Ti2", "Ti2-Ti3", "Ti3-Ti4", "Ti4-Ti5"]], scheme=ACTRISScheme()
        )
        qc_tw = SaQC(self.tw_diff[["Tw1-Tw2", "Tw2-Tw3"]], scheme=ACTRISScheme())

        for key, value in self.std.items():
            qc_ti = qc_ti.flagRange(
                key, min=0 - 2 * value, flag=638 + int(key.split("-")[0].split("i")[1])
            )
        for i in [1, 2]:
            qc_tw = qc_tw.flagZScore(f"Tw{i}-Tw{i+1}", thresh=3.5, flag=642 + i)
        flags_data_ti = qc_ti.flags.to_df()
        flags_data_ti[["run_id", "op_id"]] = self.temp_diff[["run_id", "op_id"]]
        flags_data_tw = qc_tw.flags.to_df()
        flags_data_tw[["run_id", "op_id"]] = self.tw_diff[["run_id", "op_id"]]
        return flags_data_ti, flags_data_tw

    def write_into_log_file(self, flags_ti, flags_tw):
        """
        Opens the log file for each operation and adds flags

        Parameters
        ----------
        flags_ti : pd.DataFrame
            flags for gas temperature
        flags_tw : pd.DataFrame
            flags for wall temperature

        """
        flags_ti_group = flags_ti.groupby("op_id")
        flags_tw_group = flags_tw.groupby("op_id")
        flags_dict = settings.flag_errors
        for operation in flags_ti_group.groups.keys():
            ti_data = flags_ti_group.get_group(operation)
            try:
                tw_data = flags_tw_group.get_group(operation)
            except KeyError:
                tw_data = pd.DataFrame(columns=["op_id", "run_id"])
            ti_data.drop(columns=["op_id"], inplace=True)
            tw_data.drop(columns=["op_id"], inplace=True)
            logging.basicConfig(
                filename=self.campaign_path
                / f"Quality_Control/PINE-{self.pine_id}_{self.campaign}_op_id_{operation}_flags.log",
                filemode="a",
                format="%(levelname)s:%(message)s",
                level=logging.WARNING,
            )
            ti_data.replace(to_replace="000", value=np.nan, inplace=True)
            ti_data.set_index("run_id", inplace=True)
            ti_data.dropna(how="all", inplace=True)
            if len(ti_data) == 0:
                runs_ti = pd.DataFrame(index=[1], columns=["Ti"])
                runs_ti["Ti"][1] = {}
            else:
                runs_ti = ti_data.groupby("run_id").apply(lambda x: self.unique_values(x))
            tw_data.replace(to_replace="000", value=np.nan, inplace=True)
            tw_data.set_index("run_id", inplace=True)
            tw_data.dropna(how="all", inplace=True)
            if len(tw_data) == 0:
                runs_tw = pd.DataFrame(index=[1], columns=["Tw"])
                runs_tw["Tw"][1] = {}
            else:
                runs_tw = tw_data.groupby("run_id").apply(lambda x: self.unique_values(x))
            # write run-wise error messages
            for run in runs_ti.index:
                try:
                    for flag in runs_ti[run]:
                        level = flags_dict.get(flag)[1]
                        if level == "warning":
                            logging.warning(f"run {int(run)}, flush: {flags_dict.get(flag)[0]}")
                        elif level == "error":
                            logging.error(f"run {int(run)}, flush: {flags_dict.get(flag)[0]}")
                except KeyError:
                    pass
            for run in runs_tw.index:
                try:
                    for flag in runs_tw[run]:
                        level = flags_dict.get(flag)[1]
                        if level == "warning":
                            logging.warning(f"run {int(run)}, flush: {flags_dict.get(flag)[0]}")
                        elif level == "error":
                            logging.error(f"run {int(run)}, flush: {flags_dict.get(flag)[0]}")
                except KeyError:
                    pass
            for handler in logging.root.handlers[:]:
                logging.root.removeHandler(handler)

    def unique_values(self, data):
        """
        Creates as list of unique flags per data set and counts the appearance.

        Parameters
        ----------
        data : pd.DataFrame

        Returns
        -------
        values_counter : Counter

        """
        values = []
        for index, row in data.iterrows():
            flag = row.unique()
            flag = [x for x in flag if pd.isnull(x) is False]
            values += flag
        values_counter = Counter(values)
        return values_counter
