"""
SPDX-FileCopyrightText: 2024 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

import os
import sys
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

import pia.functions as func
from pia.extract_data import ExtractData
from pia.version import version


class Lev0Data(object):
    """
    Extracts all the necessary data from the files.

    Parameters
    ----------
    campaign : str
        campaign name as used for documentation
    pine_id : str
        PINE Id, eg: 01-A, 01-B, 04-01, AIDAm
    operation_id : int
        Operation Id for analysis
    chamber : int, optional
        Number of chamber if multiple chambers are used

    """

    def __init__(self, campaign, pine_id, operation_id, chamber=None):
        raw_data = ExtractData(campaign, pine_id, operation_id, chamber)
        self.chamber = chamber
        self.version = version
        self.campaign = campaign
        self.pine_id = pine_id
        self.pine_name = raw_data.pine_name
        self.operation_id = operation_id
        self.settings = raw_data.settings
        self.metadata = raw_data.metadata
        self.path_campaign = raw_data.path_campaign
        self.path_analysis = raw_data.path_analysis
        self.instrument_data = raw_data.read_instrument_data()
        self.opc_id = raw_data.config_info[1]
        self.calib_file_name = raw_data.config_info[2]
        self.structure = raw_data.data_structure(self.instrument_data)
        self.pfr_file = raw_data.read_pfr_file(self.structure)
        if len(self.pfr_file) == 0:
            raise IndexError("No run in operation")
        self.ref_time = raw_data.ref_time
        if (self.ref_time < datetime.strptime("2020-01-01", "%Y-%m-%d")) & (
            not self.pine_id.startswith("A")
        ):
            raw_data.check_date()
        self.dt = raw_data.dt
        grid, self.distribution_bins = func.make_df_grid_log(100, 0.1, 1000)
        read_opc_data = self.get_opc_data(self.calib_file_name, grid)
        self.size_distribution = read_opc_data[0]
        self.calib_dict = read_opc_data[1]
        if self.pine_name == "AIDAm":
            self.type_id = None
            self.aerosol = None
        else:
            logbook_info = self.get_logbook_info()
            self.type_id = logbook_info[0]
            self.aerosol = logbook_info[1]

    def get_opc_data(self, calib_file_template, grid):
        """
        Reads opc files for given operation.

        Parameters
        ----------
        calib_file_template: str
            name of the calibration file
        grid: Dataframe
            logarithmic grid for size binning

        Returns
        -------
        opc_list : list
            list containing dfs with binned opc data
        calib_dict : dict
            dictionary to convert opc bin numbers to diameters
        """
        if self.pine_id.startswith("air"):
            opc_path = (
                self.path_campaign / "raw_Data" / f"OPC{self.chamber}" / f"OP{self.operation_id}"
            )
        elif self.structure is not None:
            if self.structure < 3:
                opc_path = self.path_campaign / "L0_Data" / self.opc_id
            elif self.structure < 5:
                opc_path = self.path_campaign / "raw_Data" / self.opc_id
            elif self.structure == 5:
                opc_path = self.path_campaign / "raw_Data" / self.opc_id / f"OP {self.operation_id}"
            else:
                opc_path = self.path_campaign / "raw_Data/OPC" / f"OP {self.operation_id}"
        else:
            opc_path = self.path_campaign / "L0_Data" / self.opc_id
        if self.campaign == "PICNIC":
            opc_file = (
                f"df_PINE-{self.pine_id[1] + self.pine_id[3]}_PDD18_opid-{self.operation_id}_runid-"
            )
        else:
            if self.structure is None:
                opc_file = f"df_AIDAm_{self.campaign}_opid-{self.operation_id}_runid-"
            elif (self.structure > 1) | (self.pine_id == "3"):
                opc_file = f"df_{self.pine_name}_{self.campaign}_opid-{self.operation_id}_runid-"
            elif self.structure < 2:
                opc_file = f"df_PINE-{self.pine_id[1]}{self.pine_id[3:]}_{self.campaign}_opid-{self.operation_id}_runid-"
        if "unittest" in self.campaign:
            path_calib = Path(__file__).parents[1].joinpath("tests/test_data")
        else:
            path_calib = Path(self.metadata["paths"]["CALIB_PATH"])
        if self.structure is not None:
            if self.structure > 1:
                opc_file_suffix = "{}_opc.txt"
            elif self.structure < 2:
                opc_file_suffix = "{}_.txt"
            if self.pine_id.startswith("air"):
                opc_spd_filename = f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_OPC{self.chamber}_run_id_"
            else:
                opc_spd_filename = (
                    f"{self.pine_name}_{self.campaign}_op_id_{self.operation_id}_run_id_"
                )
        else:
            opc_file_suffix = "{}_.txt"
            opc_spd_filename = f"AIDAm_{self.campaign}_op_id_{self.operation_id}_run_id_"
        if self.chamber is None:
            opc_spd_path = (
                self.path_analysis / f"L0_Data/exportdata/exportdata_opc_spd/OP{self.operation_id}"
            )
            opc_files = [
                opc_path / (opc_file + opc_file_suffix.format(i)) for i in self.pfr_file.index
            ]
        else:
            opc_spd_path = (
                self.path_analysis
                / f"L0_Data/exportdata/exportdata_opc_spd/OPC{self.chamber}/OP{self.operation_id}"
            )
            opc_files = [
                opc_path
                / (opc_file + opc_file_suffix.format(i).replace("opc", f"opc{self.chamber}"))
                for i in range(0, len(self.pfr_file) + 1)
            ]
        opc_spd_name = opc_spd_filename + "{}_opc_spd.txt"

        opc_spd_files = [opc_spd_path / opc_spd_name.format(i) for i in self.pfr_file.index]
        opc_list = []
        time_operation = (
            self.pfr_file.loc[len(self.pfr_file), "time end"] - self.ref_time
        ).total_seconds()
        if time_operation <= 0:
            try:
                time_operation = (
                    self.pfr_file.loc[len(self.pfr_file) - 1, "time end"] - self.ref_time
                ).total_seconds()
            except KeyError:
                sys.exit("no run in operation")
        time_grid = func.make_time_grid(time_operation, self.dt)
        date = self.ref_time + pd.to_timedelta(time_grid, unit="s")
        date_end = self.ref_time + pd.to_timedelta(time_grid + self.dt, unit="s")
        grid_df = pd.concat(
            [
                pd.DataFrame(time_grid, columns=["t_start"]),
                pd.DataFrame(date, columns=["date"]),
                pd.DataFrame(date_end, columns=["date_end"]),
            ],
            axis=1,
        )
        calibration_file = pd.read_csv(
            path_calib / calib_file_template, header=None, names=["channel", "d"], sep="\t"
        )
        if self.pine_id.startswith(("air")):
            number_files = len(self.pfr_file) + 1
        else:
            number_files = len(self.pfr_file)
        os.makedirs(opc_spd_path, exist_ok=True)
        if len([x for x in os.listdir(opc_spd_path)]) == number_files:
            print("opc data already calibrated")
            with open(opc_spd_path / opc_spd_name.format(1), "r", encoding="latin1") as file:
                try:
                    version = file.readlines()[0].split(":")[1].strip()
                except:
                    version = "0"
            if version == "0":
                header_rows = 5
            elif int(version[0]) < 4:
                header_rows = 6
            opc_spd_dfs = (
                pd.read_csv(
                    file,
                    skiprows=header_rows,
                    header=None,
                    sep="\t",
                    names=["date", "channel", "p_l", "d"],
                    parse_dates=[0],
                    encoding="latin1",
                    usecols=[0, 2, 3],
                    date_format="%Y-%m-%d %H:%M:%S",
                )
                for file in opc_spd_files
            )
            for run_id, file in enumerate(opc_spd_dfs, 1):
                file["t_rel_op"] = pd.to_datetime(file.date) - self.ref_time
                size_distribution_run = self.create_size_distribution(run_id, file, grid, grid_df)
                opc_list.append(size_distribution_run)
        else:
            opc_dfs = (
                pd.read_csv(
                    file,
                    header=None,
                    names=["date", "msec", "channel", "p_l"],
                    parse_dates=[0],
                    date_format="%Y-%m-%d %H:%M:%S",
                    sep="\t",
                    usecols=[0, 2, 3],
                    encoding="latin1",
                )
                for file in opc_files
            )
            os.makedirs(opc_spd_path, exist_ok=True)
            if self.chamber is None:
                for run_id, file in enumerate(opc_dfs, 1):
                    file["t_rel_op"] = pd.to_datetime(file.date) - self.ref_time
                    file = self.calibration(calibration_file, file)
                    opc_spd = file[["date", "channel", "p_l", "d"]]
                    header = (
                        f"software version: {self.version}\n"
                        + "timestamp from opc in UTC\n"
                        + "channel number reported from opc\n"
                        + "pulse length reported from opc in µs\n"
                        + "calibrated particle diameter in µm\n"
                        + "timestamp\tchannel\tp_l\td"
                    )
                    np.savetxt(
                        opc_spd_path / opc_spd_name.format(run_id),
                        opc_spd.values,
                        fmt="%s",
                        delimiter="\t",
                        encoding="utf-8",
                        header=header,
                        comments="",
                    )
                    del opc_spd
                    file.drop(["channel"], axis=1, inplace=True)
                    size_distribution_run = self.create_size_distribution(
                        run_id, file, grid, grid_df
                    )
                    opc_list.append(size_distribution_run)
            else:
                for run_id, file in enumerate(opc_dfs, 0):
                    file["t_rel_op"] = pd.to_datetime(file.date) - self.ref_time
                    file = self.calibration(calibration_file, file)
                    if run_id == 0:
                        run_0 = file[["date", "channel", "p_l", "d"]]
                    elif run_id == 1:
                        file = pd.concat([run_0, file])
                    opc_spd = file[["date", "channel", "p_l", "d"]]
                    header = (
                        f"software version: {self.version}\n"
                        + "timestamp from opc in UTC\n"
                        + "channel number reported from opc\n"
                        + "pulse length reported from opc in µs\n"
                        + "calibrated particle diameter in µm\n"
                        + "timestamp\tchannel\tp_l\td"
                    )
                    np.savetxt(
                        opc_spd_path / opc_spd_name.format(run_id),
                        opc_spd.values,
                        fmt="%s",
                        delimiter="\t",
                        encoding="utf-8",
                        header=header,
                        comments="",
                    )
                    del opc_spd
                    file.drop(["channel"], axis=1, inplace=True)
                    if run_id == 0:
                        continue
                    size_distribution_run = self.create_size_distribution(
                        run_id, file, grid, grid_df
                    )
                    opc_list.append(size_distribution_run)
        return opc_list, calibration_file

    def calibration(self, calib_dict, opc_data):
        """
        Assigns diameters to channels according to calibration file.

        Parameters
        ----------
        calib_dict : dict
            dictionary of calibration table
        opc_data :
            dataframe with channel number
            needed column: "channel", "date"

        Returns
        -------
        opc_data: DataFrame
            Calibrated opc data
        """
        opc_data = opc_data[opc_data.channel > 0]
        opc_data["d"] = opc_data["channel"].map(calib_dict.set_index("channel")["d"])
        if len(opc_data) > 0:
            opc_data.d = round(opc_data.d, 4)
        else:
            opc_data["d"] = np.nan
        return opc_data

    def create_size_distribution(self, run_id, file, grid, time_grid):
        """
        Bins opc data for time and size grid.

        Parameters
        ----------
        run_id : int
        file : DataFrame
            opc data
        grid : DataFrame
            logarithmic size grid
        time_grid : DataFrame
            time grid

        Returns
        -------
        size_distribution : DataFrame
            Binned opc data

        """
        if run_id == 1:
            t_start = self.ref_time
        else:
            t_start = self.pfr_file.loc[run_id, "time start"]
        t_expansion = self.pfr_file.loc[run_id, "time expansion"]
        t_refill = self.pfr_file.loc[run_id, "time refill"]
        t_end = self.pfr_file.loc[run_id, "time end"]
        size_distribution_run = time_grid.loc[
            (time_grid.date_end >= t_start) & (time_grid.date <= t_end), :
        ].copy()
        size_distribution_run.reset_index(inplace=True, drop=True)
        if (t_end - t_start).total_seconds() <= 0:
            print(
                "Time for run end is set before time of run start in "
                + f"pfr-file for run {run_id}"
            )
            self.pfr_file.drop(labels=run_id, axis=0, inplace=True)
            if len(self.pfr_file) == 0:
                sys.exit("no run in operation")
            return pd.DataFrame()
        elif (t_refill - t_start).total_seconds() <= 0:
            print(
                "Time for expansion end is set before time of run start in pfr-file "
                + f"for run {run_id}"
            )
            self.pfr_file.drop(labels=run_id, axis=0, inplace=True)
            if len(self.pfr_file) == 0:
                sys.exit("no run in operation")
            return pd.DataFrame()
        else:
            # size_distribution_run.reset_index(drop=True, inplace=True)
            size_distribution_run.loc[:, "run_id"] = run_id
            if "time idle" in self.pfr_file.columns:
                t_idle = self.pfr_file.loc[run_id, "time idle"]
                t_flush = self.pfr_file.loc[run_id, "time flush"]
                size_distribution_run.loc[
                    (size_distribution_run.date < t_idle) | (size_distribution_run.date >= t_flush),
                    "run_modus",
                ] = 1
                size_distribution_run.loc[
                    (size_distribution_run.date >= t_idle)
                    & (size_distribution_run.date < t_expansion),
                    "run_modus",
                ] = 4
                size_distribution_run.loc[
                    (size_distribution_run.date >= t_refill)
                    & (size_distribution_run.date < t_flush),
                    "run_modus",
                ] = 3
            else:
                size_distribution_run.loc[
                    (size_distribution_run.date < t_expansion), "run_modus"
                ] = 1
                size_distribution_run.loc[(size_distribution_run.date >= t_refill), "run_modus"] = 3
            size_distribution_run.loc[
                (size_distribution_run.date >= t_expansion)
                & (size_distribution_run.date < t_refill),
                "run_modus",
            ] = 2
            opc_run = file
            if len(opc_run) == 0:
                size_distribution_run["opc_n"] = np.nan
                size_distribution_run[self.distribution_bins] = np.nan
            else:
                opc_run.reset_index(drop=True, inplace=True)
                opc_run["t_rel"] = (opc_run.date - self.ref_time).dt.total_seconds()
                opc_run.drop(["date", "p_l"], axis=1, inplace=True)
                run_group = (
                    opc_run.groupby(
                        pd.cut(opc_run.t_rel, size_distribution_run.t_start, right=False),
                        observed=False,
                    )["d"]
                    .apply(lambda x: self.get_hist_data(x, grid))
                    .to_frame()
                )
                run_group = pd.DataFrame(
                    run_group["d"].to_list(), columns=self.distribution_bins, index=run_group.index
                )
                run_group.reset_index(drop=True, inplace=True)
                size_distribution_run.loc[:, "opc_n"] = run_group.sum(axis=1)
                size_distribution_run.loc[:, self.distribution_bins] = run_group
            size_distribution = size_distribution_run.copy()
            return size_distribution

    def get_hist_data(self, opc_run, grid):
        """
        Applies the histogram grid to data.

        Parameters
        ----------
        opc_run : series
            particle diameter
        grid : dataframe
            logarithmic grid for binning the data

        Returns
        -------
            histogram data
        """
        dp_sorted = np.log10(np.sort(opc_run.to_numpy()))
        hist, bins = np.histogram(dp_sorted.tolist(), bins=grid.dlog.tolist(), density=False)
        del dp_sorted, bins
        return np.append(hist, 0)

    def get_logbook_info(self):
        """
        Reads operation type

        Returns
        -------
        type_id : int
            const. temp: 1, temp. ramp: 2, background: 3, cirrus: 4, test: 5
        aerosol : str
            aerosol used for experiment

        """
        try:
            logbook = pd.read_excel(
                self.path_campaign / f"Logbook_{self.pine_name}_{self.campaign}.xlsx",
                usecols=[2, 4, 6],
                header=0,
                names=["operation", "operation type", "aerosol"],
                index_col=0,
                skiprows=1,
            )
        except FileNotFoundError:
            try:
                logbook = pd.read_excel(
                    self.path_campaign / f"Logbook_{self.campaign}.xlsx",
                    usecols=[2, 4, 6],
                    header=0,
                    names=["operation", "operation type", "aerosol"],
                    index_col=0,
                    skiprows=1,
                )
            except FileNotFoundError:
                try:
                    logbook = pd.read_excel(
                        self.path_campaign / f"Logbook_{self.campaign}_PINE-1A.xlsx",
                        usecols=[2, 4, 6],
                        header=0,
                        names=["operation", "operation type", "aerosol"],
                        index_col=0,
                        skiprows=1,
                    )
                except FileNotFoundError:
                    sys.exit("Logbook can not be found")
        try:
            op_type = logbook.loc[self.operation_id, "operation type"]
            aerosol = logbook.loc[self.operation_id, "aerosol"]
        except KeyError:
            sys.exit("Operation not in logbook")
        if not isinstance(op_type, str):
            try:
                op_type.reset_index(inplace=True, drop=True)
                op_type = op_type[0]
            except AttributeError:
                sys.exit("No valid entry for operation type in logbook")
        if not isinstance(aerosol, str):
            try:
                aerosol.reset_index(inplace=True, drop=True)
                aerosol = aerosol[0]
            except AttributeError:
                sys.exit("No valid entry for aerosol in logbook")
        type_id = int(op_type[op_type.find("(") + 1])
        return type_id, aerosol
