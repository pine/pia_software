# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2021 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""
import os
from datetime import timedelta

import numpy as np
import pandas as pd

import pia.functions as func
from pia.extract_data import ExtractData


class EvalData(ExtractData):
    """
    Parent class to create Level data.
    Provides functionalities to export level data.

    Parameters
    ----------
    level0_data : instance of class Lev0Data
    """

    def __init__(self, level0_data):
        self.version = level0_data.version
        self.path_analysis = level0_data.path_analysis
        self.pine_id = level0_data.pine_id
        self.pine_name = level0_data.pine_name
        self.campaign = level0_data.campaign
        self.operation_id = level0_data.operation_id
        self.settings = level0_data.settings
        self.chamber = level0_data.chamber
        self.metadata_info = level0_data.metadata
        self.instrument_data = level0_data.instrument_data
        self.pfr_file = level0_data.pfr_file
        self.num_runs = len(self.pfr_file)
        self.type_id = level0_data.type_id
        self.metadata = self.create_metadata_header(level0_data.aerosol)

    def create_dir(self, path):
        """
        Creates directories for exported data.

        Parameters
        ----------
        path : pathlib.Path

        """
        os.makedirs(path, exist_ok=True)

    def export_data(self, path_rel, filename, df, header):
        """
        Exports text files.

        Parameters
        ----------
        path_rel : str
            path relative to the campaign folder
        filename : str
        df : pd.DataFrame
            dataframe that should be exported
        header : str
            header for the text file

        """
        np.savetxt(
            self.path_analysis / path_rel / filename,
            df.values,
            fmt="%s",
            delimiter="\t",
            encoding="utf-8",
            header=header,
            comments="",
        )

    def create_metadata_header(self, aerosol):
        """
        Creates a dictionary of relevant metadata.

        Parameters
        ----------
        aerosol : str

        Returns
        -------
        metadata_dict : dict

        """
        if self.type_id is None:
            operation_type = None
        else:
            operation_types = {
                1: "Background Measurement",
                2: "Constant Temperature",
                3: "Temperature Ramp",
                4: "Cirrus",
                5: "Test",
            }
            operation_type = operation_types[self.type_id]
        metadata_dict = {
            "software version": self.version,
            "station": self.metadata_info["metadata"]["STATION"],
            "lat": self.metadata_info["metadata"]["LAT"],
            "lon": self.metadata_info["metadata"]["LON"],
            "alt": self.metadata_info["metadata"]["ALT"],
            "PI": self.metadata_info["metadata"]["PI"],
            "institution": self.metadata_info["metadata"]["INSTITUTE"],
            "aerosol": aerosol,
            "operation type": operation_type,
            "opc": self.chamber,
        }
        return metadata_dict


class Lev1Data(EvalData):
    """
    Creates level 1 data.

    Parameters
    ----------
    level0_data : instance of class Lev0Data
    ref_time : datetime
        beginning of the operation
    manual_ice_threshold : bool
        whether the ice threshold diameters should be read in or calculated
    cirrus_mode : bool
        whether the measurements were made in cirrus regime or not
    """

    def __init__(self, level0_data, ref_time, manual_ice_threshold, cirrus_mode):
        super().__init__(level0_data)
        self.size_distribution = level0_data.size_distribution
        self.dt = self.settings["analysis"]["DT"]
        self.opc_id = level0_data.opc_id
        if self.pine_id.startswith("air"):
            self.path_concentrations = (
                f"L1_Data/exportdata/exportdata_cn/OPC{self.chamber}/OP{self.operation_id}/"
            )
            self.path_ice_threshold = f"L1_Data/exportdata/ice_threshold/OPC{self.chamber}"
        else:
            self.path_concentrations = f"L1_Data/exportdata/exportdata_cn/OP{self.operation_id}/"
            self.path_ice_threshold = "L1_Data/exportdata/ice_threshold"

        grid, self.distribution_bins = func.make_df_grid_log(100, 0.1, 1000)
        if manual_ice_threshold:
            self.ice_threshold = super().read_ice_threshold_file(True, self.chamber)
        elif cirrus_mode is False:
            self.ice_threshold = self.find_ice_threshold_diameter(
                grid,
                level0_data.calib_dict.loc[level0_data.calib_dict["channel"] == 1, "d"].to_numpy()[
                    0
                ],
            )
        self.bin_concentrations(ref_time, cirrus_mode)
        self.df_exp, self.df_flush = self.df_for_flush_and_expansion(ref_time, cirrus_mode)

    def find_ice_threshold_diameter(self, grid, opc_min):
        """
        Calculates the ice threshold for each run.

        Parameters
        ----------
        grid : pd:DataFrame
            logarithmic grid for binning the data
        opc_min : float
            minimum diameter detectable by opc

        Returns
        -------
        ice_threshold : df
            DataFrame containing the ice threshold for each run
        """
        var_data = pd.concat(self.size_distribution)
        var_data = var_data.loc[var_data["run_modus"] < 3]
        # normalize size bin counts to total number
        # extract OPC histogram data, run_id and run_modus
        bindata = var_data[["run_id"] + self.distribution_bins]
        bindata.columns = ["run_id"] + [f"nbin{i}" for i in range(0, 100)]
        bindata = bindata.loc[bindata["run_id"].notna(), :]
        bindata.set_index(["run_id"], inplace=True)
        bindata = bindata.astype("float64")
        bindata = bindata.groupby(["run_id"]).sum()
        bindata = bindata.cumsum(axis=1)
        bindata_cdf = bindata.div(bindata.iloc[:, -1], axis=0)

        buffer = bindata["nbin99"]

        # calculate 2. gradient of CDF
        bindata_cdf_grad2 = bindata_cdf.copy(deep=True)
        number_of_runs = len(bindata_cdf.index.unique(level="run_id"))
        for run_id in range(1, number_of_runs + 1):
            if buffer.loc[run_id] < 100.0:
                print(f"{run_id}: n_p@run<100! Ice-threshold might be too high!")
            bindata_cdf_grad2.loc[run_id] = np.gradient(bindata_cdf_grad2.loc[run_id].values)
            bindata_cdf_grad2.loc[run_id] = np.gradient(bindata_cdf_grad2.loc[run_id].values)
        del bindata

        d_grid = grid["d"]
        ice_threshold = pd.DataFrame(columns=["bin", "d"], index=range(1, number_of_runs + 1))

        for run_id in range(1, number_of_runs + 1):
            # if not all bins are NaN/ Null
            if not bindata_cdf_grad2.loc[run_id].isnull().all():
                # extract all bins above first minimum

                # find index for minimum in CDF''
                min_index = bindata_cdf_grad2.loc[run_id, :].idxmin()
                # select all bin-data above index of minimum
                bindata_cdf_grad2_min = bindata_cdf_grad2.loc[run_id, min_index:]

                # search all bins for which CDF''>=0
                bins_INP = bindata_cdf_grad2_min[bindata_cdf_grad2_min >= 0.0].index.values
                bins_INP = [int(b.replace("nbin", "")) for b in bins_INP]

                # check if index of maximum is lower than the first
                # index for which CDF''>=0
                max_index = int(bindata_cdf_grad2.loc[run_id, :].idxmax().replace("nbin", ""))
                if max_index < bins_INP[0]:
                    if bins_INP:  # if bins>=0 were found
                        ice_threshold.loc[run_id, "bin"] = bins_INP[0]
                    else:
                        print(f"{run_id}: no threshold found! Threshold set to default!")
                        ice_threshold.loc[run_id, "bin"] = 0
                else:
                    print(f"{run_id}: no clear liquid cloud found! Threshold set to default!")
                    ice_threshold.loc[run_id, "bin"] = 0
            else:
                # b = bindata_cdf_grad2.loc[run]
                bindata_cdf_grad2_min = pd.DataFrame(columns=bindata_cdf_grad2.columns.values)
                print(f"{run_id}: no bin data found! Threshold set to default!")
                ice_threshold.loc[run_id, "bin"] = 0

            # set diameter d to found bin no
            ice_threshold.loc[run_id, "d"] = d_grid.loc[ice_threshold.loc[run_id, "bin"]]
        return ice_threshold

    def export_ice_threshold(self):
        """
        Writes ice threshold file.

        """
        filename = super().create_filename("ice_threshold", chamber=True)
        super().create_dir(self.path_analysis / self.path_ice_threshold)
        ice_threshold_export = self.ice_threshold.copy().reset_index(drop=False)
        header = f"software version: {self.version}\nrun id\tbin\td_min"
        super().export_data(self.path_ice_threshold, filename, ice_threshold_export, header)

    def bin_concentrations(self, ref_time, cirrus_mode):
        """
        Calculates the concentrations of particles/ INPs in time bins

        Parameters
        ----------
        ref_time : datetime
            beginning of the operation
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not

        """
        for run_id in self.pfr_file.index:
            concentrations_run = self.size_distribution[run_id - 1]
            t_start = concentrations_run.loc[0, "date"]
            t_end = concentrations_run.loc[len(concentrations_run) - 1, "date_end"]

            if cirrus_mode is False:
                concentrations_run["n_ice"] = concentrations_run.loc[
                    :,
                    [str(i) for i in self.distribution_bins[int(self.ice_threshold.bin[run_id]) :]],
                ].sum(axis=1, min_count=1)
            else:
                concentrations_run["n_ice"] = np.nan
            instrument_run = func.choose_time_interval(self.instrument_data, t_start, t_end)
            instrument_grid = list(concentrations_run.date)
            instrument_grid.append(instrument_grid[-1] + timedelta(seconds=self.dt))
            instrument_group = instrument_run.groupby(
                pd.cut(instrument_run.date, instrument_grid, right=False), observed=False
            ).mean(numeric_only=True)
            instrument_group.reset_index(inplace=True, drop=True)
            del instrument_run, instrument_grid

            if self.pine_id.startswith("A"):
                flow = instrument_group.Fe
            elif self.pine_id.startswith("air"):
                flow = instrument_group.Fe
                mask_refill_idle = (concentrations_run["run_modus"] == 3) | (
                    concentrations_run["run_modus"] == 4
                )
                mask_flush = concentrations_run["run_modus"] == 1
                instrument_group["Fm"] = self.settings["analysis"]["FLOW_MAIN_PINEAIR"]
                mask_exp = concentrations_run["run_modus"] == 2
                flow_refill_idle = mask_refill_idle * 0
                flow_flush = mask_flush * instrument_group.Fm
                flow_exp = mask_exp * instrument_group.Fe
                flow = flow_flush + flow_exp + flow_refill_idle
            else:
                mask_flush = (concentrations_run["run_modus"] == 1) | (
                    concentrations_run["run_modus"] == 3
                )
                flow_flush = mask_flush * instrument_group.Fm
                mask_exp = concentrations_run["run_modus"] == 2
                flow_exp = mask_exp * instrument_group.Fe
                flow = flow_flush + flow_exp
                del mask_flush, flow_flush, mask_exp, flow_exp

            concentrations_run["flow"] = flow
            concentrations_run[
                ["Pch", "Ti1", "Ti2", "Ti3", "Ti4", "Ti5", "Tw1", "Tw2", "Tw3", "Fe", "Fm", "DP"]
            ] = instrument_group[
                [
                    "Pch",
                    "Ti1",
                    "Ti2",
                    "Ti3",
                    "Ti4",
                    "Ti5",
                    "Tw1",
                    "Tw2",
                    "Tw3",
                    "Fe",
                    "Fm",
                    "DP",
                ]
            ]

            delt = self.dt
            concentrations_run["opc_cn_bin"] = [
                func.calculate_concentration(
                    self.determine_flow(concentrations_run.flow[i]),
                    concentrations_run.opc_n[i],
                    delt,
                )
                if concentrations_run.flow[i] > 0
                else 0
                for i in range(len(concentrations_run))
            ]
            concentrations_run["cn_ice"] = [
                func.calculate_concentration(
                    self.determine_flow(concentrations_run.flow[i]),
                    concentrations_run.n_ice[i],
                    delt,
                )
                if concentrations_run.flow[i] > 0
                else 0
                for i in range(len(concentrations_run))
            ]
            concentrations_run["t_rel"] = (concentrations_run.date - t_start).dt.total_seconds()

            self.size_distribution[run_id - 1] = concentrations_run

    def determine_flow(self, flow_rec):
        """
        Determines the flow through the OPC

        Parameters
        ----------
        flow_rec : float
            measured flow

        Return
        ------
        flow : float
            corrected flow through opc
        """
        if self.opc_id[0] == "f":
            flow = flow_rec
        elif self.opc_id[0] == "w":
            # correction factor for welas
            flow = 0.1062 * flow_rec
        else:
            print("!!! opc_id is not known!!!")
        return flow

    def df_for_flush_and_expansion(self, ref_time, cirrus_mode):
        """
        Creates DataFrames with concentrations of each run for flush
        and expansion.

        Parameters
        ----------
        ref_time : datetime
            beginning of the operation
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not

        Return
        ------
        opc_cn_exp : pd.DataFrame
            contains concentrations during expansion
        opc_cn_flush : pd.DataFrame
            contains concentrations during flush
        """
        opc_cn_flush = pd.DataFrame(
            columns=["date", "t_rel", "opc_cn", "opc_n", "run_id", "cn_ice", "n_ice"],
            index=range(len(self.pfr_file)),
        )
        for i, data in enumerate(self.size_distribution):
            if self.pine_id.startswith("air"):
                if len(data) == 0:
                    continue
                # concentrations during flush for run i+1
                time_idle = self.pfr_file.loc[i + 1, "time idle"]
                time_start = time_idle - timedelta(seconds=90)
                flush = func.choose_time_interval(data, time_start, time_idle)
                flow = self.settings["analysis"]["FLOW_MAIN_PINEAIR"]
            else:
                if len(data) == 0:
                    continue
                # concentrations during flush for run i+1
                flush = data.loc[data["run_modus"] == 1, :]
                flow = self.determine_flow(np.mean(flush.flow))
            flush.reset_index(inplace=True)
            opc_cn_flush.loc[i, "date"] = flush.loc[0, "date"]
            opc_cn_flush.loc[i, "t_rel"] = (
                opc_cn_flush.loc[i, "date"] - ref_time
            ).total_seconds() / 60
            t_flush = float(flush.t_rel[len(flush) - 1]) + self.dt
            # particle count
            opc_cn_flush.loc[i, "opc_n"] = sum(flush.opc_n)
            opc_cn_flush.loc[i, "opc_cn"] = func.calculate_concentration(
                flow, opc_cn_flush.opc_n[i], t_flush
            )
            opc_cn_flush.loc[i, "run_id"] = i + 1
            # INP count
            if cirrus_mode is True:
                opc_cn_flush.loc[i, "n_ice"] = np.nan
                opc_cn_flush.loc[i, "cn_ice"] = np.nan
            else:
                opc_cn_flush.loc[i, "n_ice"] = sum(flush.n_ice)
                opc_cn_flush.loc[i, "cn_ice"] = func.calculate_concentration(
                    flow, opc_cn_flush.loc[i, "n_ice"], t_flush
                )
        opc_cn_exp = pd.DataFrame(
            columns=[
                "date",
                "t_rel",
                "opc_cn_0",
                "opc_cn_ca",
                "opc_n_0",
                "opc_n_ca",
                "run_id",
                "cn_ice_0",
                "cn_ice_ca",
                "n_ice_0",
                "n_ice_ca",
                "t_rel_cloud_activation",
            ],
            index=range(len(self.pfr_file)),
        )
        for i, data in enumerate(self.size_distribution):
            if len(data) == 0:
                continue
            # choose expansion of run i+1
            exp = data[data["run_modus"] == 2]
            exp = exp.reset_index(drop=True)
            opc_cn_exp.loc[i, "run_id"] = i + 1
            opc_cn_exp.loc[i, "date"] = (
                self.pfr_file.loc[i + 1, "time start"]
                + (self.pfr_file.loc[i + 1, "time expansion"] - self.pfr_file["time start"][i + 1])
                / 2
            )
            opc_cn_exp.loc[i, "t_rel"] = (opc_cn_exp.loc[i, "date"] - ref_time).total_seconds() / 60
            if len(exp) <= 3:
                print(f"expansion of run {i+1} is too short.")
                continue

            time_cloud_activation = self.cloud_activation(exp, i)
            opc_cn_exp.loc[i, "t_rel_cloud_activation"] = (
                time_cloud_activation - self.pfr_file.loc[i + 1, "time start"]
            ).total_seconds()
            exp_ca = func.choose_time_interval(exp, time_cloud_activation, None)
            exp_ca[exp_ca.flow != 0]
            exp[exp.flow != 0]
            exp_ca.reset_index(drop=True, inplace=True)
            flow_ca = self.determine_flow(np.mean(exp_ca.flow))
            flow_0 = self.determine_flow(np.mean(exp.flow))
            t_exp_ca = exp_ca.loc[len(exp_ca) - 1, "t_rel"] - exp_ca.loc[0, "t_rel"] + self.dt
            t_exp_0 = exp.loc[len(exp) - 1, "t_rel"] - exp.loc[0, "t_rel"] + self.dt
            opc_cn_exp.loc[i, "opc_n_ca"] = sum(exp_ca.opc_n)
            opc_cn_exp.loc[i, "opc_n_0"] = sum(exp.opc_n)
            opc_cn_exp.loc[i, "opc_cn_ca"] = func.calculate_concentration(
                flow_ca, opc_cn_exp.loc[i, "opc_n_ca"], t_exp_ca
            )
            opc_cn_exp.loc[i, "opc_cn_0"] = func.calculate_concentration(
                flow_0, opc_cn_exp.loc[i, "opc_n_0"], t_exp_0
            )
            if cirrus_mode is True:
                opc_cn_exp.loc[i, "n_ice_ca"] = np.nan
                opc_cn_exp.loc[i, "n_ice_0"] = np.nan
                opc_cn_exp.loc[i, "cn_ice_ca"] = (
                    opc_cn_exp.loc[i, "opc_cn_ca"] - opc_cn_flush.loc[i, "opc_cn"]
                )
                opc_cn_exp.loc[i, "cn_ice_0"] = (
                    opc_cn_exp.loc[i, "opc_cn_0"] - opc_cn_flush.loc[i, "opc_cn"]
                )
            else:
                opc_cn_exp.loc[i, "n_ice_ca"] = sum(exp_ca.n_ice)
                opc_cn_exp.loc[i, "n_ice_0"] = sum(exp.n_ice)
                opc_cn_exp.loc[i, "cn_ice_ca"] = func.calculate_concentration(
                    flow_ca, opc_cn_exp.loc[i, "n_ice_ca"], t_exp_ca
                )
                opc_cn_exp.loc[i, "cn_ice_0"] = func.calculate_concentration(
                    flow_0, opc_cn_exp.loc[i, "n_ice_0"], t_exp_0
                )
            if flow_ca == 0:
                opc_cn_exp.loc[i, "opc_cn_ca"] = 0
                opc_cn_exp.loc[i, "cn_ice_ca"] = 0
            if flow_0 == 0:
                opc_cn_exp.loc[i, "opc_cn_0"] = 0
                opc_cn_exp.loc[i, "cn_ice_0"] = 0

        opc_cn_exp = opc_cn_exp.set_index("run_id")
        opc_cn_flush = opc_cn_flush.set_index("run_id")
        return opc_cn_exp, opc_cn_flush

    def cloud_activation(self, expansion, index):
        """
        Determines the time of cloud activation

        Parameters
        ----------
        expansion : pd.DataFrame
            data of the expansion of one run
        index : int
            run index

        Returns
        -------
        time_cloud_activation : datetime

        """
        expansion["n_smooth"] = (
            expansion.loc[:, "opc_n"].rolling(window=3, center=True, min_periods=2).mean()
        )
        expansion["del_n"] = expansion.n_smooth.diff()

        for x in range(1, min(11, len(expansion) - 2)):
            if (
                (expansion.loc[x, "del_n"] >= 2)
                & (expansion.loc[x + 1, "del_n"] >= 2)
                & (expansion.loc[x + 2, "del_n"] >= 2)
            ):
                break

        if x == min(10, len(expansion) - 3):
            time_cloud_activation = self.pfr_file.loc[index + 1, "time expansion"]
        else:
            time_cloud_activation = expansion.loc[x, "date"] + timedelta(seconds=self.dt)
        return time_cloud_activation

    def create_df_ice(self, *arg):
        """
        Creates Dataframe with INP concentrations and important
        parameters of each run.

        Parameters
        ----------
        *arg : tuple
            instances of Lev1Data for different chambers

        Returns
        -------
        df_ice : pd.DataFrame
        """
        columns_df_ice = [
            "run_id",
            "datetime",
            "chamber",
            "T_min",
            "P_end",
            "Fe_mean",
            "INP_cn_0",
            "INP_cn_flush",
            "opc_cn_0",
            "opc_cn_flush",
        ]
        if len(arg[0]) == 0:
            df_ice = pd.DataFrame(columns=columns_df_ice, index=self.pfr_file.index)
            df_ice["chamber"] = 1
            # df_ice.datetime = self.df_exp.date
            for i in self.pfr_file.index:
                t_0 = self.pfr_file.loc[i, "time expansion"]
                t_1 = self.pfr_file.loc[i, "time refill"]
                instrument_del = func.choose_time_interval(
                    self.instrument_data, t_0, t_1 + timedelta(seconds=1)
                )
                df_ice.loc[i, "run_id"] = i
                df_ice.loc[i, "datetime"] = self.df_exp.loc[i, "date"]
                if len(instrument_del) == 0:
                    df_ice.loc[i, "T_min"] = np.nan
                    df_ice.loc[i, "P_end"] = np.nan
                    df_ice.loc[i, "Fe_mean"] = np.nan
                else:
                    df_ice.loc[i, "T_min"] = round(min(instrument_del.Ti5), 3)
                    df_ice.loc[i, "P_end"] = min(instrument_del.Pch)
                    df_ice.loc[i, "Fe_mean"] = round(np.mean(instrument_del.Fe), 3)
                df_ice.loc[i, "INP_cn_0"] = round(self.df_exp.loc[i, "cn_ice_0"], 3)
                df_ice.loc[i, "INP_cn_flush"] = round(self.df_flush.loc[i, "cn_ice"], 3)
                df_ice.loc[i, "opc_cn_0"] = round(self.df_exp.loc[i, "opc_cn_0"], 3)
                df_ice.loc[i, "opc_cn_flush"] = round(self.df_flush.loc[i, "opc_cn"], 3)
        else:
            df_ice = pd.DataFrame(columns=columns_df_ice)
            for chamber in arg[0]:
                df_ice_chamber = pd.DataFrame(columns=columns_df_ice, index=chamber.pfr_file.index)
                df_ice_chamber.chamber = chamber.chamber
                for i in chamber.pfr_file.index:
                    t_0 = chamber.pfr_file.loc[i, "time expansion"]
                    t_1 = chamber.pfr_file.loc[i, "time refill"]
                    instrument_del = func.choose_time_interval(
                        chamber.instrument_data, t_0, t_1 + timedelta(seconds=1)
                    )
                    df_ice_chamber.loc[i, "run_id"] = i
                    df_ice_chamber.loc[i, "datetime"] = t_0
                    if len(instrument_del) == 0:
                        df_ice_chamber.loc[i, "T_min"] = np.nan
                        df_ice_chamber.loc[i, "P_end"] = np.nan
                        df_ice_chamber.loc[i, "Fe_mean"] = np.nan
                    else:
                        df_ice_chamber.loc[i, "T_min"] = round(min(instrument_del.Ti5), 3)
                        df_ice_chamber.loc[i, "P_end"] = min(instrument_del.Pch)
                        df_ice_chamber.loc[i, "Fe_mean"] = round(np.mean(instrument_del.Fe), 3)
                    df_ice_chamber.loc[i, "INP_cn_0"] = round(chamber.df_exp.loc[i, "cn_ice_0"], 3)
                    df_ice_chamber.loc[i, "INP_cn_flush"] = round(
                        chamber.df_flush.loc[i, "cn_ice"], 3
                    )
                    df_ice_chamber.loc[i, "opc_cn_0"] = round(chamber.df_exp.loc[i, "opc_cn_0"], 3)
                    df_ice_chamber.loc[i, "opc_cn_flush"] = round(
                        chamber.df_flush.loc[i, "opc_cn"], 3
                    )
                df_ice = pd.concat([df_ice, df_ice_chamber])
            df_ice.set_index(["run_id", "chamber"], inplace=True, drop=False)
            df_ice.sort_index(level="run_id", inplace=True)
        return df_ice

    def export_concentrations(self, cirrus_mode):
        """
        Writes bin-wise INP concentration files.

        Parameters
        ----------
        cirrus_mode : bool
            whether the measurements were made in cirrus regime or not

        """

        super().create_dir(self.path_analysis / self.path_concentrations)
        for i, data in enumerate(self.size_distribution, 1):
            if self.dt == 3:
                filename = super().create_filename("cn", i, chamber=True)
            else:
                filename = super().create_filename(f"cn_{self.dt}sec", i, chamber=True)
            if len(data) == 0:
                continue
            if cirrus_mode is False:
                ice_threshold_run = self.ice_threshold.loc[i, "d"]
            else:
                ice_threshold_run = np.nan
            t_start = self.pfr_file.loc[i, "time start"]
            concentrations_export = data[:]
            concentrations_export["t_rel"] = (
                concentrations_export.date - t_start
            ).dt.total_seconds()
            concentrations_export.opc_cn_bin = [
                format(concentrations_export.loc[j, "opc_cn_bin"], ".2E")
                for j in range(len(concentrations_export))
            ]
            concentrations_export.cn_ice = [
                format(concentrations_export.loc[j, "cn_ice"], ".2E")
                for j in range(len(concentrations_export))
            ]
            concentrations_export = concentrations_export.round(
                {
                    "flow": 2,
                    "Pch": 4,
                    "Ti1": 4,
                    "Ti2": 4,
                    "Ti3": 4,
                    "Ti4": 4,
                    "Ti5": 4,
                    "t_rel": 1,
                }
            )
            concentrations_export = concentrations_export[
                [
                    "t_rel",
                    "opc_cn_bin",
                    "opc_n",
                    "cn_ice",
                    "n_ice",
                    "run_modus",
                    "flow",
                    "Pch",
                    "Ti1",
                    "Ti2",
                    "Ti3",
                    "Ti4",
                    "Ti5",
                ]
                + self.distribution_bins
            ]
            if self.pine_id.startswith("air"):
                header = (
                    ("\n").join([f"{key}: {value}" for (key, value) in self.metadata.items()])
                    + f"\nstart: {t_start}\n"
                    + f"size time bins: {self.dt}s\n"
                    + f"ice threshold: {round(ice_threshold_run, 4)}µm\n"
                    + "relative time to flush start in seconds\n"
                    + "particle concentration per time bin in stdL-3\n"
                    + "total number of particles in time bin\n"
                    + "INP concentration in time bin in stdL-1\n"
                    + "INP particles in time bin\n"
                    + "run modus: flush (1), expansion (2), refill (3), idle (4)\n"
                    + "mean flow for time bin in stdL/min\n"
                    + "mean chamber pressure for time bin in hPa\n"
                    + "mean Ti1 for time bin in K\n"
                    + "mean Ti2 for time bin in K\n"
                    + "mean Ti3 for time bin in K\n"
                    + "mean Ti4 for time bin in K\n"
                    + "mean Ti5 for time bin in K\n"
                    + "size distribution of particles, smallest particle "
                    + "diameter in bin in µm\n"
                    + "t_rel\tcn_bin\tn\tcn_ice\tn_ice\trun_modus\tflow\tPch\t"
                    + "Ti1\tTi2\tTi3\tTi4\tTi5\t"
                    + "\t".join(self.distribution_bins)
                )
                super().export_data(
                    self.path_concentrations, filename, concentrations_export, header
                )
            else:
                header = (
                    ("\n").join([f"{key}: {value}" for (key, value) in self.metadata.items()])
                    + f"\nstart: {t_start}\n"
                    + f"size time bins: {self.dt}s\n"
                    + f"ice threshold: {round(ice_threshold_run, 4)}µm\n"
                    + "relative time to flush start in seconds\n"
                    + "particle concentration per time bin in stdL-3\n"
                    + "total number of particles in time bin\n"
                    + "INP concentration in time bin in stdL-1\n"
                    + "INP particles in time bin\n"
                    + "run modus: flush (1), expansion (2), refill (3)\n"
                    + "mean flow for time bin in stdL/min\n"
                    + "mean chamber pressure for time bin in hPa\n"
                    + "mean Ti1 for time bin in K\n"
                    + "mean Ti2 for time bin in K\n"
                    + "mean Ti3 for time bin in K\n"
                    + "mean Ti4 for time bin in K\n"
                    + "mean Ti5 for time bin in K\n"
                    + "size distribution of particles, smallest particle "
                    + "diameter in bin in µm\n"
                    + "t_rel\tcn_bin\tn\tcn_ice\tn_ice\trun_modus\tflow\tPch\t"
                    + "Ti1\tTi2\tTi3\tTi4\tTi5\t"
                    + "\t".join(self.distribution_bins)
                )
                super().export_data(
                    self.path_concentrations, filename, concentrations_export, header
                )

    def export_ice(self, *arg):
        """
        Exports run-wise ice data.

        Parameters
        ----------
        *arg : tuple
            instances of Lev1Data for different chambers
        """
        chambers = arg
        path_ice = "L1_Data/exportdata/exportdata_ice"
        super().create_dir(self.path_analysis / path_ice)
        filename = super().create_filename("ice")
        header = (
            ("\n").join([f"{key}: {value}" for (key, value) in self.metadata.items()])
            + "\nRun Id\n"
            + "Timestamp at middle of flush in UTC\n"
            + "Chamber number\n"
            + "Minimal temperature (Ti5) reached in expansion in K\n"
            + "Pressure at the end of expansion in mbar\n"
            + "Mean flow during expansion in stdL/min\n"
            + "INP concentration in stdL-1 (start point: begin expansion)\n"
            + "Concentration of particles bigger than ice threshold in stdL-1 during flush\n"
            + "Total particle concentration in stdL-1 (start point: begin expansion)\n"
            + "Total particle concentration in stdL-1 during flush\n"
            + "run_id\tdatetime\tchamber\tT_min\tP_end\tFe_mean\tINP_cn_0\t"
            + "INP_cn_flush\tspd_cn_0\tspd_cn_flush"
        )
        super().export_data(path_ice, filename, self.create_df_ice(chambers), header)


class Lev2Data(EvalData):
    """
    Creates Level 2 Data

    parameters
    ----------
    level0_data : instance of class Lev0Data
    level1_data : instance of class Lev1Data
    del_temp : float, optional
        bin size for temperature bins
        default: 0.5
    """

    def __init__(self, level0_data, level1_data, del_temp=0.5):
        super().__init__(level0_data)
        T_range = (-60, 0)
        number_bins = (T_range[1] - T_range[0]) / del_temp
        self.grid_T = np.linspace(T_range[0], T_range[1], int(number_bins) + 1)
        self.cn_data = level1_data.size_distribution
        if self.pine_id.startswith("air"):
            self.directory = f"L2_Data/Temp_Spec/OPC{self.chamber}"
        else:
            self.directory = "L2_Data/Temp_Spec"
        self.df_exp_lev1 = level1_data.df_exp
        self.df_temp, self.df_exp = self.create_t_spec()

    def export_data(self):
        """
        Exports Level 2 data.

        """
        if self.pine_id.startswith("air"):
            directory = f"L2_Data/Temp_Spec/OPC{self.chamber}"
        else:
            directory = "L2_Data/Temp_Spec"
        super().create_dir(self.path_analysis / directory)
        filename = super().create_filename("temp", chamber=True)
        temp = self.df_temp.copy().round({"cn_ice": 4, "Pch": 4})
        header = (
            ("\n").join([f"{key}: {value}" for (key, value) in self.metadata.items()])
            + "\nStart temperature bin in °C\n"
            + "End temperature bin in °C\n"
            + "run id\n"
            + "Timestamp at middle of flush in UTC\n"
            + "Mean time relative to start of expansion in temperature bin in sec\n"
            + "Mean INP concentration in temperature bin in stdL-1\n"
            + "Lowest chamber pressure in temperature bin in hPa\n"
            + "Temp_start\tTemp_end\trun_id\tdatetime\tt_rel\tcn_ice\tPch"
        )
        super().export_data(self.directory, filename, temp, header)

    def export_data_mean(self, *arg):
        """
        Exports Level 2 mean data.

        Parameters
        ----------
        *arg : tuple
            instances of Lev2Data for different chambers

        """
        chambers = arg
        filename = super().create_filename("temp_mean")
        header = (
            ("\n").join([f"{key}: {value}" for (key, value) in self.metadata.items()])
            + "\nStart temperature bin in °C \n"
            + "End temperature bin in °C \n"
            + "Mean INP concentration in temperature bin over all runs in stdL-1\n"
            + "Standard deviation of INP concentration\n"
            + "Temp_start\tTemp_end\tcn_ice\tcn_ice_std"
        )
        mean = self.create_df_mean(chambers).copy().round({"cn_ice": 4, "cn_ice_std": 4})
        super().export_data("L2_Data/Temp_Spec", filename, mean, header)

    def create_df_mean(self, *arg):
        """
        Creates Dataframe with INP concentrations and important
        parameters of each run.

        Parameters
        ----------
        *arg : tuple
            instances of Lev2Data for different chambers

        Returns
        -------
        df_mean : pd.DataFrame
        """
        if len(arg[0]) == 0:
            df_exp = self.df_exp
        else:
            df_exp_ls = []
            for chamber in arg[0]:
                df_exp_ls.append(chamber.df_exp)
            df_exp = pd.concat(df_exp_ls)
        df_mean = pd.DataFrame()
        df_mean["Temp start"] = self.grid_T[0:-1]
        df_mean["Temp end"] = self.grid_T[1:]
        df_mean_group = (
            df_exp["cn_ice"]
            .groupby(pd.cut(df_exp.Ti5, self.grid_T, right=False), observed=True)
            .mean(numeric_only=True)
        )
        lowest_temp = df_mean_group.index[0].left
        highest_temp = df_mean_group.index[len(df_mean_group) - 1].left
        df_mean = df_mean.loc[
            (df_mean["Temp start"] >= lowest_temp) & (df_mean["Temp start"] <= highest_temp), :
        ].reset_index(drop=True)
        df_mean["cn_ice"] = df_mean_group.reset_index(drop=True)
        df_mean["cn_ice_std"] = (
            df_exp["cn_ice"]
            .groupby(pd.cut(df_exp.Ti5, self.grid_T, right=False), observed=True)
            .std()
            .reset_index(drop=True)
        )
        df_mean.dropna(inplace=True)
        df_mean = df_mean.iloc[::-1]
        return df_mean

    def create_t_spec(self):
        """
        Creates temperature spectra for each run.

        Returns
        -------
        df_temp : pd.DataFrame
            temperature spectra for each run
        df_exp : pd.DataFrame
            dataframe with binned concentrations of all expansions

        """

        data = self.cn_data
        df_temp_ls = []
        df_exp_ls = []
        for run_id, file in enumerate(data, 1):
            if len(file) == 0:
                continue
            df_temp_run = pd.DataFrame(columns=["Temp start", "Temp end", "run_id", "datetime"])
            df_temp_run["Temp start"] = self.grid_T[0:-1]
            df_temp_run["Temp end"] = self.grid_T[1:]
            expansion = file[file["run_modus"] == 2]
            expansion = expansion[["cn_ice", "Pch", "t_rel", "Ti5"]]
            expansion.reset_index(inplace=True, drop=True)
            expansion.Ti5 -= 273.15
            try:
                expansion.t_rel -= expansion.loc[0, "t_rel"]
            except KeyError:
                continue
            df_temp_run["run_id"] = run_id
            df_temp_group = (
                expansion[["t_rel", "cn_ice"]]
                .groupby(pd.cut(expansion.Ti5, self.grid_T, right=False), observed=True)
                .mean()
            )
            if len(df_temp_group) == 0:
                continue
            lowest_temp = df_temp_group.index[0].left
            highest_temp = df_temp_group.index[len(df_temp_group) - 1].left
            df_temp_run = df_temp_run.loc[
                (df_temp_run["Temp start"] >= lowest_temp)
                & (df_temp_run["Temp start"] <= highest_temp),
                :,
            ].reset_index(drop=True)
            df_temp_run[["t_rel", "cn_ice"]] = df_temp_group[["t_rel", "cn_ice"]].reset_index(
                drop=True
            )
            df_temp_run.datetime = self.df_exp_lev1.loc[run_id, "date"]
            df_temp_run.dropna(inplace=True)
            df_temp_run = df_temp_run.iloc[::-1]
            df_temp_run.reset_index(inplace=True, drop=True)
            df_temp_run["Pch"] = (
                expansion[["Pch"]]
                .groupby(pd.cut(expansion.Ti5, self.grid_T, right=False), observed=True)
                .tail(1)
                .reset_index(drop=True)
            )
            df_temp_ls.append(df_temp_run)
            df_exp_ls.append(expansion)
        df_temp = pd.concat(df_temp_ls)
        df_exp = pd.concat(df_exp_ls)
        return df_temp, df_exp
