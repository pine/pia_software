# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2023 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""


import numpy as np
from saqc.constants import FILTER_NONE, UNFLAGGED
from saqc.core.translation.basescheme import MappingScheme


class ACTRISScheme(MappingScheme):
    DFILTER_DEFAULT = FILTER_NONE

    # define the mapping from ACTRIS flags to SaQC flags
    _FORWARD: dict = {
        "000": UNFLAGGED,
        **{k: k for k in np.arange(100, 1000, dtype=int)},
    }

    # define the mapping from SaQC flags to ACTRIS flags
    _BACKWARD: dict = {
        UNFLAGGED: "000",
        **{k: k for k in np.arange(100, 1000, dtype=int)},
    }

    def __init__(self):
        super().__init__(forward=self._FORWARD, backward=self._BACKWARD)
