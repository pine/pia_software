# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2022 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

version = "3.0.3"
