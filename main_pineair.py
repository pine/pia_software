# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2023 IMK-AAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""
# insert path to software folder
# from pathlib import Path
# import sysw
# sys.path.append(Path("//imkaaf-srv1/agm-field/Software/pine_analysis_software"))

from datetime import datetime

from pia.flagging import Flagging
from pia.level0_data import Lev0Data
from pia.level_data import Lev1Data, Lev2Data
from pia.plotting import PlotsPINEair

if __name__ == "__main__":
    pine_id = "air"
    campaign = "SBO23"
    operation_id = 45

    manual_ice_threshold = False
    cirrus_mode = False

    t_0 = datetime.now()
    print(t_0)
    print("read raw data, create level0 data")
    level0_data_1 = Lev0Data(campaign, pine_id, operation_id, chamber=1)
    level0_data_2 = Lev0Data(campaign, pine_id, operation_id, chamber=2)
    level0_data_3 = Lev0Data(campaign, pine_id, operation_id, chamber=3)
    reference_time = level0_data_1.ref_time
    print("create level1 data")
    level1_data_1 = Lev1Data(level0_data_1, reference_time, manual_ice_threshold, cirrus_mode)
    level1_data_2 = Lev1Data(level0_data_2, reference_time, manual_ice_threshold, cirrus_mode)
    level1_data_3 = Lev1Data(level0_data_3, reference_time, manual_ice_threshold, cirrus_mode)
    if (cirrus_mode is False) & (manual_ice_threshold is False):
        level1_data_1.export_ice_threshold()
        level1_data_2.export_ice_threshold()
        level1_data_3.export_ice_threshold()
    level1_data_1.export_concentrations(cirrus_mode)
    level1_data_2.export_concentrations(cirrus_mode)
    level1_data_3.export_concentrations(cirrus_mode)
    level1_data_1.export_ice(level1_data_1, level1_data_2, level1_data_3)
    print("create level2 data")
    level2_data_1 = Lev2Data(level0_data_1, level1_data_1)
    level2_data_2 = Lev2Data(level0_data_2, level1_data_2)
    level2_data_3 = Lev2Data(level0_data_3, level1_data_3)
    level2_data_1.export_data()
    level2_data_2.export_data()
    level2_data_3.export_data()
    level2_data_1.export_data_mean(level2_data_1, level2_data_2, level2_data_3)
    print("flag data")
    flagged_data_1 = Flagging(level0_data_1, level1_data_1, cirrus_mode)
    flagged_data_2 = Flagging(level0_data_2, level1_data_2, cirrus_mode)
    flagged_data_3 = Flagging(level0_data_3, level1_data_3, cirrus_mode)
    flagged_data_1.export_data()
    flagged_data_2.export_data()
    flagged_data_3.export_data()
    print("create plots")
    plots = PlotsPINEair(campaign, pine_id, operation_id, manual_ice_threshold, 1, 2, 3)
    plots.plot_timeseries(cirrus_mode)
    print(datetime.now() - t_0)
