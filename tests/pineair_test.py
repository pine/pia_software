# -*- coding: utf-8 -*-
"""
SPDX-FileCopyrightText: 2024 IMKAAF/KIT

SPDX-License-Identifier: GPL-3.0-or-later
"""

from pia.flagging import Flagging
from pia.level0_data import Lev0Data
from pia.level_data import Lev1Data, Lev2Data


def main(campaign, pine_id, operation_id, manual_ice_threshold, cirrus_mode):
    level0_data_1 = Lev0Data(campaign, pine_id, operation_id, 1)
    level0_data_2 = Lev0Data(campaign, pine_id, operation_id, 2)
    level0_data_3 = Lev0Data(campaign, pine_id, operation_id, 3)
    reference_time = level0_data_1.ref_time
    level1_data_1 = Lev1Data(level0_data_1, reference_time, manual_ice_threshold, cirrus_mode)
    level1_data_2 = Lev1Data(level0_data_2, reference_time, manual_ice_threshold, cirrus_mode)
    level1_data_3 = Lev1Data(level0_data_3, reference_time, manual_ice_threshold, cirrus_mode)
    if (cirrus_mode is False) & (manual_ice_threshold is False):
        level1_data_1.export_ice_threshold()
        level1_data_2.export_ice_threshold()
        level1_data_3.export_ice_threshold()
    level1_data_1.export_concentrations(cirrus_mode)
    level1_data_2.export_concentrations(cirrus_mode)
    level1_data_3.export_concentrations(cirrus_mode)
    level1_data_1.export_ice(level1_data_1, level1_data_2, level1_data_3)
    level2_data_1 = Lev2Data(level0_data_1, level1_data_1)
    level2_data_2 = Lev2Data(level0_data_2, level1_data_2)
    level2_data_3 = Lev2Data(level0_data_3, level1_data_3)
    level2_data_1.export_data()
    level2_data_2.export_data()
    level2_data_3.export_data()
    level2_data_1.export_data_mean(level2_data_1, level2_data_2, level2_data_3)
    flagged_data_1 = Flagging(level0_data_1, level1_data_1, cirrus_mode)
    flagged_data_2 = Flagging(level0_data_2, level1_data_2, cirrus_mode)
    flagged_data_3 = Flagging(level0_data_3, level1_data_3, cirrus_mode)
    flagged_data_1.export_data()
    flagged_data_2.export_data()
    flagged_data_3.export_data()
    return flagged_data_3


def test_pineair():
    flagged_data = main("SBO23_unittest", "air", 2, False, False)
    assert flagged_data


def test_cirrus():
    flagged_data = main("SBO23_unittest", "air", 2, False, True)
    assert flagged_data
