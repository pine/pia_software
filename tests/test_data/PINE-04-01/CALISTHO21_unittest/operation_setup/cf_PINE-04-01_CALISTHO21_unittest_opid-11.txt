Pine configuration

[general]
pine-id=PINE-04-01
time=2021-10-13_15-08-14

[OPC 1]
opc-id=fidasp-03
promo-sn=
mesurement-range=2.0 - 105 microns
calib-aerosol-type=
calib-file=welasc1_1_cal_range4_133.txt

PM=1.07 V
time base=0.136 �s
scope range=0,3 mV - 3000 mV
low pass=140 kHz
trigger on=0.80 mV
trigger off=0.60 mV
trigger dynamic=16 channels
t_min hard=8.00 �s
coincidence=10.00 %
offset adjust=2.49 V

