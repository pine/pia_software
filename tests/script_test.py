from pia.flagging import Flagging
from pia.level0_data import Lev0Data
from pia.level_data import Lev1Data, Lev2Data


def main(campaign, pine_id, operation_id, manual_ice_threshold, cirrus_mode):
    level0_data = Lev0Data(campaign, pine_id, operation_id)
    reference_time = level0_data.ref_time
    level1_data = Lev1Data(level0_data, reference_time, manual_ice_threshold, cirrus_mode)
    if (manual_ice_threshold is False) & (cirrus_mode is False):
        level1_data.export_ice_threshold()
    level1_data.export_concentrations(cirrus_mode)
    level1_data.export_ice()
    level2_data = Lev2Data(level0_data, level1_data)
    level2_data.export_data()
    level2_data.export_data_mean()
    flagged_data = Flagging(level0_data, level1_data, cirrus_mode)
    flagged_data.export_data()
    return flagged_data


def test_01_A():
    flagged_data = main("CORONA_unittest", "01-A", 1, False, False)
    assert flagged_data


def test_04_01():
    flagged_data = main("CALISTHO21_unittest", "04-01", 11, False, False)
    assert flagged_data


def test_aidam_tropic():
    flagged_data = main("TROPIC05_unittest", "AIDAm", 2, False, False)
    assert flagged_data


def test_04_02():
    flagged_data = main("Kosetice_unittest", "04-02", 1, False, False)
    assert flagged_data


def test_05_02():
    flagged_data = main("VAL01_CCICE_unittest", "05-02", 1, False, False)
    assert flagged_data


def test_manual_ice_threshold():
    flagged_data = main("VAL01_CCICE_unittest", "05-02", 2, True, False)
    assert flagged_data


def test_cirrus():
    flagged_data = main("VAL01_CCICE_unittest", "05-02", 1, False, True)
    assert flagged_data
